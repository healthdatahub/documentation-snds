## Contribution Guide

This guide will assist you in making contributions to the project. Please follow these steps to ensure your changes are properly submitted and reviewed.

1. **Access the Repository:**
   - Navigate to the project on [GitLab](https://gitlab.com/healthdatahub/applications-du-hdh/documentation-snds).

2. **Find the File:**
   - Locate the file you wish to update.

3. **Edit the File:**
   - Click the `Edit` button to start making changes.

4. **Open in Web IDE:**
   - Select `Open in Web IDE` to edit files in a more comprehensive editor.![web IDE](<assets/Pasted image 20240228143453.png>)

5. **Make Your Changes:**
   - Modify the files as needed and save your changes.

6. **Commit to a New Branch:**
   - **Important:** Do not commit to the master branch. Instead, use the Source Control Tab on the left sidebar to commit your changes. When committing, change the target from `master` to a new branch by using the dropdown menu. Then commit your modifications.![commit](<assets/Pasted image 20240228145205.png>)

7. **Create a Merge Request:**
   - After committing your changes, create a Merge Request to propose your changes to the main project.![merge](<assets/Pasted image 20240228145330.png>)

8. **Pipeline and Deployment:**
   - Wait for the test and deploy pipelines to complete. This process ensures that your changes are compatible and do not introduce errors.![pipeline](<assets/Pasted image 20240228145444.png>)

9. **Review Changes on Preview:**
   - Once the pipelines are successful, review your changes in the deployment preview to ensure everything looks correct.![how to find deployment preview](<assets/Pasted image 20240228112653.png>)

10. **Merge Changes:**
    - If the pipeline results and preview are satisfactory, proceed to merge your changes into the main branch.

By following these steps, you can efficiently contribute to the project while adhering to the correct workflow and standards.