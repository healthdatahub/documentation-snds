---
tags:
  - Bénéficiaires
  - Prestations
  - Dépenses
  - DCIR/DCIRS
---


# Majorations du ticket modérateur hors parcours de soins coordonnés

<!-- SPDX-License-Identifier: MPL-2.0 -->

<TagLinks />


:::tip Crédits 

Cette fiche a été rédigée par la DREES (Vincent Reduron) dans le cadre des travaux sur les [comptes de la santé](https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/les-comptes-de-la-sante).

*A savoir : Des références peuvent être données à la fin de la fiche. La dernière date de modification se situe tout en bas à droite.*
:::


## Définitions

### Parcours de soins coordonnés

#### Principes

La **réforme de l’Assurance maladie du 13 août 2004** a instauré dans le système de santé la notion de **parcours de soins coordonnés**. Avec cette réforme, tout patient de plus de 16 ans doit avoir désigné un médecin, dit médecin traitant, qui coordonne et centralise sa prise en charge. Le non-respect par les patients de ce nouveau cadre (non désignation d'un médecin traitant par exemple) est "hors parcours de soins coordonnés". Il est assorti de pénalités, qui prennent la forme de majorations de ticket modérateur. Pour rappel, le ticket modérateur est la part restant à charge des patients après le remboursement de l'Assurance maladie. Une majoration du ticket modérateur est donc une réduction de la prise en charge du patient.

#### Critères pour qu'un soin soit dans ou hors parcours

Ces majorations de ticket modérateur sont entrées en vigueur à partir de janvier 2006. Elles concernent les **actes et consultations réalisées par les médecins**, qui sont remboursées à un taux de remboursement inférieur si elles sont hors parcours de soins coordonnés.

Pour être dans le parcours de soins coordonnés, le médecin réalisant les actes ou consultations doit être:

- soit le **médecin traitant** ;

- soit un **médecin correspondant** du médecin traitant : un médecin vers lequel le médecin traitant a orienté le patient.

Les médecins correspondants sont typiquement des spécialistes. Leur adressage par le médecin traitant est initial : ils peuvent ensuite suivre le patient pour toute une séquence de soins ou le suivi d'une maladie chronique. Les médecins traitants sont surtout des généralistes, mais pas forcément.

En dehors de cela, le soin est hors parcours, mais un certain nombre d'**exceptions** sont prévues. Les principales sont :

- les cas où le patient ne peut pas consulter son médecin traitant : remplacement de ce dernier, éloignement géographique du patient (cas "Hors résidence") ou consultation en urgence.
- les actes et consultations de 3 spécialités médicales : gynécologie, ophtalmologie et stomatologie (cas d'exception "Accès direct spécifique"). L'exception s'applique pour une liste de grands types d'actes de suivi, de dépistage et de soins, avec un périmètre toutefois large et peu restrictif ;
- les consultations de psychiatres ou de neuropsychiatres pour les patients entre 16 et 25 ans ;
- les actes et consultations des patients en affection de longue durée (ALD) pour leur maladie chronique ;

- d'autres cas plus spécifiques.

La liste complète des exceptions peut se trouver dans les références de réglementation plus bas.

Les patients n'ayant pas désigné de médecin traitant sont hors parcours de soins coordonnés, sauf dans les cas d'exception. A contrario, ceux de moins de 16 ans n'ont pas besoin de désigner de médecin traitant et sont forcément dans le parcours. Enfin, le parcours de soins coordonnés est entré en vigueur à Mayotte de façon différée, avec la loi Organisation et transformation du système de santé de juillet 2019.

### Majoration du ticket modérateur

La pénalité appliquée hors parcours de soins coordonnés est une **majoration du ticket modérateur restant à charge du patient**. Cela ne concerne donc que la base de remboursement reconnue par l'Assurance maladie, les dépassements d'honoraires n'étant pas concernés.

<ins>Dans le parcours de soins coordonnés</ins>, les actes et consultations des médecins sont remboursés ) à 70%. Le ticket modérateur à charge du patient est de 30% de la base de remboursement reconnue. Dans l'exemple d'une consultation à 26,50 euros les montants sont les suivants :

![Image](/files/DREES/Majorations_hors_parcours_de__soins_img1_dans.png)

<ins>Hors parcours de soins coordonnés</ins>, le taux de remboursement est de 30 %, soit une **diminution de 40 points** par rapport au taux légal de 70 %. Le ticket modérateur est donc de 70 %, et non 30 %, de la base de remboursement. Dans l'exemple d'une consultation à 26,50 euros le reste à charge du patient est de 18,55 euros :

![Image](/files/DREES/Majorations_hors_parcours_de__soins_img2_hors_direct.png)

Ces 18,55 euros se décomposent en un ticket modérateur classique (légal) et une majoration :

![Image](/files/DREES/Majorations_hors_parcours_de__soins_img3_hors_direct.png)

Toutefois, la majoration est plafonnée. Elle ne peut pas dépasser 40% du tarif de la consultation d'un médecin généraliste (par exemple 10,60 euros depuis novembre 2013, soit 40% de 26,50 euros). Ce plafonnement n'est _a priori_ pas identifié dans la réglementation et n'est généralement pas évoqué dans la documentation sur le sujet. 

Dans l'exemple d'un acte technique médical à 44 euros, la majoration est plafonnée ; elle vaut 10,60 euros et représente 24,1% de la base de remboursement. 

![Image](/files/DREES/Majorations_hors_parcours_de__soins_img6_hors_limite.png)

Le reste à charge global est donc de 23,80 euros, soit une majoration de 10,60 euros et un ticket modérateur classique (légal) de 13,20 euros :

![Image](/files/DREES/Majorations_hors_parcours_de__soins_img7_hors_limite.png)

>**NB:** A la mise en place de la réforme, la majoration était de 10 % de la base de remboursement, le taux de remboursement passant de 70 % dans le parcours à 60 % hors parcours.

### Prélèvement en cas de tiers-payant

Il y a donc deux cas d'application de la majoration du ticket modérateur :

- directe ;

- différée.

La majoration est appliquée directement quand c'est possible, en diminuant le remboursement au patient. La possibilité dépend principalement de l'application du tiers payant par le médecin réalisant le soin. 

Sans tiers payant, c'est possible et la majoration est simplement déduite du montant de remboursement au patient. Cette déduction **directe** est aussi possible en cas de tiers payant intégral, si le patient est bénéficiaire de Complémentaire santé solidaire (CSS). En effet, la CSS offre une couverture intégrale des actes et consultations médicales (remboursement à 100%) et couvre le montant de la majoration éventuelle. La majoration est donc appliquée directement, mais compensée immédiatement par un montant supplémentaire de CSS venant la couvrir.

En cas de tiers payant, le patient ne fait aucune avance de frais et la majoration ne peut généralement pas être déduite au moment des soins. Elle est donc récupérée **ultérieurement** par l'Assurance Maladie, au prochain remboursement sans tiers payant du patient. Cette gestion est identique à celles des franchises et participations forfaitaires. Ainsi, il y a d'abord un remboursement avec les montants légaux :

![Image](/files/DREES/Majorations_hors_parcours_de__soins_img4_hors_differe.png)

Puis la majoration est retirée au patient selon les mêmes modalités que les franchises et participations forfaitaires :

![Image](/files/DREES/Majorations_hors_parcours_de__soins_img5_hors_differe.png)


## Dans le SNDS

### Parcours de soins coordonnés

Dans la table er_prs_f du DCIR, la variable `PRS_PDS_QCP` (Qualificatif du parcours de soins calculé) retrace la gestion par les caisses d'Assurance maladie du parcours de soins coordonnés, pour chaque soin présenté au remboursement. Elle indique si le soin est dans le parcours ou en dehors, et pour quel motif. Elle est renseignée à `33`ou `34` pour les patients de moins de 16 ans (exclus de la logique de parcours de soins, donc exempts de majorations). Elle est renseignée à `31` pour les soins non concernés, à savoir tout ce qui n'est pas réalisé par des médecins.

Les codes qui indiquent l'application d'une majoration sont `22`, `23`, `24`et `26`. Ces modalités correspondent aux différents cas prévus dans la réglementation (absence de médecin traitant, ...). Les autres codes indiquent le respect du parcours de soins coordonnés ; ces codes se répartissent selon les différents cas prévus par la réglementation ("Accès direct", urgence, "Hors résidence", ...).

Dans la table **er_prs_f** du DCIR, la variable `PRS_PDS_QTP` (Qualificatif du parcours de soins transmis) indique l'information transmise par le médecin ayant réalisé les soins. C'est donc l'information que reçoivent les caisses d'Assurance maladie de la part des médecins, attestant par exemple que le patient a bien été adressé par son médecin traitant.

### Majoration du ticket modérateur

Dans la table **er_prs_f** du DCIR, plusieurs variables doivent être mobilisées pour suivre les majorations : 
- `RGO_MIN_TAU` (Taux de remboursement modulé hors parcours de soins),
- `RGO_MOD_MNT` (Montant de la majoration de la participation de l'assuré),
- `PRS_TYP_MAJ` (Type de majoration),
- `PRS_MNT_MAJ` (Montant de la majoration) 
- la nature de prestation `TMT` (Majoration hors parcours de soins, code PS5 `1957`).
- `RGO_FTA_COD` et `CPL_FTA_COD` (Forçage du taux hors parcours de soins)

Selon l'application de la majoration (directe ou différée), le suivi statistique est différent, reflétant la différence de gestion par l'Assurance maladie dans ses caisses.

Dans le parcours de soins, dans l'exemple d'une consultation à 26,50 euros, les variables `BSE_REM_MNT` et `RGO_REM_TAU` indiquent le montant remboursé et le taux de remboursement :

| BSE_REM_BSE | BSE_REM_MNT | RGO_REM_TAU |
| ----------- | ----------- | ----------- |
| 26,50 €     | 18,55 €     | 70%         |

### Direct
  
En cas de prélèvement direct, la majoration est déduite du montant de remboursement au patient. 

Dans le DCIR, la variable `BSE_REM_MNT` reflète bien la majoration du ticket modérateur. La variable `RGO_REM_TAU` indique bien le taux de remboursement minoré.

`RGO_FTA_COD` n'indique pas un forçage (d'après son libellé), ce qui voudrait dire une opération manuelle d'un technicien en caisse d'assurance maladie. Elle indique le type de majoration du ticket modérateur : `1` pour le prélèvement direct sans plafonnement, `2` ou `5` pour le prélèvement direct avec plafonnement, `3` pour le prélèvement différé sans plafonnement, `4` ou `6` pour le prélèvement différé avec plafonnement.
A noter que `RGO_FTA_COD` ne porte que sur la prestation de base (comme la variable `BSE_PRS_NAT`). La variable `CPL_FTA_COD` porte sur le complément ou majoration (`CPL_PRS_NAT`). 

`RGO_MOD_MNT` indique le montant de majoration du ticket modérateur. `RGO_MOD_MNT` globalise prestation de base et complément/majoration. Si le soin n'était pas hors parcours, le montant remboursé par l'Assurance maladie au patient serait `BSE_REM_MNT + CPL_REM_MNT` mais hors parcours de soins coordonnés le montant est `BSE_REM_MNT + CPL_REM_MNT + RGO_MOD_MNT`. 

Les variables `RGO_MIN_TAU` et `RGO_THE_TAU` indiquent des éléments de calcul **avant plafonnement éventuel** : respectivement le taux de remboursement après majoration (30%) et avant majoration (70%). 

| BSE_REM_BSE | BSE_REM_MNT | RGO_REM_TAU | RGO_MIN_TAU | RGO_MOD_MNT | RGO_THE_TAU | RGO_FTA_COD |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| 26,50 €     | 7,95 €      | 30%         | 30%         | 10,60 €     | 70%         | 1           |

En cas de plafonnement, le taux RGO_REM_TAU diminue mais `RGO_MIN_TAU` et `RGO_THE_TAU` sont inchangés :

| BSE_REM_BSE | BSE_REM_MNT | RGO_REM_TAU | RGO_MIN_TAU | RGO_MOD_MNT | RGO_THE_TAU | RGO_FTA_COD |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| 44,00 €     | 20,20 €     | 24,1%       | 30%         | 10,60 €     | 70%         | 2 ou 5      |



**NB :** Dans la table **er_aro_f** du DCIR, `ARO_MIN_TAU`, `ARO_MOD_MNT` et `ARO_FTA_COD` donnent les mêmes informations que `RGO_MIN_TAU`, `RGO_MOD_MNT` et `RGO_FTA_COD` dans la table **er_prs_f**, pour les prises en charge des majorations par la CSS ou plus rarement l'Aide médicale d'Etat (AME). Les montants de la variable `ARO_MOD_MNT` sont négatifs (typiquement -10,60 euros), un montant opposé à `RGO_MOD_MNT` dans **er_prs_f** pour la même clef9 qui fait que la majoration est nulle pour le patient (`ARO_REM_MNT` complète la prise en charge à 100% garantie par la CSS). Toujours dans le même exemple, si le patient bénéficie de la CSS, on voit cette ligne dans la table **er_aro_f**: un montant de CSS de 18,55 euros vient compléter la prise en charge à 100% de la consultation (7,95 de CSS classique et 10,60 de prise en chage de la majoration).

| ARO_MOD_MNT | ARO_REM_MNT | ARO_REM_TAU | ARO_FTA_COD |
| ----------- | ----------- | ----------- | ----------- |
| - 10,60 €   | 18,55 €     | 70%         | 1           |


### Différé

En cas de prélèvement différé, le montant de majoration est récupéré via la facturation au patient d'une prestation `TMT` (*Majoration hors parcours de soins*, code PS5 1957), au prochain remboursement sans tiers payant du patient.
   
Dans le DCIR, il y a donc deux lignes, la ligne initiale de l'acte ou de la consultation, et une ligne dédiée à la facturation ultérieure d'une prestation `TMT`. 
   
Le remboursement initial est sans majoration. Les variables `BSE_REM_MNT` et `RGO_REM_TAU` ont donc les mêmes valeurs que dans le parcours de soins coordonnés. Toutefois, la trace de la majoration à prélever ultérieurement est présente pour information : on a `PRS_TYP_MAJ == 'T'`, et `PRS_MNT_MAJ` indique le montant en euros de majoration à prélever. `RGO_MIN_TAU` indique le taux de remboursement virtuel une fois que la majoration sera prélevée (majoritairement 30%, mais un taux plus élevé en cas de plafonnement).

| BSE_REM_BSE | BSE_REM_MNT | RGO_REM_TAU | PRS_TYP_MAJ | RGO_MIN_TAU | PRS_MNT_MAJ |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| 26,50 €     | 18,55 €     | 70%         | 'T'         | 30%         | 10,60 €     |

Cette ligne vérifie `DRG_NAT parmi 35, 36` car il y a forcément tiers payant.

Attention, les valeurs de `PRS_TYP_MAJ` et `PRS_MNT_MAJ` sont également reproduites sur les lignes dédiées aux participations assurés (on y retrouve le montant `PRS_MNT_MAJ` que dans la ligne initiale). En sommant `PRS_MNT_MAJ` sans exclure les participations assurés, on surévaluerait le montant global.

La ligne dédiée à la facturation ultérieure d'une prestation `TMT` a les caractéristiques suivantes :
  
  - `CPL_PRS_NAT == 1957` : prestation TMT
  - `DRG_NAT == 10` : absence de tiers payant car la facturation est déclenchée "au prochain remboursement sans tiers payant du patient"
  - `CPL_AFF_COD == 24`
  - `CPL_MAJ_TOP == 2`
  - `CPL_REM_MNT == - montant de la majoration` (signe négatif car montant prélevé et non versé).

| CPL_REM_BSE | CPL_REM_MNT | CPL_PRS_NAT | CPL_AFF_COD | CPL_MAJ_TOP |
| ----------- | ----------- | ----------- | ----------- | ----------- |
| 0 €         | -10,60 €    | 1957        | 24          | 2           |

Dans cette ligne, on a `RGO_MOD_MNT == 0`, `RGO_MIN_TAU == 0`, `PRS_MNT_MAJ == 0` et `PRS_TYP_MAJ == 'ZZ'` : les variables spécifique aux majorations hors parcours de soins ne sont pas renseignées. La variable PRS_NAT_REF permet de connaître la nature de l'acte ou consultation pour lequel la majoration a été déclenchée.

## Proposition de code 

Le code suivant dénombre les majorations de ticket modérateur pour l'année de soins 2022, sous RStudio.

Le montant global de majorations prélevées est de **106 millions d'euros en 2022**, dont 67 millions d'euros pour les médecins généralistes et 39 millions d'euros pour les médecins spécialistes. A titre de comparaison, cela représente respectivement 0,8% et 0,3% de leurs honoraires (hors dépassements) : 8 Md€ pour les généralistes d'après les comptes de la santé de 2022, et 11 Md€ pour les spécialistes.

Deux tiers (67%) des montants sont prélevés directement et un tiers (33%) de façon différée.

Les majorations sont plus souvent appliquées à des patients avec médecin traitant déclaré (59% des montants) que sans (41%). Quand le médecin traitant est déclaré, une grande majorité des majorations concernent des médecins généralistes (78% des montants) plutôt que spécialistes (22%). Quand le médecin traitant n'est pas déclaré, la répartition entre médecins généralistes (42%) et spécialistes (58%) est plus équilibrée et reflète la répartition générale des honoraires mentionnée plus haut.


```

#*-----------------------------------------------------------------------------------------------------------
#* Programme : Estimation montants majo hors parcours soins coor.R 
#* Titre : Montants en 2022 des majorations de ticket modérateur hors parcours de soins coordonnés
#* Output  : tableaux de montants totaux annuels selon :
#*           - le prélèvement direct ou différé de la majoration
#*           - la spécialité médicale
#*           - la déclaration d'un médecin traitant ou non.
#*           - le bénéfice de la CSS ou non
#*
#* Auteur    : vincent.reduron@sante.gouv.fr, DREES/SEEE/BACS
#* Date      : 04/03/2023
#* Version   : 1 	
#*-----------------------------------------------------------------------------------------------------------


# (0) Oracle : paramètres de connexion au serveur  
#*-----------------------------------------------

library(ROracle)
library(dplyr) ; library(dbplyr)
library(tidyr) ; library(stringr) ; library(purrr)

options(dplyr.summarise.inform = FALSE) # pour supprimer le message à chaque group_by %>% summarise

drv <- dbDriver("Oracle")
conn <- dbConnect(drv, dbname = "IPIAMPR2.WORLD")
Sys.setenv(TZ = "Europe/Paris")
Sys.setenv(ORA_SDTZ = "Europe/Paris")

clef9 = c("FLX_DIS_DTD", "FLX_EMT_NUM", "FLX_EMT_ORD", "FLX_EMT_TYP", "FLX_TRT_DTD", "ORG_CLE_NUM", "DCT_ORD_NUM", "PRS_ORD_NUM", "REM_TYP_AFF")


# (1) Création références de tables distantes (remote tables) 
#*-----------------------------------------------------------------

# Année de réalisation des soins 2022, année de remboursement des soins 2022 ou 2023

ER_PRS_F = tbl(conn, 'ER_PRS_F') %>% 
  filter(DPN_QLF != 71
       , between(EXE_SOI_DTD, to_date('2022-01-01', 'YYYY-MM-DD'), to_date('2022-12-01', 'YYYY-MM-DD'))
       , between(FLX_DIS_DTD, to_date('2022-02-01', 'YYYY-MM-DD'), to_date('2024-01-01', 'YYYY-MM-DD')))

ER_ARO_F = tbl(conn, 'ER_ARO_F') %>% 
  filter(between(FLX_DIS_DTD, to_date('2022-02-01', 'YYYY-MM-DD'), to_date('2024-01-01', 'YYYY-MM-DD'))) %>%
  inner_join(
    tbl(conn, 'ER_PRS_F') %>%
       filter(between(FLX_DIS_DTD, to_date('2022-02-01', 'YYYY-MM-DD'), to_date('2024-01-01', 'YYYY-MM-DD'))
            , DPN_QLF != 71
            , between(EXE_SOI_DTD, to_date('2022-01-01', 'YYYY-MM-DD'), to_date('2022-12-01', 'YYYY-MM-DD')))
  , by = clef9) 


# (2) Description lignes de DCIR avec majoration hors parcours de soins coordonnés
#*--------------------------------------------------------------------------------

# (a) Montant prélevé directement (typiquement avec tiers payant)

  print("Lignes de DCIR des actes et consultations réalisés hors parcours de soins coordonnés, dont le remboursement est directement diminué par une majoration du ticket modérateur")

  desc_majo_dir <- function(col)
  { 
    ER_PRS_F %>% filter(RGO_MOD_MNT != 0, CPL_AFF_COD != 16) %>% 
    group_by({{col}}) %>% summarize(mnt = sum(RGO_MOD_MNT, na.rm = T)) %>% collect
  }
  desc_majo_dir(PRS_PDS_QCP)
  desc_majo_dir(DRG_NAT)
  desc_majo_dir(RGO_MOD_MNT)
  desc_majo_dir(RGO_MIN_TAU)
  desc_majo_dir(PRS_MNT_MAJ)
  desc_majo_dir(CPL_MAJ_TOP)
  desc_majo_dir(CPL_AFF_COD)
  desc_majo_dir(RGO_FTA_COD) # devrait s'appeler BSE_FTA_COD
  desc_majo_dir(CPL_FTA_COD)
  

# (b) Majoration déclenchée ultérieurement (en raison du tiers payant)

  ## ligne initiale

  print("Lignes de DCIR des actes et consultations réalisés hors parcours de soins coordonnés")

  desc_majo_1 <- function(col) 
  { 
    # lignes vérifiant PRS_TYP_MAJ == 'T' & PRS_MNT_MAJ != 0
    ER_PRS_F %>% filter(PRS_MNT_MAJ != 0, CPL_AFF_COD != 16) %>% 
    group_by({{col}}) %>% summarize(mnt = sum(PRS_MNT_MAJ, na.rm = T)) %>% collect
  }
  desc_majo_1(PRS_PDS_QCP)
  desc_majo_1(DRG_NAT)
  desc_majo_1(RGO_MOD_MNT)
  desc_majo_1(RGO_MIN_TAU)
  desc_majo_1(PRS_MNT_MAJ)
  desc_majo_1(RGO_FTA_COD)
  
  ## ligne ultérieure (prestation nature TMT)
  
  print("Lignes de DCIR des prestations nature TMT prélevés au patient")

  desc_majo_2 <- function(col) 
  { 
    # lignes vérifiant CPL_PRS_NAT == 1957 (nature de prestation TMT)
    ER_PRS_F %>% filter(CPL_PRS_NAT == 1957) %>% 
    group_by(({{col}})) %>% summarize(mnt = sum(CPL_REM_MNT, na.rm = T)) %>% collect
  }
  desc_majo_2(PRS_PDS_QCP)
  desc_majo_2(DRG_NAT)
  desc_majo_2(RGO_MOD_MNT)
  desc_majo_2(RGO_MIN_TAU)
  desc_majo_2(PRS_MNT_MAJ)
  desc_majo_2(CPL_MAJ_TOP)
  desc_majo_2(CPL_AFF_COD)
  desc_majo_2(RGO_FTA_COD)


# (3) Statistiques descriptives
#*--------------------------------------------------------------------------------
  
# (a) Montant prélevé directement (typiquement avec tiers payant)

  stat_majo_dir <- ER_PRS_F %>% filter(RGO_MOD_MNT != 0, CPL_AFF_COD != 16) %>% 
    mutate(
      # Indicatrice type de gestion de la majoration
      PRS_GES_MAJ = 'DIRECT'
    , # Indicatrice médecin généraliste ou spécialiste
      PSE_MED_GEN = ifelse(PSE_SPE_COD %in% c(1, 22, 23), '1', '2')  # '1' si généraliste, '2' si spécialiste
    , # Indicatrice médecin traitant déclaré à l'Assurance maladie
      TOP_MTT_DCL = ifelse(PRS_MTT_NUM == '00000000', 'SANS', 'AVEC')
    , # Indicatrice motif de majoration ticket modérateur hors PDS : motif avec ou sans médecin traitant déclaré à l'Assurance maladie
      TOP_MTT_MTF = ifelse(PRS_PDS_QCP == 24        , 'SANS', 'AVEC')  ) %>% 
    group_by(PRS_GES_MAJ, TOP_MTT_DCL, PSE_MED_GEN, BEN_CMU_TOP) %>% summarize(mnt = sum(RGO_MOD_MNT, na.rm = T)) %>% collect

  stat_majo_aro <- ER_ARO_F %>% filter(ARO_MOD_MNT != 0) %>% 
    mutate(
      # Indicatrice type de gestion de la majoration
      PRS_GES_MAJ = 'DIRECT'
    , # Indicatrice médecin généraliste ou spécialiste
      PSE_MED_GEN = ifelse(PSE_SPE_COD %in% c(1, 22, 23), '1', '2')  # '1' si généraliste, '2' si spécialiste
    , # Indicatrice médecin traitant déclaré à l'Assurance maladie
      TOP_MTT_DCL = ifelse(PRS_MTT_NUM == '00000000', 'SANS', 'AVEC')
    , # Indicatrice motif de majoration ticket modérateur hors PDS : motif avec ou sans médecin traitant déclaré à l'Assurance maladie
      TOP_MTT_MTF = ifelse(PRS_PDS_QCP == 24        , 'SANS', 'AVEC')  ) %>% 
    group_by(PRS_GES_MAJ, TOP_MTT_DCL, PSE_MED_GEN) %>% summarize(mnt = sum(ARO_MOD_MNT, na.rm = T)) %>% collect

  
# (b) Majoration déclenchée ultérieurement (en raison du tiers payant)

  print("Pour les majorations déclenchées ultérieurement, on cherche le montant dans la ligne ultérieure et non la ligne initiale.")
  print("C'est donc le cas 2 (CPL_PRS_NAT == 1957) à l'exclusion du cas 1 (PRS_MNT_MAJ != 0) pour éviter les doublons.")
  
  stat_majo_2 <- ER_PRS_F %>% filter(CPL_PRS_NAT == 1957) %>% 
    mutate(
      # Indicatrice type de gestion de la majoration
      PRS_GES_MAJ = 'ULTERIEUR'
    , # Indicatrice médecin généraliste ou spécialiste
      PSE_MED_GEN = ifelse(PSE_SPE_COD %in% c(1, 22, 23), '1', '2')  # '1' si généraliste, '2' si spécialiste
    , # Indicatrice médecin traitant déclaré à l'Assurance maladie
      TOP_MTT_DCL = ifelse(PRS_MTT_NUM == '00000000', 'SANS', 'AVEC')
    , # Indicatrice motif de majoration ticket modérateur hors PDS : motif avec ou sans médecin traitant déclaré à l'Assurance maladie
      TOP_MTT_MTF = ifelse(PRS_PDS_QCP == 24        , 'SANS', 'AVEC')  ) %>% 
    group_by(PRS_GES_MAJ, TOP_MTT_DCL, PSE_MED_GEN, BEN_CMU_TOP) %>% summarize(mnt = -sum(CPL_REM_MNT, na.rm = T)) %>% collect

# Ensemble
  
  warning( "Les médecins généralistes peuvent avoir des expertises particulières (cause potentielle pour lesquels les consulter hors parcours de soins coordonnés alors qu'on a un médecin généraliste comme médecin traitant)")
  stat_finale <- rbind(stat_majo_2, stat_majo_dir, stat_majo_aro) 
```

## Réglementation

- Pages de l'Assurance maladie sur la majoration du ticket modérateur hors parcours de soins coordonnés :
  
  - https://www.ameli.fr/assure/remboursements/etre-bien-rembourse/medecin-traitant-parcours-soins-coordonnes
  
  - https://www.ameli.fr/assure/remboursements/rembourse/consultations-telemedecine/metropole#text_640
  
  - https://www.ameli.fr/assure/remboursements/rembourse/consultations-telemedecine/guadeloupe-martinique-guyane-reunion#text_664

- Page de service-public.fr sur le rôle du médecin traitant et la majoration du ticket modérateur hors parcours de soins coordonnés : https://www.service-public.fr/particuliers/vosdroits/F163 

- Principaux textes de loi sur le rôle du médecin traitant et la majoration du ticket modérateur hors parcours de soins coordonnés : 
  
  - https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000048691431
  
  - https://www.legifrance.gouv.fr/codes/id/LEGISCTA000006185483

## Bibliographie rapide

- https://www.irdes.fr/Publications/Qes/Qes124.pdf

- https://www.cairn.info/revue-francaise-des-affaires-sociales-2007-1-page-109.htm
