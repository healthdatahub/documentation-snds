---
tags:
    - Généralité SNDS
    - Régime / Organisme
    - Initiation au SNDS
    - DCIR/DCIRS
    - PMSI
    - Référentiels des bénéficiaires
---

# Historique des données du SNDS
<!-- SPDX-License-Identifier: MPL-2.0 -->

<TagLinks />

::: tip Crédits

Le contenu original de cette fiche provient du document [Historique des données du SNDS](/files/INDS/historique_donnees/2019_INDS_Historique-des-données-SNDS_MPL-2.0.pptx) [INDS - 2019 - MPL-2.0].  

**Autorat et mises-à-jour**
- Rédigée par Anne Cuerq à partir des références (1) et (2), avec des contributions de Tim Vlaar à partir des références (3) et (4) (cf. section références).  
- *07/12/2020* - MAJ des deux images de la section "Historique de l'information et des régimes dans les bases du SNDS" suite à l'intégration des données de causes médicales de décès pour les années 2006 à 2012 + 2016. Plus d'informations dans la [documention](../formation_snds/documents_cnam/guide_cepidc/README.md) de la Cnam et du CépiDC. NB : Graphique de disponibilités des données selon les années et régimes par Lisa Cahour.  
- *04/06/2024* - Contribution d'Albert Vuagnat (Drees) pour le tableau : Régimes disponibles et années de présence dans les tables.  
- *01/2025* - Contribution d'Albert Vuagnat (Drees).  

*A savoir : Des références peuvent être données à la fin de la fiche. La dernière date de modification se situe tout en bas à droite.*
:::

Cette présentation illustre l'historique de la disponibilité d'informations relatives :
- aux bénéficiaires de soins,
- à la structure de l'offre de soins,
- aux consommations de soins de ville,
- aux consommations de soins en établissement de santé.

Cet historique permet de voir aussi l'ajout des différents régimes dans les bases.

## Historique de l'information et des régimes dans les bases du SNDS
![Historique données SNDS - P1](/files/Sante_publique_France/2022-11-22_historique_SNDS_MPL-2.0.png)

![Historique données SNDS - P2](/files/INDS/historique_donnees/Diapo2_MAJ2020novembre.png)

## Régimes disponibles et années de présence dans les tables

| Régime | <div style="width:150px">Intitulé</div> | 2 ou 3 premiers caractères de ORG_AFF_BEN | IR_BEN_R (*) | DCIR | CONSOPAT | <div style="width:290px">Commentaires</div> |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| RG | Régime Général | 01C | 2006 |2006 | 2006 | |
| BDF | Régime spécial de sécurité sociale de la banque de France  |  01 (09 dans la table ORAVAL.IR_GRG_V) | 2009 | 2009 | 2009 | Les bénéficiaires sont gérés par le Régime Général (01) et identifiables via les codes petit régimes RGM_COD (DCIR, REF_RGM_COD dans IR_BEN_R) 114, 144, 154, 244, 374, 534 ; à compter de septembre 2023, il n’y a plus d’admission dans ce régime. |
| SLM | Sections Locales Mutualistes  (détaillées dans les dernières lignes du tableaux) | 01M + 91 à 99 | 2006 | 2006 | 2006 | Le régime CAMIEG (Gaz et Electricité) est rattaché aux SLM depuis juillet 2008 mais codé en « CAMIEG » dans la variable REGIME de CONSOPAT. |
| MSA | Mutualité Sociale Agricole | 02 | 2009 | 2009 | 2009 | Depuis juillet 2014, la MSA inclut les données de l'organisme APRIA-AMEXA, soit l’ensemble des données du régime agricole. Entre janvier 2012 et juillet 2014, les personnes relevant d’APRIA-AMEXA sont identifiables par la requête RGM_GRG_COD=2 et FLX_EMT_TYP=21 (DCIR, table ER_PRS_F). Avant 2012, les données d’APRIA-AMEXA n'ont pas été alimentées dans le SNDS.  |
| RSI | Régime Social des Indépendants | 03 (-> 01)  | 2009-2019 | 2009-2019 | 2009-2019 | A compter du 1er janvier 2019, les nouveaux travailleurs indépendants  sont gérés par le Régime Général (01) et identifiables via les codes petits régimes RGM_COD=(103, 105, 106, 107, 360, 390, 660). Les anciens travailleurs indépendants restent avec le code grand régime RGM_GRG_COD=03 toute l’année 2019.À compter de 2019, les remboursements des travailleurs indépendants sont identifiés par PRS_FAC_TOP=1 (DCIR, table ER_PRS_F ; REF_TIN_TOP=1 dans IR_BEN_R à compter de 2020). |
| CPRP SNCF | Société Nationale des Chemins de fer Français | 04 | 2012 | 2012 | 2015 |  |
| CCAS RATP | Régie Autonome des Transports Parisiens | 05 | 2012 | 2012 | 2015 |  |
| ENIM | Etablissement National des Invalides de la Marine | 06 | 2012 | 2012 | 2015 |  |
| CANSSM | Caisse Autonome Nationale de Sécurité Sociale dans les Mines | 07 | 2012 | 2012 | 2015 |  |
| CNMSS | Caisse Nationale Militaire de Sécurité Sociale | 08 | 2009 | 2009 | 2009 |  |
| CRPCEN | Caisse de Retraite et de Prévoyance des Clercs et Employés de Notaires  | 10 | 2010 | 2010 | 2010 | Données disponibles à partir de août 2009 |
| CCIP | Chambre de Commerce et d'Industrie de Paris | 12 (-> 01) | 2012-2013 | 2012-2013 | 2012 | Depuis janvier 2013, le régime CCIP n’existe plus et ses bénéficiaires sont gérés par le régime général. Pour isoler les bénéficiaires anciennement rattaché au CCIP : RGM_GRG_COD=1 et RGM_COD=(119, 129, 159, 188, 209, 539). |
| CPPAB | Caisse de Prévoyance du Port Autonome de Bordeaux | 16 (-> 01)  | 2012-2018 | 2012-2018 | 2012 | Depuis janvier 2018, les bénéficiaires du CCPAB sont intégrés à la CPAM de Bordeaux et ne sont plus identifiables dans le DCIR. |
| CAVIMAC |  Caisse d'Assurance Vieillesse, Invalidité et MAladie des Cultes | 90 | 2010 | 2010 | 2010 | Données disponibles à partir de août 2009 |
|  FSSAN |  Fonds de Sécurité sociale de l'Assemblée Nationale | 14  | 2009 | - | - |  |
|  RA3S | Régime autonome de la sécurité sociale du Sénat | 15  | 2009 | - | - |  |
| CFE | Caisse des Français de l’Étranger | 17 | - | - | - | |
|MGEN | Mutuelle générale de l'éducation nationale | 91C ou 01M+501/506 (**) | 2006 | 2006 | 2006 | | 
| MG | Mutuelle générale des PTT (Postes, Télégraphes et Téléphones) | 92C ou 01M+512 (**) | 2006 | 2006-2018 | 2006-2018 | reprise de la gestion par la CNAM en mai 2018|
| MGP | Mutuelle générale de la police |93C ou 01M+537/604 (**) | 2006 | 2006 | 2006 | |
|MFPS | Mutuelles de la fonction publique | 94C, 96C ou 01M+523/532/533/599 (**) | 2006 | 2006 |  2006 | reprise de la gestion par la CNAM en février 2019, à l’exception de la mutuelle civile de la guerre |
|MNT | Mutuelle nationale territoriale | 95C ou 01M+609/616/652 (**) | 2006 | 2006-2017 | 2006-2017|reprise de la gestion par la CNAM octobre 2017|
| Autres | Autres mutuelles | 99C ou 01M+505/516/602/610/613/614/654/689 (**) | 2006 | 2006-2022 |2006-2022 | reprise de la gestion des différentes mutuelles par la CNAM, dernières : MNAM en février 2019, MCVPAP en décembre 2022 |
| Étudiants | Mutuelles des étudiants | 01M+601/617/618 (**) | 2006 | 2006-2019 | 2006-2019 |reprise de la gestion par la CNAM en août 2019 |
| EDF | Mutuelles de l'E.D.F. | 01M+603 (**) | 2006 | 2006 | 2006 | |
| MNH | Mutuelle nationale des hospitaliers | 01M+619/651 (**) | 2006 | 2006-2021 |2006-2021 |reprise de la gestion par la CNAM en mars 2021 |

(*) : Le référentiel des bénéficiaires IR_BEN_R donne pour chaque ligne (BEN_NIR_PSA, BEN_RNG_GEM) l’affiliation la plus récente ; celle-ci peut avoir changé (déménagement dans un autre département de résidence ; changement de type d’emploi) ; pour un historique il convient d’utiliser la table ER_PRS_F du DCIR 

(**) : 3 derniers caractères de ORG_AFF_BEN (ORG_CLE_NUM avant 2011, quand la finale de ORG_AFF_BEN est ‘000’) qui codent les composantes de chaque SLM ; ne sont indiqué, pour une meilleure lisibilité, que les valeurs effectivement utilisées depuis 2013 

Le PMSI enregistre l’ensemble des séjours hospitaliers en France quel que soit le régime d’assurance maladie des bénéficiaires. Le code du grand régime (variable `NOE_RGM`) est présent dans la table `T_MCOaaFASTC` depuis 2007 (établissements publics) et dans la table `T_MCOaaFA` depuis 2006 (établissements privés).


## Identifiants de caisse d’affiliation / liquidation

Pour chaque ligne de prestation (table ER_PRS_F du DCIR) figure l’identifiant de la caisse d’affiliation du bénéficiaire ainsi de celui de la caisse ayant liquidé le remboursement des soins ou le versement d’indemnités. Ces identifiants ont une longueur de 9 caractères et sont structurés de la façon suivante :  2 premiers caractères indiquent le code du grand régime, le caractère suivant le type d’organisme, les 3 suivants donnent les numéro de la caisse (N° de département + numéro d’ordre), les 3 derniers sont, pour les identifiant débutant avec 01M, le code de la mutuelle SLM, pour les initiales 01C le code UGE des CPAM; les libellés de ces différents codes figurent dans les tables suivantes du répertoire ORAVAL : IR_GRG_V, IR_TYO_V, IR_CAI_V, IR_ORG_V, IR_SLM_V, pour les codes de regroupements IR_SLG_V,IR_SL2_V.

A compter de 2008, est ajouté ORG_CLE_NEW qui recueille la nouvelle numérotation des caisses pour la liquidation (suite à une action dite de départementalisation) ; à compter de 2011 ORG_CLE_NEW et ORG_CLE_NUM sont identiques.

Les champs ORB_BSE_NUM et ORL_BSE_NUM dits « organismes de bases » correspondent aux 6 premiers caractères de la caisse d’affiliation du bénéficiaire ORG_AFF_BEN respectivement de la caisse de liquidation ORG_CLE_NUM.

Dans le référentiel des bénéficiaires les valeurs les plus récentes de ORG_AFF_BEN (à compter de mars 2012) et ORG_CLE_NEW, figurant dans la table prestations, sont reproduite. 


## Références

(1) *Tuppin et al., Value of a national administrative database to guide public decisions: From the système national d’information interrégimes de l’Assurance Maladie (SNIIRAM) to the système national des données de santé (SNDS) in France*  

(2) *Support de formation PMSI / portail SNDS*

(3) *Document [SNDS, ce qu'il faut savoir](../formation_snds/Sante_publique_France.md) réalisé par Santé Publique France*

(4) *Tableaux de chargement du DCIR de la documentation du portail SNDS de la CNAM*

(5) *Fiche de la Cnam sur les [notions autour des sections locales mutualistes](../files/Cnam/SNDS_Fiches_Thematiques_SLM-Mutuelles_MLP_2.0.docx)*

(6) *Document de la Cour des Comptes : [Chapitre XIII - La réorganisation des réseaux de caisses du régime général : un mouvement significatif, un impossible statu quo](https://www.ccomptes.fr/sites/default/files/EzPublish/20150915-rapport-securite-sociale-2015-reorganisation-reseaux-caisses-regime-general.pdf)*
