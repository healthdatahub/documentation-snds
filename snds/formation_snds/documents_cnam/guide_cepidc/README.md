# Guide causes médicales de décès
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette section renvoie vers la documentation du [CépiDC](../../../glossaire/CepiDC.md) à l’attention des utilisateurs concernant l’intégration des causes médicales de décès dans le Système National des Données de Santé (SNDS).

### Téléchargement

::: tip Liens de téléchargement
* [Documentation](/files/Cnam/CepiDC/202403_Documentation_utilisateurs_causes_de_deces_dans_SNDS_V5_F.pdf) à l’attention des utilisateurs concernant
 l’intégration des causes médicales de décès dans le Système National des Données de Santé - Version 5
* [Documentation](/files/Cnam/CepiDC/202403_V0_documentation_variables_CepiDc_SNDS_F.xlsx) des tables et variables des causes de décès dans le SNDS
:::

### Citation

Ce document a été initialement publié sur le portail SNDS de la Cnam en date du 3 septembre 2020.
Il a été rédigé par la [Cnam](../../../glossaire/Cnam.md) (Laurence De Roquefeuil et départements DEMSI, DATAD et DFID), avec l’aide de Grégoire Rey, Karim Bounebache et Claire Imbaud (INSERM – CépiDc).

La version actuellement disponible en téléchargement a été communiquée le 8 mars 2024 aux utilisateurs. En plus d'un guide, elle contient une document listant l'ensemble des variables disponibles.
Elle a rédigée par :
* la Cnam (départements DEMSI et DATAD) avec l’aide de Laurence De Roquefeuil
* et le CépiDC (Inserm) avec l’aide de Grégoire Rey, Claire Imbaud, Diane Martin, Pierre Boulet et Elise Coudin

### Mise-à-jour

Par rapport à la version précédente, datée du 3 septembre 2020, cette version 5 est enrichie :  
* des résultats de l’appariement des données de 2017 à 2021 selon l’appariement indirect avec les référentiels bénéficiaires (Cf. chap.II p.8), 
* des nouvelles tables de valeurs, des tables de valeurs modifiées, des variables ajoutées ou supprimées suite au changement de la méthodologie du CépiDc pour le codage des causes de décès intégrant des modèles de machine learning (Cf. chap.IV p.15) 
* d’une présentation rapide des campagnes de production 2018, 2019 et 2021, lesquelles reposent maintenant en partie sur l’usage de l’IA (réseaux de neurones) pour prédire les causes de décès de certains certificats, avec  les références vers les documents détaillant ces approches (rapports de production de campagne)