# Documents Sante publique France
<!-- SPDX-License-Identifier: MPL-2.0 -->

Documents publiés par [Santé publique France](../glossaire/SpF.md) sur ce site.

## SNDS, ce qu'il faut savoir

::: tip Guide de SPF  
Télécharger [SNDS, ce qu'il faut savoir](/files/Sante_publique_France/2024_07_GUIDE_SNDS_SpFrance_MPL-2.0.pdf) [Sante publique France - MPL-2.0], mise à jour de juillet 2024.  
:::

## Résumé 

**Guide d’utilisation du système national des données de santé pour la surveillance et l’observation épidémiologiques**

Ce guide a été élaboré par l’équipe SNDS de la Direction appui, traitements et analyses des données (Data) de Santé publique France à partir de documents déjà existants (communiqués ou documents de formation du portail SNDS, documentation collaborative du SNDS du Health Data Hub, questions/réponses des forums…) et du retour d’expérience de l’utilisation des données par les auteurs. Il n‘est pas exhaustif mais se veut une aide à l’utilisation des données du DCIR, du PMSI et des causes médicales de décès sur le portail SNDS.  
Initialement destiné aux utilisateurs de Santé publique France dans le cadre de leurs missions de surveillance de l’état de santé des populations et d’observation épidémiologique, ce guide est mis à disposition de la communauté des utilisateurs du SNDS.  
Ce document sera mis à jour régulièrement afin de prendre en compte, entre autres, les évolutions du SNDS.

*Citation suggérée* : Guide d’utilisation du système national des données de santé pour la surveillance et l’observation épidémiologiques. Saint-Maurice : Santé publique France, 2024. 78 p. Disponible à partir de l’URL : [https://www.santepubliquefrance.fr](https://www.santepubliquefrance.fr)  