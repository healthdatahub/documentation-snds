---
permalink: /tables/T_RIPaaR3A
---
# T\_RIPaaR3A
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI RIP/T_RIPaaR3A.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
