---
permalink: /tables/T_RIPaaSUP_DSC
---
# T\_RIPaaSUP\_DSC
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI RIP/T_RIPaaSUP_DSC.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
