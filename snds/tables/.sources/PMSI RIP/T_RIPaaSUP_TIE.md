---
permalink: /tables/T_RIPaaSUP_TIE
---
# T\_RIPaaSUP\_TIE
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI RIP/T_RIPaaSUP_TIE.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
