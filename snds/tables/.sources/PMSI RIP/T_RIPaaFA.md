---
permalink: /tables/T_RIPaaFA
---
# T\_RIPaaFA
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI RIP/T_RIPaaFA.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
