---
permalink: /tables/T_RIPaaTRPT
---
# T\_RIPaaTRPT
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI RIP/T_RIPaaTRPT.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
