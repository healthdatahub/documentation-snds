---
permalink: /tables/T_RIPaaE
---
# T\_RIPaaE
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI RIP/T_RIPaaE.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
