---
permalink: /tables/T_RIPaaRSAD
---
# T\_RIPaaRSAD
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI RIP/T_RIPaaRSAD.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
