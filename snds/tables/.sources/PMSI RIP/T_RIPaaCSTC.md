---
permalink: /tables/T_RIPaaCSTC
---
# T\_RIPaaCSTC
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI RIP/T_RIPaaCSTC.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
