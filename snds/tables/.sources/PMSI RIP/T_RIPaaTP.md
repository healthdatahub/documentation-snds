---
permalink: /tables/T_RIPaaTP
---
# T\_RIPaaTP
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI RIP/T_RIPaaTP.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
