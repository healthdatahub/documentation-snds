---
permalink: /tables/T_RIPaaFP
---
# T\_RIPaaFP
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI RIP/T_RIPaaFP.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
