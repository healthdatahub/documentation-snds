---
permalink: /tables/T_RIPaaTP_CTL
---
# T\_RIPaaTP\_CTL
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI RIP/T_RIPaaTP_CTL.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
