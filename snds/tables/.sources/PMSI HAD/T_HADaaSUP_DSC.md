---
permalink: /tables/T_HADaaSUP_DSC
---
# T\_HADaaSUP_DSC
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI HAD/T_HADaaSUP_DSC.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
