---
permalink: /tables/T_HADaaSUP_PREADMI
---
# T\_HADaaSUP_PREADMI
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI HAD/T_HADaaSUP_PREADMI.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
