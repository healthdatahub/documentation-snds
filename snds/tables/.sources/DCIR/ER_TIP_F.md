---
permalink: /tables/ER_TIP_F
---
# ER\_TIP\_F
<!-- SPDX-License-Identifier: MPL-2.0 -->
Dans **ER_TIP_F**, l’information affinée est communiqué sous la variable `TIP_PRS_IDE`. Anciennement appelé TIPS (tarifs interministériel des prestations sanitaires), les produits de la [LPP](/snds/glossaire/LPP.md) (liste des produits et prestations) concernent les dispositifs médicaux (DM) pour traitements, les matériels d’aide à la vie, les aliments diététiques, les articles de pansements, les orthèses et prothèses, les dispositifs médicaux implantables (DMI) et les véhicules pour handicapé.e.s physiques.

Il existe un référentiel SNDS - **NT_LPP** - qui contient la liste des codes LPP. 

Pour plus d'informations sur les dépenses par exemple, consulter la section dédiée sur la fiche [Dépenses dans les tables affinées du DCIR](/snds/fiches/tables_affinees.md).

::: tip Crédits
Les informations ci-dessus sont tirées du document [*SNDS, ce qu'il faut savoir*](/snds/formation_snds/Sante_publique_France.md) constitué par Santé Publique France.
:::

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/DCIR/ER_TIP_F.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
