---
permalink: /tables/ER_UCD_F
---
# ER\_UCD\_F
<!-- SPDX-License-Identifier: MPL-2.0 -->
Dans **ER_UCD_F**, l’information affinée est communiqué sous le code [UCD](/snds/glossaire/UCD.md) dans la variable `UCD_UCD_COD`.  

Les informations retrouvées dans cette table sont celles de la pharmacie hospitalière codée rétrocédée, ainsi que les [médicaments en sus du GHS (molécule couteuse)](/snds/fiches/medicaments_de_la_liste_en_sus.md).  
Pour plus d'informations sur les dépenses par exemple, consulter la section dédiée sur la fiche [Dépenses dans les tables affinées du DCIR](/snds/fiches/tables_affinees.md).  
Pour plus d'informations, consulter la section dédiée sur la fiche [Dépenses dans les tables affinées du DCIR](https://documentation-snds.health-data-hub.fr/fiches/tables_affinees.html#les-medicaments-retrocedes-et-de-la-liste-en-sus).  

Les codes UCD sont consultables sur le site [ameli](https://www.ameli.fr/pharmacien/exercice-professionnel/facturation-remuneration/bases-de-codage-lpp-medicaments/medicaments#text_11330).  

::: tip Crédits
Les informations ci-dessus sont tirées du document [*SNDS, ce qu'il faut savoir*](/snds/formation_snds/Sante_publique_France.md) constitué par Santé Publique France.
:::

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/DCIR/ER_UCD_F.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
