---
permalink: /tables/ER_BIO_F
---
# ER\_BIO\_F
<!-- SPDX-License-Identifier: MPL-2.0 -->
**ER_BIO_F** est la table affinée des actes de biologie médicale du DCIR. Les codes des actes de biologie sont restitués dans la variable `BIO_PRS_IDE` sous la Nomenclature des Actes de Biologie Médicale ([NABM](/snds/glossaire/NABM.md)).

Il existe également un référentiel SNDS - **IR_BIO_R** - qui contient l’historique complet des modifications de tarifs des actes biologiques depuis mars 2018.

Pour plus d'informations sur les dépenses par exemple, consulter la section dédiée sur la fiche [Dépenses dans les tables affinées du DCIR](/snds/fiches/tables_affinees.md).

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/DCIR/ER_BIO_F.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
