---
permalink: /tables/IR_ESM_R
---
# IR\_ESM\_R
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/REFERENTIELS/IR_ESM_R.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
