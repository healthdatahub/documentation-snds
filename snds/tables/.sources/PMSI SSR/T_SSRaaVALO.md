---
permalink: /tables/T_SSRaaVALO
---
# T\_SSRaaVALO
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI SSR/T_SSRaaVALO.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
