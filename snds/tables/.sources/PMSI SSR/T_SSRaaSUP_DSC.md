---
permalink: /tables/T_SSRaaSUP_DSC
---
# T\_SSRaaSUP\_DSC
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI SSR/T_SSRaaSUP_DSC.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
