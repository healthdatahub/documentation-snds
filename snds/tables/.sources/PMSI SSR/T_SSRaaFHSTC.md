---
permalink: /tables/T_SSRaaFHSTC
---
# T\_SSRaaFHSTC
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI SSR/T_SSRaaFHSTC.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
