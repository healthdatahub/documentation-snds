---
permalink: /tables/T_SSRaaSUP_VAC
---
# T\_SSRaaSUP\_VAC
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI SSR/T_SSRaaSUP_VAC.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
