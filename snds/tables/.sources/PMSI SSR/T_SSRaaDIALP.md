---
permalink: /tables/T_SSRaaDIALP
---
# T\_SSRaaDIALP
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI SSR/T_SSRaaDIALP.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
