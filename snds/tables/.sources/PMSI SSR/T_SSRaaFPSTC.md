---
permalink: /tables/T_SSRaaFPSTC
---
# T\_SSRaaFPSTC
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI SSR/T_SSRaaFPSTC.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
