---
permalink: /tables/T_SSRaaSUP_IUM
---
# T\_SSRaaSUP\_IUM
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI SSR/T_SSRaaSUP_IUM.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
