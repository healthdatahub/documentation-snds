---
permalink: /tables/T_SSRaaSUP_TIE
---
# T\_SSRaaSUP\_TIE
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI SSR/T_SSRaaSUP_TIE.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
