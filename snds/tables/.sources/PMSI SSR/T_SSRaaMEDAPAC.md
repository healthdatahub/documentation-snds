---
permalink: /tables/T_SSRaaMEDAPAC
---
# T\_SSRaaMEDAPAC
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI SSR/T_SSRaaMEDAPAC.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
