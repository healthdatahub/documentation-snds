---
permalink: /tables/CT_IDE_GN_AAAA
---
# CT\_IDE\_GN\_AAAA
<!-- SPDX-License-Identifier: MPL-2.0 -->

La cartographie des pathologies et des dépenses est une base produite par la Cnam à partir d’algorithmes développés sur les données du SNDS. Elle présente des données individuelles préparées et facilite ainsi le travail des chercheurs pour repérer les pathologies et calculer les dépenses associées.  
> Plus d'informations dans la fiche dédiée : [Cartographie des pathologies](/snds/fiches/cartographie_pathologies.md).

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/CARTOGRAPHIE_PATHOLOGIES/CT_IDE_GN_AAAA.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
