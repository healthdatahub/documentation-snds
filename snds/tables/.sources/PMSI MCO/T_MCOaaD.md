---
permalink: /tables/T_MCOaaD
---
# T\_MCOaaD
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI MCO/T_MCOaaD.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
