---
permalink: /tables/T_SUPaaATU
---
# T\_SUPaaATU
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI MCO/T_SUPaaATU.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
