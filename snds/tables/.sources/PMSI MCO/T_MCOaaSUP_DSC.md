---
permalink: /tables/T_MCOaaSUP_DSC
---
# T\_MCOaaSUP\_DSC
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI MCO/T_MCOaaSUP_DSC.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
