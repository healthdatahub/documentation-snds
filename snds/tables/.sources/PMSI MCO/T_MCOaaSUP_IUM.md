---
permalink: /tables/T_MCOaaSUP_IUM
---
# T\_MCOaaSUP\_IUM
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI MCO/T_MCOaaSUP_IUM.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
