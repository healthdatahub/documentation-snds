---
permalink: /tables/T_MCOaaTRPT
---
# T\_MCOaaTRPT
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI MCO/T_MCOaaTRPT.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
