---
permalink: /tables/T_MCOaaSUP_TIE
---
# T\_MCOaaSUP\_TIE
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI MCO/T_MCOaaSUP_TIE.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
