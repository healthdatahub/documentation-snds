---
permalink: /tables/T_MCOaaSUP_PCR
---
# T\_MCOaaSUP\_PCR
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI MCO/T_MCOaaSUP_PCR.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
