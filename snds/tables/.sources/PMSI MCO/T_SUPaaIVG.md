---
permalink: /tables/T_SUPaaIVG
---
# T\_SUPaaIVG
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI MCO/T_SUPaaIVG.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
