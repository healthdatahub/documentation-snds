---
permalink: /tables/T_MCOaaSUP_BPHNP
---
# T\_MCOaaSUP\_BPHNP
<!-- SPDX-License-Identifier: MPL-2.0 -->

<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessous -->
#include "snds/tables/.schemas/PMSI MCO/T_MCOaaSUP_BPHNP.md"
<!-- ATTENTION : Ne pas supprimer ou modifier la ligne ci-dessus -->
