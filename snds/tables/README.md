# Tables
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette partie décrit les tables du SNDS et leurs schémas : variables (nom, type et description) et clés. 

Les informations structurées de cette section sont générées à partir du [schéma formalisé du SNDS](https://gitlab.com/healthdatahub/schema-snds), qui alimente également le [dictionnaire interactif](https://health-data-hub.shinyapps.io/dico-snds/).
