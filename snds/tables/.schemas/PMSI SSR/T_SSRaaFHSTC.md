### Schéma


- Titre : Prestations Hospitalières Médicaments
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, SEQ_NUM`=> table [T_SSRaaFASTC](/tables/T_SSRaaFASTC)[ `ETA_NUM`, `SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACH_PRI`| nombre entier |Prix d'achat unitaire TTC||
`COD_UCP_CIP`| chaîne de caractères |Code UCD ou CIP||
`DEL_DAT_ENT`| nombre entier |Délai de l'acte||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |numéro FINESS géographique||
`EXE_SOI_AMD`| date |Date d'entrée du séjour||
`EXE_SOI_DTD`| date |Date de l acte||
`FAC_TOT`| nombre entier |Montant total facturé TTC||
`FRACT_COE`| nombre entier |Coefficient de fractionnement||
`MNT_UNI_ECA`| nombre entier |Montant unitaire de l'écart indemnisable||
`QUA_COD`| nombre entier |Quantité||
`SEQ_NUM`| chaîne de caractères |N° séquentiel de facture||
`TOT_MNT_ECA`| nombre entier |Montant total de l'écart indemnisable||
`TYP_ART`| chaîne de caractères |Type d'enregistrement (H)||
`UCD_UCD_COD`| nombre entier |Code UCD (13c)||