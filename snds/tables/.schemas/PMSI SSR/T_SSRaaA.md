### Schéma


- Titre : Acte CCAM
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`CDC_ACT`| chaîne de caractères |Code de l'acte CCAM||
`ETA_NUM`| chaîne de caractères |N° FINESS de l'établissement||
`PHA_ACT`| chaîne de caractères |Phase||
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`RHA_VER`| chaîne de caractères |N° version du format du RHA||
`RHS_NUM`| chaîne de caractères |Numéro séquentiel du RHS||