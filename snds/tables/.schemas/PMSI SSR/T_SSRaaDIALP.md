### Schéma


- Titre : Fich comp dialyse péritonéale en sus
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |N° FINESS e-PMSI||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`ANN`| chaîne de caractères |Année période||
`MOI`| date |N° période (mois)||
`RHA_NUM`| chaîne de caractères |Numéro séquentiel de séjour ||
`PRS_COD`| chaîne de caractères |Code prestation||
`NBR_PRS`| nombre entier |Nombre de suppléments DIP||