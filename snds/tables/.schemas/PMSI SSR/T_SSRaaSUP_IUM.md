### Schéma


- Titre : Fichier d’information des UM
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_SSRaaE](/tables/T_SSRaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`FIC_TYP`| chaîne de caractères |Numéro de format||
`ANN_MOI`| date |Année période de transmission||
`ETA_NUM_GEO`| chaîne de caractères |Nunéro FINESS géographique||
`NUM_ANO_UM`| nombre entier |N° séquentiel de l'UM||
`AUT_TYP_UM`| chaîne de caractères |Type d'autorisation||
`HOS_TYP`| chaîne de caractères |Type d'hospitalisation||