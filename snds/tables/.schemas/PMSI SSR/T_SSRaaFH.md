### Schéma


- Titre : OQN med en sus
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`TYP_ART`| chaîne de caractères |Type d'enregistrement (H)||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||
`EXE_SOI_DTD`| date |Date de soins||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`ETA_NUM`| chaîne de caractères |N° FINESS||
`COD_UCP_CIP`| chaîne de caractères |Code UCD||
`ACH_PRI`| nombre entier |Prix d'achat||
`QUA_COD`| nombre entier |Quantité ||
`FAC_TOT`| nombre entier |Montant total facturé||
`UCD_UCD_COD`| chaîne de caractères |Code UCD (0 à gauche)||
`FRACT_COE`| nombre entier |Coefficient de fractionnement||
`MNT_UNI_ECA`| nombre entier |Montant unitaire écart indemnisable||
`TOT_MNT_ECA`| nombre entier |Montant total de l'écart indémnisable||
`EXE_SOI_AMD`| date |Date d'entrée du séjour||
`ACT_DEL`| nombre entier |Délai de l'acte||
`PRI_UNI_FAC`| nombre entier |Prix unitaire facturé||
`MNT_IND`| nombre entier |Montant écart indemnisable||
`COD_TAU`| chaîne de caractères |Code Taux||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1=Ancien/2=Nouveau)||
`ENT_ANN`| année |Année de début de séjour||
`ENT_MOI`| date |mois de début de séjour||