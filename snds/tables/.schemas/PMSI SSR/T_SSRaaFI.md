### Schéma


- Titre : OQN Interruption séjour
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`TYP_ART`| chaîne de caractères |Type d'enregistrement (I)||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`EXE_SOI_DTD`| date |Date de début||
`EXE_SOI_DTF`| date |Date de fin||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||
`ETA_NUM`| chaîne de caractères |N° FINESS||
`SEJ_FIN`| chaîne de caractères |Nature d'intervention ou de fin de séjour||
`ETB_NUM`| chaîne de caractères |Etablissement||
`EXE_SOI_AMD`| date |Date d'entrée du séjour||
`EXE_SOI_AMF`| date |Date de sortie du séjour||
`ACT_DEL`| nombre entier |Délai de l'acte||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1=Ancien/2=Nouveau)||
`ACT_DUR`| nombre entier |Durée de l'acte||