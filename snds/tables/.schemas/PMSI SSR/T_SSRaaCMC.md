### Schéma


- Titre : Catégories Majeures de diagnostics
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |N° FINESS de l'établisement||
`RHA_VER`| chaîne de caractères |N° version du format du RHA||
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`NBR_JOU`| nombre entier |Nombre de jours||
`CMC_COD`| chaîne de caractères |Code CMC||