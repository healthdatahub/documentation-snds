### Schéma


- Titre : OQN DMI en sus
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`TYP_ART`| chaîne de caractères |Type d'enregistrement (P)||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`ETA_NUM`| chaîne de caractères |N° FINESS||
`LPP_COD`| chaîne de caractères |Code LPP ||
`LPP_QUA`| nombre entier |Quantité ||
`FAC_MNT`| nombre entier |Montant facturé ||
`LPP_PRI_UNI`| nombre entier |Prix d'achat unitaire||
`LPP_PRI_REF`| nombre entier |Tarif référence LPP / Prix Unitaire ||
`EXE_SOI_DTD`| date |Date de soins||
`MNT_UNI_ECA`| nombre entier |Montant unitaire de l'écart indemnisable||
`TOT_MNT_ECA`| nombre entier |Montant total de l'écart indémnisable||
`TIP_PRS_IDE`| nombre entier |Code référence LPP (format numérique)||
`EXE_SOI_AMD`| date |Date d'entrée du séjour||
`ACT_DEL`| nombre entier |Délai de l'acte||
`LPP_COD1`| chaîne de caractères |Code LPP 1||
`LPP_QUA1`| nombre entier |Quantité 1||
`LPP_PRI_UNI1`| nombre entier |Tarif référence LPP / Prix Unitaire 1||
`FAC_MNT1`| nombre entier |Montant facturé 1||
`LPP_COD2`| chaîne de caractères |Code LPP 2||
`LPP_QUA2`| nombre entier |Quantité 2||
`LPP_PRI_UNI2`| nombre entier |Tarif référence LPP / Prix Unitaire 2||
`FAC_MNT2`| nombre entier |Montant facturé 2||
`SOR_ANN`| année |Année de sortie||
`SOR_MOI`| date |Mois de sortie||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1=Ancien/2=Nouveau)||