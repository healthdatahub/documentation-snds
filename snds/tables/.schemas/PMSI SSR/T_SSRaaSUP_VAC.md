### Schéma


- Titre : Table Vaccins
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_SSRaaE](/tables/T_SSRaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS ePMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`ANN_MOI`| date |Année et Mois  de la période de transmission||
`FVACC_SUP`| chaîne de caractères |Forfait de ligne vaccinale OU type de supplément horaire||
`PERIOD`| chaîne de caractères |Période||
`NBR_PRS`| nombre entier |Nombre de prestation||