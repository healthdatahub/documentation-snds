### Schéma


- Titre : ACE Prestation
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, SEQ_NUM`=> table [T_SSRaaFASTC](/tables/T_SSRaaFASTC)[ `ETA_NUM`, `SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`SEQ_NUM`| chaîne de caractères |N° séquentiel||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`EXE_SOI_DTD`| date |Date de soins||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`ETA_NUM`| chaîne de caractères |Numéro FINESS||
`PSH_MDT`| chaîne de caractères |Mode de traitement||
`PSH_DMT`| chaîne de caractères |Discipline de prestation (ex DMT)||
`EXO_TM`| chaîne de caractères |Justification exo TM||
`EXE_SPE`| chaîne de caractères |Spécialité exécutant||
`ACT_COD`| chaîne de caractères |Code acte||
`ACT_NBR`| nombre entier |Quantité||
`ACT_COE`| nombre entier |Coefficient||
`ACT_DNB`| nombre entier |Dénombrement||
`PRI_UNI`| nombre entier |Prix Unitaire||
`REM_BAS`| nombre entier |Montant Base remboursement||
`REM_TAU`| nombre entier |Taux applicable à la prestation||
`AMO_MNR`| nombre entier |Montant Remboursable par la caisse (AMO)||
`HON_MNT`| nombre entier |Montant des honoraire (dépassement compris) ou Montant total de la dépense pour PH||
`AMC_MNR`| nombre entier |Montant remboursable par l'organisme complémentaire (AMC)||
`ETA_NUM_GEO`| chaîne de caractères |FINESS géographique||
`FJ_COD_PEC`| chaîne de caractères |Code de prise en charge FJ||
`COEF_SSR`| nombre entier |Coefficient MCO (1)||
`EXE_SOI_AMD`| date |Date d'entrée du séjour||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1 : ancien, 2 : nouveau)||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||
`SOR_ANN`| année |Année des soins||
`SOR_MOI`| date |Mois des soins||