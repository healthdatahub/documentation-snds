### Schéma


- Titre : OQN Honoraire
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`TYP_ART`| chaîne de caractères |Type d'enregistrement (C)||
`EXE_SOI_DTD`| date |Date de l'acte||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`ETA_NUM`| chaîne de caractères |N° FINESS||
`PSH_MDT`| chaîne de caractères |Mode de traitement||
`PSH_DMT`| chaîne de caractères |Discipline de prestation (ex DMT)||
`EXO_TM`| chaîne de caractères |Justification exonération TM||
`ACT_COD`| chaîne de caractères |Code acte||
`ACT_NBR`| nombre entier |Quantité||
`ACT_COE`| nombre entier |Cœfficient||
`ACT_DNB`| nombre entier |Dénombrement||
`PRI_UNI`| nombre entier |Prix Unitaire||
`REM_BAS`| nombre entier |Montant Base remboursement||
`REM_TAU`| nombre entier |Taux Remboursement||
`AMO_MNR`| nombre entier |Montant Remboursable AMO||
`HON_MNT`| nombre entier |Montant des honoraires (dépassement compris)||
`AMC_MNR`| nombre entier |Montant remboursable AMC||
`NOE_MNR`| nombre entier |Montant remboursé NOEMIE Retour||
`NOE_OPE`| chaîne de caractères |Nature opération récupération NOEMIE Retour||
`EXE_SPE`| chaîne de caractères |Spécilité exécutant||
`EXE_SOI_AMD`| date |Date d'entrée du séjour||
`ACT_DEL`| nombre entier |Délai de l'acte||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1=Ancien/2=Nouveau)||
`SOIN_ANN`| année |Année de soins||
`SOIN_MOI`| date |Mois de soins||