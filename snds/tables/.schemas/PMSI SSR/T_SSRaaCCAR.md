### Schéma


- Titre : classification acte de réadaptation
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`APP_SUP_COD`| chaîne de caractères |Code supplémentaire "appareillage"||
`CCAR_ACT`| chaîne de caractères |Code principal||
`CCAR_COD`| chaîne de caractères |Code principal||
`CCAR_ACT_COMP_WEEK`| chaîne de caractères |Acte compatible avec  la semaine||
`CCAR_NBR_REA`| nombre entier |Nombre de réalisations||
`DELAI_ENT_UM`| nombre entier |Délai depuis l'entrée dans l'UM||
`ETA_NUM`| chaîne de caractères |N° FINESS de l'établissement||
`INT_COD`| chaîne de caractères |Code de l'intervenant||
`MODUL_LIEU_COD`| chaîne de caractères |Code modulateur de lieu||
`MODUL_PAT1_COD`| chaîne de caractères |Code "modulateur de patient" n°1||
`MODUL_PAT2_COD`| chaîne de caractères |Code "modulateur de patient" n°2||
`NBR_PAT`| nombre entier |Nombre de patients en acte individuel||
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`RHA_VER`| chaîne de caractères |N° version du format du RHA||
`RHS_NUM`| chaîne de caractères |Numéro séquentiel du RHS||