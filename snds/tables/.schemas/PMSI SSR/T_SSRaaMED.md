### Schéma


- Titre : Med en sus
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |N° FINESS||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`ANN`| chaîne de caractères |Année période||
`MOIS`| date |N° période (mois)||
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`RHS_NUM`| chaîne de caractères |Numéro séquentiel du RHS||
`UCD_UCD_COD`| chaîne de caractères |Code UCD||
`ADM_NBR`| nombre entier |Nombre administré éventuellement fractionnaire||
`ACH_PRI_ADM`| nombre entier |Prix d'achat multiplié par le nombre administré||
`ADM_MOIS`| date |Mois de la date d'administration||
`ADM_ANN`| année |Année de la date d'administration||
`DAT_DELAI`| nombre entier |Délai entre la date d’entrée du séjour et la date de dispensation||
`TOP_UCD_AUTO`| chaîne de caractères |Top transcodage UCD13 auto||
`UCD_COD`| chaîne de caractères |Code UCD||