### Schéma


- Titre : Fichier d’information Transports inter-établissements
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_SSRaaE](/tables/T_SSRaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |N° FINESS d’inscription e-PMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`CL_DISTANCE`| nombre entier |Classe de distance||
`NBR_SUP_TRSP`| nombre entier |Nombre de suppléments||
`ANN_MOI`| date |Année+MOIS||
`ACT_COD`| chaîne de caractères |Code||
`NBR_SEJ`| nombre entier |Nombre de séjours||