### Schéma


- Titre : actes du Catalogue spécifique des actes de rééducation et réadaptation
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
` DEL_DAT_ENT`| nombre entier |Délai depuis la date d'entrée du séjour||
`ETA_NUM`| chaîne de caractères |N° FINESS de l'établisement||
`RHA_VER`| chaîne de caractères |N° version du format du RHA||
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`RHS_NUM`| chaîne de caractères |Numéro séquentiel du RHS||
`CSARR_COD`| chaîne de caractères |Code principal||
`APP_SUP`| chaîne de caractères |Code supplémentaire "appareillage"||
`MOD_LIEU`| chaîne de caractères |Code modulateur de lieu||
`MOD_PAT`| chaîne de caractères |Code modulateur de patient||
`MOD_TEC`| chaîne de caractères |Code modulateur de technicité||
`TYP_INTERV`| chaîne de caractères |Code de l'intervenant||
`CLAS_NBR_PAT_PS`| nombre entier |Nombre de patients en acte individuel non dédié ou collectif (classe)||
`NBR_CSARR`| nombre entier |Nombre de réalisations||
`COMP_SEM`| chaîne de caractères |Acte compatible avec  la semaine||
`NBR_PAT_INTERV`| nombre entier |Nombre réel de patients||
`NBR_INTERV`| nombre entier |Nombre d'intervenants||
`EXT_DOC`| chaîne de caractères |Extension documentaire||
`ENT_DAT_DEL_UM`| nombre entier |Délai depuis l'entrée dans l'UM||