### Schéma


- Titre : ACE actes de biologie (issus de la NABM)
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, SEQ_NUM`=> table [T_SSRaaFASTC](/tables/T_SSRaaFASTC)[ `ETA_NUM`, `SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`SEQ_NUM`| chaîne de caractères |N° séquentiel||
`EXE_SOI_DTD`| date |Date de l'acte 1||
`TYP_ART`| chaîne de caractères |Type d'enregistrement (L)||
`ETA_NUM`| chaîne de caractères |N° FINESS d'inscription e-PMSI||
`PSH_MDT`| chaîne de caractères |Mode de traitement||
`PSH_DMT`| chaîne de caractères |Discipline de prestation (ex DMT)||
`DEL_DAT_ENT`| nombre entier |délai par rapport à la date d'entrée||
`ACT_NBR`| nombre entier |Quantité acte||
`NABM_COD`| chaîne de caractères |Code acte NABM||
`ETA_NUM_GEO`| chaîne de caractères |FINESS géographique||
`EXE_SOI_AMD`| date |Date d'entrée du séjour||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1=Ancien/2=Nouveau)||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||
`ANN_MOI`| date |Mois et année||