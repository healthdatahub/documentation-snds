### Schéma


- Titre : Erreurs détectées par la fonction groupage
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS||
`MOI`| date |N° période (mois)||
`ANN`| chaîne de caractères |Année période||
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`RHS_NUM`| chaîne de caractères |Numéro séquentiel du RHS||
`COD_ERR`| chaîne de caractères |Code erreur||