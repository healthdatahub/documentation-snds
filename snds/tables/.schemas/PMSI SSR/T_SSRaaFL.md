### Schéma


- Titre : OQN actes de biologie (issus de la NABM)
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`TYP_ART`| chaîne de caractères |Type d'enregistrement (L)||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||
`ETA_NUM`| chaîne de caractères |N° FINESS d'inscription e-PMSI||
`PSH_MDT`| chaîne de caractères |Mode de traitement||
`PSH_DMT`| chaîne de caractères |Discipline de prestation (ex DMT)||
`EXE_SOI_DTD`| date |Date de l'acte 1 à 5||
`DEL_DAT_ENT`| nombre entier |délai par rapport à la date d'entrée||
`ACT_NBR`| nombre entier |Quantité acte||
`NABM_COD`| chaîne de caractères |Code acte NABM||
`EXE_SOI_AMD`| date |Date d'entrée du séjour||
`ANN_MOI`| date |Mois et année||