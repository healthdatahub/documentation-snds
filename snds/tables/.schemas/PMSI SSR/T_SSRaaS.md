### Schéma


- Titre : synthese
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |FINESS||
`RHA_VER`| chaîne de caractères |Version du Format du SRHA||
`GEN_VER`| chaîne de caractères |Version de GENRHA||
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`SUI_TYP`| chaîne de caractères |Type de suite ||
`SEJ_NBJ`| nombre entier |Durée du séjour||
`PRE_JOU_NBR`| nombre entier |Nombre de journées de présence||
`ENT_MOD`| chaîne de caractères |Mode d’entrée du séjour||
`ENT_PRV`| chaîne de caractères |Provenance||
`SOR_MOD`| chaîne de caractères |Mode de sortie du séjour||
`SOR_DES`| chaîne de caractères |Destination||
`ACT_TYP`| chaîne de caractères |Type d’hospitalisation||
`NBR_MUT`| nombre entier |Nombre de mutations||
`NBR_RHA`| nombre entier |Nombre de RHA||
`NBR_GME`| nombre entier |Nombre de GME (ng)||
`TYP_VALO_SEJ`| chaîne de caractères |Type de valorisation du séjour||
`DUMMY`| chaîne de caractères |DUMMY||
`NBR_CMC`| nombre entier |Nombre de CMC (nc)||
`NBR_GHJ`| nombre entier |Nombre de GHJ (ng)||
`NBR_UM`| nombre entier |Nombre d’unité médicale||
`NBR_CM`| nombre entier |Nombre de CM (nc)||