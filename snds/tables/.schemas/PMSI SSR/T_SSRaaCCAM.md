### Schéma


- Titre : Acte CCAM
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |N° FINESS de l'établissement||
`RHA_VER`| chaîne de caractères |N° version du format du RHA||
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`RHS_NUM`| chaîne de caractères |Numéro séquentiel du RHS||
`CCAM_ACT`| chaîne de caractères |Code de l'acte CCAM||
`EXT_PMSI`| chaîne de caractères |Extension PMSI||
`CCAM_PHA_ACT`| chaîne de caractères |Code de la phase||
` DEL_DAT_ENT`| nombre entier |Délai depuis la date d'entrée du séjour||
`CCAM_COD_ACT`| chaîne de caractères |Code de l'activité||
`CCAM_EXT_DOC`| chaîne de caractères |Extension documentaire||
`CCAM_NBR_REA`| nombre entier |Nombre de réalisation||
`CCAM_VAL_DAT`| chaîne de caractères |Date de validité de l'acte compatible avec  la semaine||
`CCAM_DEL_ENT_UM`| nombre entier |Délai depuis l'entrée dans l'UM||