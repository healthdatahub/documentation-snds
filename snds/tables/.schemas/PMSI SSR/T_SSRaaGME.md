### Schéma


- Titre : GME
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |N° FINESS de l'établisement||
`RHA_VER`| chaîne de caractères |N° version du format du RHA||
`RHA_NUM`| chaîne de caractères |N° Séquentiel du séjour||
`NBR_JOU`| nombre entier |Nombre de jours de présence valorisables||
`GME_COD`| chaîne de caractères |Code GME||
`GMT_NUM`| chaîne de caractères |GMT||
`GME`| chaîne de caractères |GME||
`RET_COD`| chaîne de caractères |code retour *||
`RHS_NUM`| chaîne de caractères |N° séquentiel du RHS||