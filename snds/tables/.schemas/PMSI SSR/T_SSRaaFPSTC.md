### Schéma


- Titre : Prestations Hospitalières Prothèses
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, SEQ_NUM`=> table [T_SSRaaFASTC](/tables/T_SSRaaFASTC)[ `ETA_NUM`, `SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`DEL_DAT_ENT`| nombre entier |Délai de l'acte||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |numéro FINESS géographique||
`EXE_SOI_AMD`| date |Date d'entrée du séjour||
`EXE_SOI_DTD`| date |Date de l acte||
`FAC_MNT`| nombre entier |Montant facturé||
`LPP_COD`| chaîne de caractères |Code référence LPP||
`LPP_PRI_REF`| nombre entier |Tarif référence LPP ou Prix Unitaire sur devis||
`LPP_PRI_UNI`| nombre entier |Prix d'achat unitaire||
`LPP_QUA`| nombre entier |Quantité||
`MNT_UNI_ECA`| nombre entier |Montant unitaire de l'écart indemnisable||
`SEQ_NUM`| chaîne de caractères |N° séquentiel de facture||
`TIP_PRS_IDE`| nombre entier |Code LPP (13 c)||
`TOT_MNT_ECA`| nombre entier |Montant total de l'écart indemnisable||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||