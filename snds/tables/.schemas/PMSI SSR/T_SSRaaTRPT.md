### Schéma


- Titre : Fich comp Transports intra et inter établissements 
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHA_NUM`=> table [T_SSRaaB](/tables/T_SSRaaB)[ `ETA_NUM`, `RHA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`ANN`| chaîne de caractères |Année période de transmission||
`MOI`| date |Mois période de transmission||
`RHA_NUM`| chaîne de caractères |Numéro séquentiel de séjour ||
`RHS_NUM`| chaîne de caractères |Numéro séquentiel du RHS||
`EXE_SOI_DTD`| date |Date d'aller||
`ACT_COD`| chaîne de caractères |Code||
`CL_DISTANCE`| nombre entier |Classe de distance||
`EXE_SOI_RET`| chaîne de caractères |CR Date d'aller||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée du séjour||