### Schéma


- Titre : Table historique des affiliations à un organisme complémentaire
<br />



- Clé(s) étrangère(s) : <br />
`BEN_NIR_PSA, BEN_RNG_GEM`=> table [IR_BEN_R](/tables/IR_BEN_R)[ `BEN_NIR_PSA`, `BEN_RNG_GEM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`BEN_CMU_ORG`| chaîne de caractères |Code de l'organisme complémentaire||
`BEN_CTA_TYP`| nombre entier |Type de contrat complémentaire||
`BEN_IDT_ANO`| chaîne de caractères |Identifiant bénéficiaire anonymisé||
`BEN_NIR_ANO`| chaîne de caractères |NIR pseudonymisé du bénéficiaire||
`BEN_NIR_PSA`| chaîne de caractères |Identifiant anonyme du patient dans le SNIIRAM||
`BEN_RNG_GEM`| nombre entier |Rang de naissance du bénéficiaire||
`CTO_IDT_ANO`| chaîne de caractères |Identifiant Cartographie||
`IND_RNM_BEN`| chaîne de caractères |Top RNIAM||
`MLL_CTA_DSD`| date |Date de début du contrat complémentaire||
`MLL_CTA_DSF`| date |Date de fin du contrat complémentaire||
`REF_C2S_COD`| chaîne de caractères |Code complémentaire santé solidaire||