### Schéma


- Titre : Table Référentiel des professionnels de santé (périmètre SNDS)
<br />



 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACT_CAB_COD`| chaîne de caractères |Code rang du cabinet lié à l'activité||
`CAB_REF_DSD`| chaîne de caractères |Date début du code options conventionnelles ||
`CAB_REF_DSF`| chaîne de caractères |Date fin du code options conventionnelles ||
`CAI_NUM`| chaîne de caractères |Caisse primaire de rattachement ||
`CES_CES_COD`| chaîne de caractères |Date fin du code options conventionnelles ||
`CNV_CNV_COD`| chaîne de caractères |Code convention||
`DTE_ANN_TRT`| chaîne de caractères |Année de traitement ||
`DTE_MOI_FIN`| date |Mois de traitement||
`EXC_ANT_DSD`| chaîne de caractères |Date d'effet de la nature d'exercice antérieure du PS||
`EXC_ANT_MTF`| chaîne de caractères |Motif de fin d'exercice de la natire d'exercice antérieure du PS||
`EXC_ANT_NAT`| chaîne de caractères |Nature d'exercice antérieure du PS||
`EXC_EFF_DSD`| chaîne de caractères |Date d'effet de la nature d'exercice||
`EXC_EXC_NAT`| chaîne de caractères |Code nature d'exercice||
`EXC_FIN_MTF`| chaîne de caractères |Motif de fin d'exercice||
`FIS_CAI_NUM`| chaîne de caractères |CPAM resp. relevé fisc. du P.S. ||
`FIS_PFS_NUM`| chaîne de caractères |Numéro chainage fiscal d'un P.S. ||
`FIS_URC_COD`| chaîne de caractères |URCAM de chaînage fiscal ||
`IPP_ANN_NAI`| chaîne de caractères |Année de naissance du PS||
`IPP_IDV_CLE`| chaîne de caractères |_||
`IPP_IDV_NUM`| chaîne de caractères |N.I.R. du P.S ||
`IPP_NOM_PAT`| chaîne de caractères |_||
`IPP_PFS_PRN`| chaîne de caractères |_||
`IPP_SEX_COD`| chaîne de caractères |Code sexe du PS||
`LAB_CAT_COD`| chaîne de caractères |Catégorie de laboratoire||
`PFS_ACP_DSD`| chaîne de caractères |Date début de l'activité libérale||
`PFS_AMB_NBR`| chaîne de caractères |Effectif du véhicule de type ambulance ||
`PFS_CAT_COD`| chaîne de caractères |Code catégorie de PS||
`PFS_COD_POS`| chaîne de caractères |Code postal du cabinet du PS||
`PFS_CPL_ADR`| chaîne de caractères |_||
`PFS_CPL_VOI`| chaîne de caractères |_||
`PFS_EXC_COM`| chaîne de caractères |Code commune du cabinet du PS||
`PFS_EXC_NOM`| chaîne de caractères |_||
`PFS_FIN_NUM`| chaîne de caractères |N° FINESS||
`PFS_INS_DSD`| chaîne de caractères |date d'installation d'un P.S ||
`PFS_LIB_COM`| chaîne de caractères |Libellé de la commune du cabinet du PS||
`PFS_LIB_VOI`| chaîne de caractères |_||
`PFS_LIE_DIT`| chaîne de caractères |_||
`PFS_MAJ_DAT`| chaîne de caractères |Mois de dernière mise à jour||
`PFS_NUM_VOI`| chaîne de caractères |_||
`PFS_PFS_CLE`| chaîne de caractères |_||
`PFS_PFS_NUM`| chaîne de caractères |N° identifiant PS||
`PFS_PRA_SPE`| chaîne de caractères |Code spécialité||
`PFS_SCV_COD`| chaîne de caractères |Code association PS||
`PFS_SPE_ANT`| chaîne de caractères |Code spécialité antérieure||
`PFS_TXI_NBR`| chaîne de caractères |Effectif du véhicule de type Taxi ||
`PFS_TYP_VOI`| chaîne de caractères |_||
`PFS_VSL_NBR`| chaîne de caractères |Effectif du véhicule de type VSL ||
`PRA_CIV_COD`| chaîne de caractères |Code civilité du PS ||
`PRA_DIP_NBR`| chaîne de caractères |Nb de salariés diplômés ||
`PRA_IDP_DDP`| chaîne de caractères |Droit à dépassement ||
`PRA_IDP_SAL`| chaîne de caractères |Top salarié ||
`PRA_MEP_COD`| chaîne de caractères |Mode d'exercice particulier||
`PRA_SAL_NBR`| chaîne de caractères |Nb de salariés non diplômés ||
`PRA_SAL_SPE1`| chaîne de caractères |Spécialité salariés 1 ||
`PRA_SAL_SPE2`| chaîne de caractères |Spécialité salariés 2 ||
`PRA_SAL_SPE3`| chaîne de caractères |Spécialité salariés 3 ||
`PRA_SAL_SPE4`| chaîne de caractères |Spécialité salariés 4 ||
`PRA_SAL_SPE5`| chaîne de caractères |Spécialité salariés 5 ||
`PRA_SAL_SPE6`| chaîne de caractères |Spécialité salariés 6 ||
`PRA_TOP_REF`| chaîne de caractères |Code options conventionnelles ||
`PRS_CTP_COD`| chaîne de caractères |_||
`PRS_CTP_DSD`| chaîne de caractères |_||
`PRS_SIR_NUM`| chaîne de caractères |_||
`STA_CAI_NUM`| chaîne de caractères |Caisse de chainage statistique||
`STA_CTR_NUM`| chaîne de caractères |CTR DE CHAINAGE ||
`STA_PFS_NUM`| chaîne de caractères |N° PS statistique||
`T733_CTR_NUM`| chaîne de caractères |CTR de rattachement ||
`T733_STA_URC`| chaîne de caractères |URCAM DE CHAINAGE STATISTIQUE ||
`TRT_SNI_COD`| chaîne de caractères |Code traitement SNIR PS ||