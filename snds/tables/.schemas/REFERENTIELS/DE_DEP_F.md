### Schéma


- Titre : Table des dépistages COVID-19
<br />



- Clé(s) étrangère(s) : <br />
`BEN_NIR_ANO`=> table [IR_BEN_R](/tables/IR_BEN_R)[ `BEN_NIR_ANO` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ADL_POS_COD`| chaîne de caractères |Code postal associé au N° ADELI||
`BEN_AGE_COD`| nombre entier |Age du patient testé (en année)||
`BEN_HBG_COD`| chaîne de caractères |Code hébergement du patient||
`BEN_NIR_ANO`| chaîne de caractères |Identifiant pseudonymisé bénéficiaire ||
`BEN_ORD_NUM`| chaîne de caractères |Numéro d’ordre du bénéficiaire dépisté||
`BEN_SEX_COD`| nombre entier |Sexe du patient testé||
`BEN_SYM_PER`| chaîne de caractères |Période des premiers symptômes du bénéficiaire||
`DOS_STA_COD`| chaîne de caractères |Code statut du dossier  ||
`ETB_CAT_COD`| nombre entier |Code catégorie de l’établissement||
`ETB_EXE_FIN`| chaîne de caractères |N° FINESS géographique de l’établissement||
`ETB_STJ_FIN`| chaîne de caractères |N° FINESS juridique de l’établissement||
`EXE_SOI_AMD`| chaîne de caractères |Date de prélèvement en année-mois||
`EXE_SOI_DTD`| nombre entier |Date de prélèvement du test||
`JO2_COV_INF`| chaîne de caractères |Joker 2||
`JO3_COV_INF`| chaîne de caractères |Joker 3||
`PRA_ADL_LIB`| chaîne de caractères |Libellé de la profession associée au N° ADELI||
`PRA_ADL_NUM`| chaîne de caractères |N° ADELI du PS ||
`PSE_RPS_NUM`| chaîne de caractères |N° RPPS du PS exécutant||
`PSP_RPS_NUM`| chaîne de caractères |N° RPPS du PS prescripteur||
`TES_CAM_COD`| chaîne de caractères |Code de campagne du test||
`TES_DPT_COD`| chaîne de caractères |Code département du test||
`TES_PAY_COD`| chaîne de caractères |Code pays du test||
`TES_POS_COD`| nombre entier |Code postal du test||
`TES_REG_COD`| nombre entier |Code région du test||
`TES_RES_COD`| chaîne de caractères |Résultat recodé du test||
`TES_RPS_NUM`| chaîne de caractères |N° RPPS du PS du test||
`TES_TYP_COD`| chaîne de caractères |Code de type de test réalisé||
`TES_VAL_AMD`| chaîne de caractères |Date validation du test en année-mois||
`TES_VAL_DTD`| nombre entier |Date validation du test||
`TMP_DPT_COD`| chaîne de caractères |Code département de l’adresse temporaire du test||
`TMP_PAY_COD`| chaîne de caractères |Code pays de l’adresse temporaire du test||
`TMP_POS_COD`| chaîne de caractères |Code postal de l’adresse temporaire du test||