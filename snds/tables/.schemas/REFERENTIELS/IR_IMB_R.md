### Schéma


- Titre : Table du Référentiel médicalisé
<br />



- Clé(s) étrangère(s) : <br />
`BEN_NIR_PSA, BEN_RNG_GEM`=> table [IR_BEN_R](/tables/IR_BEN_R)[ `BEN_NIR_PSA`, `BEN_RNG_GEM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`BEN_NIR_PSA`| chaîne de caractères |Identifiant anonyme du patient dans le SNIIRAM||
`BEN_RNG_GEM`| nombre entier |Rang du bénéficiaire||
`IMB_ALD_DTD`| date |Date de début d'exonération du ticket modérateur attribuée par les services médicaux (ALD, AT, MP…)||
`IMB_ALD_DTF`| date |Date de fin de l'exonération attribuée par les services médicaux (ALD, AT, MP…)||
`IMB_ALD_NUM`| nombre entier |Numéro d'ALD||
`IMB_ETM_NAT`| nombre entier |Motif d'exonération du bénéficiaire||
`IMB_MLP_BTR`| chaîne de caractères |Tableau des maladies professionnelles (bis, ter)||
`IMB_MLP_TAB`| chaîne de caractères |Numéro de tableau des maladies professionnelles (MP)||
`IMB_SDR_LOP`| chaîne de caractères |Localisation / paragraphe syndrome||
`INS_DTE`| date |Date d'insertion||
`MED_MTF_COD`| chaîne de caractères |Motif médical ou pathologie (code CIM 10)||
`MED_NCL_IDT`| chaîne de caractères |Identification de la nomenclature médicale||
`UPD_DTE`| date |Date de mise à jour||