### Schéma


- Titre : Table historique de la maternité
<br />



- Clé(s) étrangère(s) : <br />
`BEN_NIR_PSA, BEN_RNG_GEM`=> table [IR_BEN_R](/tables/IR_BEN_R)[ `BEN_NIR_PSA`, `BEN_RNG_GEM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`BEN_DRA_AME`| année et mois |Année et mois de l’accouchement||
`BEN_GRS_DTE`| date |Date présumée de grossesse||
`BEN_IDT_ANO`| chaîne de caractères |Identifiant bénéficiaire anonymisé||
`BEN_NIR_ANO`| chaîne de caractères |NIR pseudonymisé du bénéficiaire||
`BEN_NIR_PSA`| chaîne de caractères |Identifiant anonyme du patient dans le SNIIRAM||
`BEN_RNG_GEM`| nombre entier |Rang de naissance du bénéficiaire||
`CTO_IDT_ANO`| chaîne de caractères |Identifiant Cartographie||
`IND_RNM_BEN`| chaîne de caractères |Top RNIAM||