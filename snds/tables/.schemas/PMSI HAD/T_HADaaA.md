### Schéma


- Titre : Table des actes
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RHAD_NUM`=> table [T_HADaaB](/tables/T_HADaaB)[ `ETA_NUM_EPMSI`, `RHAD_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACT_COD`| chaîne de caractères |Code de l'activité||
`CCAM_COD`| chaîne de caractères |Code de l'acte CCAM (hors extension PMSI)||
`EXT_PMSI`| chaîne de caractères |Extension PMSI||
`ETA_NUM_EPMSI`| chaîne de caractères |N° FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS de l’établissement (code géographique)||
`EXT_DOC`| chaîne de caractères |Extension documentaire||
`PHA_COD`| chaîne de caractères |Code de la phase||
`DEL_DAT_ENT`| chaîne de caractères |Délai depuis le début du séjour||
`REAL_NBR`| nombre entier |Nombre de réalisations||
`RHAD_NUM`| chaîne de caractères |N° séquentiel de séjour d'HAD||
`SEQ_NUM`| chaîne de caractères |N° de la séquence dans le séjour||
`SSEQ_NUM`| chaîne de caractères |N° de la sous-séquence||
`VAL_DAT_SSEQ`| chaîne de caractères |Dates de validité de l'acte compatibles avec les dates de la sousséquence||
`NBR_ACT`| chaîne de caractères |Nombre d'actes réalisés ||
`ETA_NUM_TWO`| chaîne de caractères |Numéro FINESS de l’établissement (code géographique)||
`DEL_DEB_SSEQ`| nombre entier |Délai depuis le début de la sousséquence||