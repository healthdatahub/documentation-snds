### Schéma


- Titre : Fichier donnant toutes les erreurs détectées par la fonction groupage (LEG)
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_ePMSI, RHAD_NUM`=> table [T_HADaaB](/tables/T_HADaaB)[ `ETA_NUM_EPMSI`, `RHAD_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM_ePMSI`| chaîne de caractères |N° FINESS e-PMSI||
`MOI`| date |Mois période||
`ANN`| chaîne de caractères |Année période||
`RHAD_NUM`| chaîne de caractères |N° séquentiel de séjour d'HAD/ N° séquentiel d'entrée||
`FAC_NUM`| chaîne de caractères |N° séquentiel de facture||
`SEQ_NUM`| chaîne de caractères |Numéro de la séquence dans le séjour||
`SSEQ_NUM`| chaîne de caractères |Numéro de sous-séquence||
`COD_ERR`| chaîne de caractères |Code retour||