### Schéma


- Titre : Table Tests diagnostiques Covid
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_HADaaE](/tables/T_HADaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS ePMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`ANN_MOI`| date |Année et Mois de la période de transmission||
`TYP_TEST`| chaîne de caractères |Type de tests||
`ACT_COD`| chaîne de caractères |Code de l'acte||
`PERIOD_ACT`| nombre entier |Période d'execution ||
`PMP`| nombre entier |Prix moyen pondéré par l'effectif des autotests du personnel non vacciné du 08/08/2021 au 15/10/2021 ||