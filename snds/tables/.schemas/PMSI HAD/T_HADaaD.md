### Schéma


- Titre : Table des diagnostics associés
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RHAD_NUM`=> table [T_HADaaB](/tables/T_HADaaB)[ `ETA_NUM_EPMSI`, `RHAD_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`DGN_ASS`| chaîne de caractères |Diagnostic associé||
`ETA_NUM_EPMSI`| chaîne de caractères |N° FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS de l’établissement (code géographique)||
`RHAD_NUM`| chaîne de caractères |N° séquentiel de séjour d'HAD||
`SEQ_NUM`| chaîne de caractères |N° de la séquence dans le séjour||
`SSEQ_NUM`| chaîne de caractères |N° de la sous-séquence||
`FAC_NUM`| chaîne de caractères |Numéro de facture||
`ETA_NUM_TWO`| chaîne de caractères |Numéro FINESS de l’établissement (code géographique)||