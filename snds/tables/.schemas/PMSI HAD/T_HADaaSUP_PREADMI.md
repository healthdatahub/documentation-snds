### Schéma


- Titre :  (Evaluations anticipées pour les patients résidant en EHPAD )
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI`=> table [T_HADaaE](/tables/T_HADaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS ePMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS géographique||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`ANN`| chaîne de caractères |Année de la période de transmission||
`MOIS`| date |Mois de la période de transmission||
`NBR_PAT_EPAD`| chaîne de caractères |Nombre de patients en EHPAD||
`NBR_PAT_ESMS`| chaîne de caractères |Nombre de patients dans les autres ESMS (hors EHPAD)||
`NBR_PAT_ETSO`| chaîne de caractères |Nombre de patients dans les établissements sociaux ||
`NBR_PAT`| chaîne de caractères |Nombre de patients||