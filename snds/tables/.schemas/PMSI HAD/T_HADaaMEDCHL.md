### Schéma


- Titre : Fich comp Médicament coûteux hors liste en sus et hors ATU
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RHAD_NUM`=> table [T_HADaaB](/tables/T_HADaaB)[ `ETA_NUM_EPMSI`, `RHAD_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACH_PRI_ADM`| nombre entier |Prix d'achat multiplié par le nombre administré||
`ADM_ANN`| année |Année de la date d'administration||
`ADM_MOIS`| date |Mois de la date d'administration||
`ADM_NBR`| nombre entier |Nombre administré éventuellement fractionnaire||
`ANN`| chaîne de caractères |Année période||
`DAT_DELAI`| nombre entier |Délai entre la date d’entrée du séjour et la date de dispensation||
`ETA_NUM_EPMSI`| chaîne de caractères |N° FINESS e-PMSI||
`FAC_NUM`| chaîne de caractères |N° séquentiel de facture||
`INI_VAL_PRS`| chaîne de caractères |Validation initiale de la prescription par un centre de référence ou de compétence||
`MOIS`| date |N° période (mois)||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`RHAD_NUM`| chaîne de caractères |N° séquentiel de séjour d'HAD||
`SEJ_NBR`| nombre entier |Nombre de séjours impliqués||
`SEQ_SEJ`| chaîne de caractères |Numéro de la séquence dans le séjour||
`SSEQ_NUM`| chaîne de caractères |Numéro de sous-séquence||
`TOP_UCD_AUTO`| chaîne de caractères |Top transcodage UCD13 auto||
`UCD_UCD_COD`| chaîne de caractères |Code UCD||