### Schéma


- Titre : Table des conventions HAD-EHPA
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI`=> table [T_HADaaE](/tables/T_HADaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN`| chaîne de caractères |Année période||
`DAT_DEB_CON`| chaîne de caractères |Date de début de la convention||
`DAT_FIN_CON`| chaîne de caractères |Date de fin de la convention||
`ETA_NUM_ESMS`| chaîne de caractères |N° FINESS ESMS||
`ETA_NUM_EPMSI`| chaîne de caractères |N° FINESS e-PMSI||
`FFT_COD`| chaîne de caractères |Code forfait de soins||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`MOI`| date |N° période (mois)||
`ETA_TYP_ESMS`| chaîne de caractères |Type d'établissement||
`COM_ETA`| chaîne de caractères |Commentaires||
`MOIS`| date |N° période (mois)||
`ETA_NUM_EHPA`| chaîne de caractères |N° FINESS ESMS||