### Schéma


- Titre : HAD Valorisation
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RHAD_NUM`=> table [T_HADaaB](/tables/T_HADaaB)[ `ETA_NUM_EPMSI`, `RHAD_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |finess||
`RHAD_NUM`| chaîne de caractères |seqhad||
`SEQ_NUM`| chaîne de caractères |numseq||
`SSEQ_NUM`| chaîne de caractères |numssseq||
`VALO`| chaîne de caractères |valo||
`motif_non_valo`| chaîne de caractères |motif_non_valo||
`ght1`| chaîne de caractères |ght1||
`ght2`| chaîne de caractères |ght2||
`ght3`| chaîne de caractères |ght3||
`ght4`| chaîne de caractères |ght4||
`taux`| chaîne de caractères |taux||
`mnt_ght_br`| nombre entier |mnt_ght_br||
`mnt_ght_am`| nombre entier |mnt_ght_am||
`nbj1`| nombre entier |nbj1||
`nbj2`| nombre entier |nbj2||
`nbj3`| nombre entier |nbj3||
`nbj4`| nombre entier |nbj4||
`coef_had`| nombre entier |coef_had||
`mnt_mon`| nombre entier |mnt_mon||
`mnt_med_apac`| nombre entier |mnt_med_apac||
`mnt_tot_br`| nombre entier |mnt_tot_br||
`mnt_tot_am`| nombre entier |mnt_tot_am||