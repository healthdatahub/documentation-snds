### Schéma


- Titre : Table de passage de Finess
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_PMSI`=> table [T_HADaaE](/tables/T_HADaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM_GEO`| chaîne de caractères |N° FINESS de l’entité juridique||
`ETA_NUM_JUR`| chaîne de caractères |N° FINESS de l’établissement (code géographique)||
`ETA_NUM_PMSI`| chaîne de caractères |N° FINESS e-PMSI||