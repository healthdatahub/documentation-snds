### Schéma


- Titre : Table FL : Table des actes de biologie (issus de la NABM) des établissements ex-OQN
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RHAD_NUM`=> table [T_HADaaB](/tables/T_HADaaB)[ `ETA_NUM_EPMSI`, `RHAD_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACT_NBR`| nombre entier |Quantité acte||
`ANN_MOI`| date |Mois et année de l'acte||
`DEL_DAT_ENT`| nombre entier |Delai par rapport à la date d'entrée rsfa||
`ETA_NUM_EPMSI`| chaîne de caractères |N° FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS  géographique||
`EXE_SOI_DTD`| date |Date de l'acte 1 à 5||
`FAC_NUM`| chaîne de caractères |N° Facture séquentiel||
`NABM_COD`| chaîne de caractères |Code acte NABM||
`PSH_DMT`| chaîne de caractères |Discipline de prestation (ex DMT)||
`PSH_MDT`| chaîne de caractères |Mode de traitement||
`RHAD_NUM`| chaîne de caractères |N° séquentiel||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`EXE_SOI_AMD`| chaîne de caractères |Date de l'acte||
`ETA_NUM_TWO`| chaîne de caractères |Numéro FINESS de l’établissement (site géographique)||
`ANN_MOI`| date |Mois et année de l'acte||