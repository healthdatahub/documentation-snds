### Schéma


- Titre : Fichier de déclaration agrégée d'activité (FICHSUP) sur les Molécules onéreuses
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI`=> table [T_HADaaE](/tables/T_HADaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACH_PRI_DER`| chaîne de caractères |Dernier prix d'achat||
`ANN`| chaîne de caractères |Année période||
`ETA_NUM_EPMSI`| chaîne de caractères |N° FINESS e-PMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`MOIS`| date |N° période (mois)||
`MOY_PRI`| chaîne de caractères |Prix moyen||
`UCD_COD`| chaîne de caractères |Code UCD||
`UCD_NBR_ETH`| chaîne de caractères |Nombre d'UCD dispensées au titre des essais thérapeutiques||
`UCD_NBR_TOT`| chaîne de caractères |Nombre d'UCD dispensées totales||
`UCD_UCD_COD`| chaîne de caractères |Code UCD||