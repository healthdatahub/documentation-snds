### Schéma


- Titre : Table des GHT et des GHPC (groupage paprica)
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RHAD_NUM`=> table [T_HADaaB](/tables/T_HADaaB)[ `ETA_NUM_EPMSI`, `RHAD_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM_EPMSI`| chaîne de caractères |N° FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS de l’établissement (code géographique)||
`ETE_GHS_NUM`| nombre entier |GHT_NUM en numérique||
`GHT_NBJ`| nombre entier |Groupage PAPRICA : nombre de jours||
`GHT_NUM`| chaîne de caractères |Groupage PAPRICA : n° du GHT||
`PAP_GRP_GHPC`| chaîne de caractères |Groupage PAPRICA : n° du GHPC||
`PAP_GRP_RET`| chaîne de caractères |Groupage PAPRICA : code retour||
`PAP_GRP_VER`| chaîne de caractères |Groupage établissement : version de la classification||
`RHAD_NUM`| chaîne de caractères |N° séquentiel de séjour d'HAD||
`SEQ_NUM`| chaîne de caractères |N° de la séquence dans le séjour||
`SSEQ_NUM`| chaîne de caractères |N° de la sous-séquence||
`FAC_NUM`| chaîne de caractères |Numéro de facture||
`ETA_NUM_GEO`| chaîne de caractères |N° FINESS de l’entité juridique||
`ETA_NUM_JUR`| chaîne de caractères |N° FINESS de l’établissement (code géographique)||
`SEQ_SEJ`| chaîne de caractères |Numéro de la séquence dans le séjour||
`ETA_NUM_TWO`| chaîne de caractères |Numéro FINESS de l’établissement (code géographique)||