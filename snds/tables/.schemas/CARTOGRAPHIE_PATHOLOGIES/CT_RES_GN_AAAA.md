### Schéma


- Titre : Informations contextuelles relatives au lieu de résidence des patients de l'année AAAA et l'algorithme N
<br />



- Clé(s) étrangère(s) : <br />
`BEN_IDT_ANO`=> table [CT_IDE_GN_AAAA](/tables/CT_IDE_GN_AAAA)[ `BEN_IDT_ANO` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`APL_MG_COMMUNE`| chaîne de caractères |Coefficient de l'accessibilité potentiel localisé aux médecins généralistes correspondant à la commune||
`APL_MG_INF65_COMMUNE`| chaîne de caractères |Coefficient de l'accessibilité potentiel localisé aux médecins généralistes de moins de 65 ans correspondant à la commune||
`BEN_IDT_ANO`| chaîne de caractères |Identifiant synthétique du bénéficiaire dans le SNDS||
`CODE_ARRONDISSEMENT_INSEE`| chaîne de caractères |Code de l'arrondissement correspondant à la commune||
`CODE_BASSIN_DE_VIE`| chaîne de caractères |Code du bassin de vie correspondant à la commune (2012)||
`CODE_CANTON_INSEE`| chaîne de caractères |Code du canton correspondant à la commune||
`CODE_COMMUNE_INSEE`| chaîne de caractères |Code officiel géographique INSEE de la commune||
`CODE_DEPARTEMENT_INSEE`| chaîne de caractères |Code du département correspondant à la commune||
`CODE_REGION_INSEE`| chaîne de caractères |Code de la région correspondant à la commune||
`CODE_TVS`| chaîne de caractères |Code du territoire de vie-santé correspondant à la commune||
`CONCENTR_MOY_PM10_REG`| chaîne de caractères |Concentrations moyennes annuelles de particules fines (PM 10) dans les villes (région)||
`ESPER_VIE_FEMME_DEP`| chaîne de caractères |Espérance de vie à la naissance des femmes (département)||
`ESPER_VIE_FEMME_REG`| chaîne de caractères |Espérance de vie à la naissance des femmes (région)||
`ESPER_VIE_HOMME_DEP`| chaîne de caractères |Espérance de vie à la naissance des hommes (département)||
`ESPER_VIE_HOMME_REG`| chaîne de caractères |Espérance de vie à la naissance des hommes (région)||
`INDICE_DEFAVORISATION_2015`| chaîne de caractères |Indice de défavorisation sociale (2015)||
`INTENSITE_PVT_DEP`| chaîne de caractères |Intensité de la pauvreté (département)||
`INTENSITE_PVT_REG`| chaîne de caractères |Intensité de la pauvreté (région)||
`LIB_ARRONDISSEMENT_INSEE`| chaîne de caractères |Nom de l'arrondissement correspondant à la commune||
`LIB_BASSIN_DE_VIE`| chaîne de caractères |Nom du bassin de vie correspondant à la commune (2012)||
`LIB_CANTON_INSEE`| chaîne de caractères |Nom du canton correspondant à la commune||
`LIB_COMMUNE_INSEE`| chaîne de caractères |Libellé correspondant au code officiel géographique INSEE de la commune||
`LIB_DEPARTEMENT_INSEE`| chaîne de caractères |Nom du département correspondant à la commune||
`LIB_REGION_INSEE`| chaîne de caractères |Nom de la région correspondant à la commune||
`LIB_TVS`| chaîne de caractères |Nom du territoire de vie-santé correspondant à la commune||
`MEDIANE_NIVEAU_DE_VIE`| chaîne de caractères |Médiane du niveau de vie correspondant à la commune||
`PART_20_24_SORTIS_NNDIP_COM`| chaîne de caractères |Part des non-diplômés chez les 20-24 ans sortis d'études (commune)||
`PART_20_24_SORTIS_NNDIP_DEP`| chaîne de caractères |Part des non-diplômés chez les 20-24 ans sortis d'études (département)||
`PART_20_24_SORTIS_NNDIP_REG`| chaîne de caractères |Part des non-diplômés chez les 20-24 ans sortis d'études (région)||
`PART_APA_PLUS75_DEP`| chaîne de caractères |Part des personnes de 75 ans ou plus bénéficiant de l'Aide Personnalisée d'Autonomie (APA, département)||
`PART_APA_PLUS75_REG`| chaîne de caractères |Part des personnes de 75 ans ou plus bénéficiant de l'Aide Personnalisée d'Autonomie (APA, région)||
`PART_CHOMEURS_LD_COM`| chaîne de caractères |Part des chômeurs de longue durée parmi les demandeurs d'emploi (commune)||
`PART_CHOMEURS_LD_DEP`| chaîne de caractères |Part des chômeurs de longue durée parmi les demandeurs d'emploi (département)||
`PART_CHOMEURS_LD_REG`| chaîne de caractères |Part des chômeurs de longue durée parmi les demandeurs d'emploi (région)||
`PART_JEUNES_NON_INSERES_COM`| chaîne de caractères |Part des jeunes (18-25 ans) non insérés (hors emploi et hors système scolaire, commune)||
`PART_JEUNES_NON_INSERES_DEP`| chaîne de caractères |Part des jeunes (18-25 ans) non insérés (hors emploi et hors système scolaire, département)||
`PART_JEUNES_NON_INSERES_REG`| chaîne de caractères |Part des jeunes (18-25 ans) non insérés (hors emploi et hors système scolaire, région)||
`PART_LOG_RES_PRINC_COM`| chaîne de caractères |Part des logements de type résidence principale (commune)||
`PART_LOG_RES_PRINC_DEP`| chaîne de caractères |Part des logements de type résidence principale (département)||
`PART_LOG_RES_SEC_COM`| chaîne de caractères |Part des logements de type résidence secondaire (commune)||
`PART_LOG_RES_SEC_DEP`| chaîne de caractères |Part des logements de type résidence secondaire (département)||
`PART_PLS_COM`| chaîne de caractères |Part des logements sociaux dans l'ensemble des logements (commune)||
`PART_PLS_DEP`| chaîne de caractères |Part des logements sociaux dans l'ensemble des logements (département)||
`PART_POP75_COM`| chaîne de caractères |Part des 75 ans ou plus dans la population (commune)||
`PART_POP75_DEP`| chaîne de caractères |Part des 75 ans ou plus dans la population (département)||
`PART_POP75_REG`| chaîne de caractères |Part des 75 ans ou plus dans la population (région)||
`POIDS_PRESTA_REVENU_REG`| chaîne de caractères |Poids des prestations sociales dans le revenu disponible moyen (région)||
`POPULATION_DERNIER_RECENSEMENT`| chaîne de caractères |Population du dernier recensement correspondant à la commune||
`POPULATION_MUNICIPALE`| chaîne de caractères |Population légale correspondant à la commune||
`QUINTILE_DEFAVORISATION_2015`| chaîne de caractères |Quintile de défavorisation sociale (2015)||
`REVENU_DECL_MEDIAN_DEP`| chaîne de caractères |Revenu fiscal déclaré médian par unité de consommation (département)||
`REVENU_DECL_MEDIAN_REG`| chaîne de caractères |Revenu fiscal déclaré médian par unité de consommation (région)||
`STATUT_COMMUNE`| chaîne de caractères |Statut correspondant à la commune||
`TAUX_CHOM_BIT_FEMME_DEP`| chaîne de caractères |Taux de chômage des femmes (au sens du BIT, département)||
`TAUX_CHOM_BIT_FEMME_REG`| chaîne de caractères |Taux de chômage des femmes (au sens du BIT, région)||
`TAUX_CHOM_BIT_HOMME_DEP`| chaîne de caractères |Taux de chômage des hommes (au sens du BIT, département)||
`TAUX_CHOM_BIT_HOMME_REG`| chaîne de caractères |Taux de chômage des hommes (au sens du BIT, région)||
`TAUX_CHOM_BIT_TOTAL_DEP`| chaîne de caractères |Taux de chômage (au sens du BIT, département)||
`TAUX_CHOM_BIT_TOTAL_REG`| chaîne de caractères |Taux de chômage (au sens du BIT, région)||
`TAUX_CHOM_RP_TOTAL_COM`| chaîne de caractères |Taux de chômage (au sens du recensement, commune)||
`TAUX_CHOM_RP_TOTAL_DEP`| chaîne de caractères |Taux de chômage (au sens du recensement, département)||
`TAUX_CHOM_RP_TOTAL_REG`| chaîne de caractères |Taux de chômage (au sens du recensement, région)||
`TAUX_EMP_F_COM`| chaîne de caractères |Taux d'emploi des femmes (commune)||
`TAUX_EMP_F_DEP`| chaîne de caractères |Taux d'emploi des femmes (département)||
`TAUX_EMP_F_REG`| chaîne de caractères |Taux d'emploi des femmes (région)||
`TAUX_EMP_H_COM`| chaîne de caractères |Taux d'emploi des hommes (commune)||
`TAUX_EMP_H_DEP`| chaîne de caractères |Taux d'emploi des hommes (département)||
`TAUX_EMP_H_REG`| chaîne de caractères |Taux d'emploi des hommes (région)||
`TAUX_EMP_TOTAL_COM`| chaîne de caractères |Taux d'emploi des individus (commune)||
`TAUX_EMP_TOTAL_DEP`| chaîne de caractères |Taux d'emploi des individus (département)||
`TAUX_EMP_TOTAL_REG`| chaîne de caractères |Taux d'emploi des individus (région)||
`TAUX_LGMT_SUROCCUPES_COM`| chaîne de caractères |Part des logements en situation de sur-occupation (commune)||
`TAUX_LGMT_SUROCCUPES_DEP`| chaîne de caractères |Part des logements en situation de sur-occupation (département)||
`TAUX_LGMT_SUROCCUPES_REG`| chaîne de caractères |Part des logements en situation de sur-occupation (région)||
`TAUX_MORT_INFANT_DEP`| chaîne de caractères |Taux de mortalité infantile (pour 1000 enfants nés vivants entre 2015 et 2017, département)||
`TAUX_MORT_INFANT_REG`| chaîne de caractères |Taux de mortalité infantile (pour 1000 enfants nés vivants entre 2015 et 2017, région)||
`TAUX_PAUVRETE`| chaîne de caractères |Taux de pauvreté correspondant à la commune||
`TAUX_PVT_TOTAL_DEP`| chaîne de caractères |Taux de pauvreté monétaire (60 % du niveau de vie médian) total (département)||
`TAUX_PVT_TOTAL_REG`| chaîne de caractères |Taux de pauvreté monétaire (60 % du niveau de vie médian) total (région)||
`TX_JEUNES_DIFF_LECTURE_DEP`| chaîne de caractères |Taux de jeunes en difficulté de lecture (16-17 ans, département)||
`TX_JEUNES_DIFF_LECTURE_REG`| chaîne de caractères |Taux de jeunes en difficulté de lecture (16-17 ans, région)||
`TYPE_COMMUNE`| chaîne de caractères |Type correspondant à la commune||
`TYPOLOGIE_DENSITE_4NIV`| chaîne de caractères |Type de densité correspondant à la commune (4 niveaux)||