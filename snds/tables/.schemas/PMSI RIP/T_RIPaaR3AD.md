### Schéma


- Titre : Diagnostic associé à l'acte
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, ORD_NUM, SEJ_IDT`=> table [T_RIPaaR3A](/tables/T_RIPaaR3A)[ `ETA_NUM_EPMSI`, `ORD_NUM`, `SEJ_IDT` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM_EPMSI`| chaîne de caractères |N° FINESS e-PMSI||
`ETA_NUM_TWO`| chaîne de caractères |Second n° FINESS||
`SEJ_IDT`| chaîne de caractères |Identifiant de séjour||
`IPP_IRR_CRY`| chaîne de caractères |Cryptage irreversible de l'IPP||
`ORD_NUM`| chaîne de caractères |N° d'ordre||
`ASS_DGN`| chaîne de caractères |Diagnostics et facteurs associés||