### Schéma


- Titre : Fichier d’information Transports inter-établissements
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_RIPaaE](/tables/T_RIPaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |N° FINESS e-PMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`CL_DISTANCE`| nombre entier |Classe distance||
`NBR_SUP_TRSP`| nombre entier |Nombre suppléments||
`ANN_MOI`| date |Année +Moi de la période de transmission||
`ACT_COD`| chaîne de caractères |Code||
`NBR_SEJ`| chaîne de caractères |Nombre de séjours||