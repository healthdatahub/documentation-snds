### Schéma


- Titre : prothese
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RIP_NUM`=> table [T_RIPaaFB](/tables/T_RIPaaFB)[ `ETA_NUM_EPMSI`, `RIP_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS de l’entité juridique||
`ETA_NUM_TWO`| chaîne de caractères |Numéro FINESS de l’établissement (site géographique)||
`RIP_NUM`| chaîne de caractères |Numéro séquentiel de séjour (idem RPSA)||
`FAC_NUM`| chaîne de caractères |N° séquentiel de facture||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`LPP_COD`| chaîne de caractères |Code référence LPP||
`EXE_SOI_DTD`| date |Date soins||
`LPP_QUA`| nombre entier |Quantité||
`LPP_PU_DEV`| number |Tarif référence LPP/ Prix Unitaire sur devis||
`FAC_MNT`| number |Montant total facturé||
`LPP_PRI_UNI`| number |Prix d'achat unitaire||
`MNT_UNI_ECA`| number |Montant unitaire de l'écart indemnisable||
`TOT_MNT_ECA`| number |Montant total de l'écart indemnisable||
`TIP_PRS_IDE`| nombre entier |Code référence LPP (format numérique)||
`ENT_MOI`| date |Mois de la date de début de séjour||
`ENT_ANN`| année |Année de la date de début de séjour||