### Schéma


- Titre : honoraire
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RIP_NUM`=> table [T_RIPaaFB](/tables/T_RIPaaFB)[ `ETA_NUM_EPMSI`, `RIP_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS de l’entité juridique||
`ETA_NUM_TWO`| chaîne de caractères |Numéro FINESS de l’établissement (site géographique)||
`RIP_NUM`| chaîne de caractères |Numéro séquentiel de séjour (idem RPSA)||
`FAC_NUM`| chaîne de caractères |N° séquentiel de facture||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`PSH_MDT`| chaîne de caractères |Mode de traitement||
`PSH_DMT`| chaîne de caractères |Discipline de prestation (ex DMT)||
`EXO_TM`| chaîne de caractères |Justification exo TM||
`ACT_COD`| chaîne de caractères |Code acte||
`ACT_NBR`| nombre entier |Quantité||
`ACT_COE`| number |Cœfficient||
`ACT_DNB`| nombre entier |Dénombrement||
`PRI_UNI`| number |Prix Unitaire||
`REM_BAS`| number |Montant Base remboursement||
`REM_TAU`| nombre entier |Taux Remboursement||
`AMO_MNR`| number |Montant Remboursable par AMO||
`HON_MNT`| number |Montant des honoraire (dépassement compris)||
`AMC_MNR`| number |Montant remboursable par AMC||
`EXE_SOI_DTD`| date |Date l'acte||
`NOE_MNR`| number |Montant remboursé NOEMIE Retour||
`NOE_OPE`| chaîne de caractères |Nature opération récupération NOEMIE Retour||
`ACT_MOI`| date |Mois de la date de l'acte||
`ACT_ANN`| année |Année de la date de l'acte||