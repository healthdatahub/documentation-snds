### Schéma


- Titre : debut facture
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RIP_NUM`=> table [T_RIPaaFB](/tables/T_RIPaaFB)[ `ETA_NUM_EPMSI`, `RIP_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS de l’entité juridique||
`ETA_NUM_TWO`| chaîne de caractères |Numéro FINESS de l’établissement (site géographique)||
`AGE_JOU `| nombre entier |Age jours (pour les moins un an)||
`BDI_COD`| chaîne de caractères |Code géographique||
`COD_POST`| chaîne de caractères |Code postal||
`EXE_SOI_DTD`| date |Date d'entrée séjour||
`EXE_SOI_DTF`| date |Date sortie séjour||
`RIP_NUM`| chaîne de caractères |Numéro séquentiel de séjour (idem RPSA)||
`FAC_NUM`| chaîne de caractères |N° séquentiel de facture||
`COD_SEX`| chaîne de caractères |Sexe||
`AGE_ANN`| année |Age (en années)||
`COD_CIV`| chaîne de caractères |Code civilité ||
`RNG_BEN`| chaîne de caractères |Rang de bénéficiaire||
`OPE_NAT`| chaîne de caractères |Nature opération||
`NAT_ASS`| chaîne de caractères |Nature assurance||
`CON_TYP`| chaîne de caractères |Type de contrat souscrit auprès d'un organisme complémentaire||
`EXO_TM`| chaîne de caractères |Justification d'exonération du TM||
`COD_PEC`| chaîne de caractères |Code de prise en charge||
`NOE_RGM`| chaîne de caractères |Code Gd régime||
`RNG_NAI`| chaîne de caractères |Rang de naissance||
`PH_BRM`| number |Total Base Remboursement Prestation hospitalière||
`PH_AMO_MNR`| number |Total remboursable AMO Prestation hospitalières||
`HON_MNT`| number |Total honoraire Facturé||
`HON_AM_MNR`| number |Total honoraire remboursable AM||
`PAS_OC_MNT`| number |Total participation assuré avant OC||
`PH_OC_MNR`| number |Total remboursable OC pour les PH||
`HON_OC_MNR`| number |Total remboursable OC pour les honoraires||
`PH_MNT`| number |Montant total facturé pour  PH||
`FAC_ETL`| chaîne de caractères |Etat de liquidation de la facture||
`BEN_CMU`| chaîne de caractères |Patient bénéficiaire de la CMU||
`ORG_CPL_NUM`| chaîne de caractères |N° d’organisme complémentaire||
`NUM_DAT_AT`| chaîne de caractères |Numéro accident du travail ou date d’accident de droit commun||
`ENT_MOI`| date |Mois de la date d'entrée||
`ENT_ANN`| année |Année de la date d'entrée||
`SOR_MOI`| date |Mois de la date de sortie||
`SOR_ANN`| année |Année de la date de sortie||
`SEJ_DUR`| nombre entier |Durée (Date de sortie-date d'entrée)||