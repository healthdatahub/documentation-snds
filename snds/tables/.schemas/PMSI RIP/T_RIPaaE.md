### Schéma


- Titre : etablissement
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_RIPaaFB](/tables/T_RIPaaFB)[ `ETA_NUM_EPMSI` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |N°FINESS||
`SOC_RAI`| chaîne de caractères |Raison sociale||
`REG_ETA`| chaîne de caractères |Région||
`ANN_TRT`| chaîne de caractères |N° du trimestre PMSI transmis||
`STA_ETA`| chaîne de caractères |Statut de l'établissement||
`VAL_ETA`| chaîne de caractères |Validation des données||
`ETB_EXE_FIN`| chaîne de caractères |N°FINESS sans clé||