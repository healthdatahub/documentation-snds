### Schéma


- Titre : Fich comp Transports
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RIP_NUM`=> table [T_RIPaaFB](/tables/T_RIPaaFB)[ `ETA_NUM_EPMSI`, `RIP_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS e-PMSI||
`FOR_NUM`| chaîne de caractères |Numéro format||
`ANN`| chaîne de caractères |Année période transmission||
`MOI`| date |Mois période transmission||
`RIP_NUM`| chaîne de caractères |Numéro séquentiel séjour||
`SEQ_TRSP_NUM`| chaîne de caractères |Numéro sequentiel séquence au sein séjour||
`EXE_SOI_DTD`| date |Date d'aller||
`ACT_COD`| chaîne de caractères |Code||
`CL_DISTANCE`| nombre entier |Classe distance||
`EXE_SOI_RET`| chaîne de caractères |CR Date d'aller||