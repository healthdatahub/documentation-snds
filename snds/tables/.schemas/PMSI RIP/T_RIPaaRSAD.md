### Schéma


- Titre : Diagnostic associé à la séquence
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RIP_NUM, SEQ_SEQ_NUM`=> table [T_RIPaaRSA](/tables/T_RIPaaRSA)[ `ETA_NUM_EPMSI`, `RIP_NUM`, `SEQ_SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM_EPMSI`| chaîne de caractères |N° FINESS e-PMSI||
`ETA_NUM_TWO`| chaîne de caractères |Second n° FINESS||
`IPP_IRR_CRY`| chaîne de caractères |Cryptage irreversible de l'IPP||
`SEJ_IDT`| chaîne de caractères |Identifiant de séjour||
`RIP_NUM`| chaîne de caractères |N° séquentiel de séjour||
`SEQ_SEQ_NUM`| chaîne de caractères |N° séquentiel de séquence au sein du séjour||
`ASS_DGN`| chaîne de caractères |Diagnostics et facteurs associés||