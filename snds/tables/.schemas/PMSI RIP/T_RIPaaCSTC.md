### Schéma


- Titre : ACE NIR/date
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI`=> table [T_RIPaaE](/tables/T_RIPaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS e-PMSI||
`FOR_NUM`| chaîne de caractères |Numéro format||
`FOR_NUM_ORI`| chaîne de caractères |Numéro format d'origine||
`NIR_RET`| chaîne de caractères |Code retour contrôle « Numéro sécurité sociale »||
`NAI_RET`| chaîne de caractères |Code retour contrôle « date  naissance »||
`SEX_RET`| chaîne de caractères |Code retour contrôle « sexe »||
`IPP_BEN_ANO_RET`| chaîne de caractères |Code retour contrôle « n°d'identification permanent patient »||
`COH_NAI_RET`| chaîne de caractères |Code retour contrôle « Cohérence date naissance »||
`COH_SEX_RET`| chaîne de caractères |Code retour contrôle « Cohérence sexe »||
`NIR_ANO_17`| chaîne de caractères |Pseudonyme||
`RNG_NAI_RET`| chaîne de caractères |Code retour contrôle « Rang naissance »||
`RNG_BEN_RET`| chaîne de caractères |Code retour contrôle « Rang bénéficiaire »||
`RNG_NAI`| chaîne de caractères |Rang naissance ||
`RNG_BEN`| chaîne de caractères |Rang bénéficiaire ||
`IPP_BEN_ANO`| chaîne de caractères |Numéro d'identification permanent patient anonymisé||
`IPP_IRR_CRY`| chaîne de caractères |Cryptage irréversible l'IPP||