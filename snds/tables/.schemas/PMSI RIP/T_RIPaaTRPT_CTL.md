### Schéma


- Titre : Fich de contrôle - Transport
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RIP_NUM`=> table [T_RIPaaFB](/tables/T_RIPaaFB)[ `ETA_NUM_EPMSI`, `RIP_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_TWO`| chaîne de caractères |Numéro FINESS géographique||
`MOI`| date |Mois période transmission||
`ANN`| chaîne de caractères |Année période transmission||
`RIP_NUM`| chaîne de caractères |Numéro séquentiel séjour||
`SEQ_TRSP_NUM`| chaîne de caractères |Numéro séquentiel transports||
`COD_ERR`| chaîne de caractères |Code erreur||