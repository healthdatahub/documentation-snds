### Schéma


- Titre : Fich comp Séjours à Temps Partiel
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RIP_NUM, SEQ_SEQ_NUM`=> table [T_RIPaaRSA](/tables/T_RIPaaRSA)[ `ETA_NUM_EPMSI`, `RIP_NUM`, `SEQ_SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS e-PMSI||
`FOR_NUM`| chaîne de caractères |Numéro format||
`ANN`| chaîne de caractères |Année période transmission||
`MOD_VEN`| nombre entier |Modalité de la venue||
`MOI`| date |Mois période transmsission||
`RIP_NUM`| chaîne de caractères |Numéro séquentiel séjour||
`SEQ_SEQ_NUM`| chaîne de caractères |Numéro sequentiel séquence au sein séjour||
`SEQ_VEN_NUM`| chaîne de caractères |Numéro séquentiel venue||
`FOR_ACT`| chaîne de caractères |Forme d'activité||
`ENT_DEL_DAT`| nombre entier |Délai par rapport date d'entrée séjour||
`TYP_VEN`| chaîne de caractères |Type venue||
`ACT_COD`| chaîne de caractères |Prestation||