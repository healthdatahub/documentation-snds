### Schéma


- Titre : FICHCOMP Fich de contrôle-Contention et Isolement
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI`=> table [T_RIPaaE](/tables/T_RIPaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN`| chaîne de caractères |Année période de transmission||
`COD_ERR`| chaîne de caractères |Code erreur||
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS géographique||
`MOI`| date |Mois période de transmission||
`SEQ_ISO_NUM`| chaîne de caractères |Numéro séquentiel de la séquence d’isolement ou de contention||
`SEQ_NUM`| chaîne de caractères |Numéro séquentiel de séjour||
`SEQ_SEQ_NUM`| chaîne de caractères |Numéro séquentiel de séquence au sein du séjour||
`MES_ORD_NUM`| chaîne de caractères |Numéro séquentiel de la séquence d’isolement ou de contention||