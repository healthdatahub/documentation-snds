### Schéma


- Titre : Fich de contrôle - Séjours à Temps Partiel
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RIP_NUM, SEQ_SEQ_NUM`=> table [T_RIPaaRSA](/tables/T_RIPaaRSA)[ `ETA_NUM_EPMSI`, `RIP_NUM`, `SEQ_SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_TWO`| chaîne de caractères |Numéro FINESS géographique||
`MOI`| date |Mois période transmission||
`ANN`| chaîne de caractères |Année période transmission||
`RIP_NUM`| chaîne de caractères |Numéro séquentiel séjour||
`SEQ_SEQ_NUM`| chaîne de caractères |Numéro séquentiel séquence au sein séjour||
`SEQ_VEN_NUM`| chaîne de caractères |Numéro séquentiel venue||
`COD_ERR`| chaîne de caractères |Code erreur||