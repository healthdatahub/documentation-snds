### Schéma


- Titre : medicament
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RIP_NUM`=> table [T_RIPaaFB](/tables/T_RIPaaFB)[ `ETA_NUM_EPMSI`, `RIP_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS de l’entité juridique||
`ETA_NUM_TWO`| chaîne de caractères |Numéro FINESS de l’établissement (site géographique)||
`RIP_NUM`| chaîne de caractères |Numéro séquentiel de séjour (idem RPSA)||
`FAC_NUM`| chaîne de caractères |N° séquentiel de facture||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`EXE_SOI_DTD`| date |Date soins||
`COD_UCD`| chaîne de caractères |Code UCD||
`COE_TAU`| number |Coefficient de fractionnement||
`ACH_PRI`| number |Prix d'achat unitaire TTC||
`MNT_UNI_ECA`| number |Montant unitaire de l'écart indemnisable||
`TOT_MNT_ECA`| number |Montant total de l'écart indemnisable||
`QUA_COD`| nombre entier |Quantité||
`FAC_TOT`| number |Montant total facturé TTC||
`UCD_UCD_COD`| chaîne de caractères |Code UCD (0 à gauche)||
`ENT_MOI`| date |Mois de la date de début de séjour||
`ENT_ANN`| année |Année de la date de début de séjour||