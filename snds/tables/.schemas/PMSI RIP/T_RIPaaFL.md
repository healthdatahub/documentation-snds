### Schéma


- Titre : actes de Biologie
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI, RIP_NUM`=> table [T_RIPaaFB](/tables/T_RIPaaFB)[ `ETA_NUM_EPMSI`, `RIP_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`ETA_NUM_EPMSI`| chaîne de caractères |Numéro FINESS de l’entité juridique||
`ETA_NUM_TWO`| chaîne de caractères |Numéro FINESS de l’établissement (site géographique)||
`RIP_NUM`| chaîne de caractères |Numéro séquentiel de séjour (idem RPSA)||
`EXE_SOI_DTD`| date |Date l'acte 1||
`EXE_SOI_DTD`| date |Date l'acte 2||
`EXE_SOI_DTD`| date |Date l'acte 3||
`EXE_SOI_DTD`| date |Date l'acte 4||
`EXE_SOI_DTD`| date |Date l'acte 5||
`FAC_NUM`| chaîne de caractères |N° séquentiel de facture||
`PSH_MDT`| chaîne de caractères |Mode de traitement||
`PSH_DMT`| chaîne de caractères |Discipline de prestation (ex DMT)||
`DEL_DAT_ENT`| nombre entier |délai par rapport à la date d'entrée||
`ACT_NBR`| nombre entier |Quantité acte||
`NABM_COD`| chaîne de caractères |Code acte||
`ANN_MOI`| date |Mois et année||