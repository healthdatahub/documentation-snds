### Schéma


- Titre : Fich comp Méd. thrombolytiques pour le traitement des AVC ischémiques 
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ADM_ANN`| année |Année de la date d'administration||
`ADM_MOIS`| date |Mois de la date d'administration||
`ADM_NBR`| number |Nombre administré éventuellement fractionnaire||
`ADM_PRI_ADM`| number |Prix d'achat multiplié par le nombre administré||
`ANN`| chaîne de caractères |Année période||
`DAT_DELAI`| nombre entier |Délai entre la date d’entrée du séjour et la date de dispensation||
`ETA_NUM`| chaîne de caractères |N° FINESS e-PMSI||
`ETA_NUM_ENT`| chaîne de caractères |N° FINESS du fichier d'entrée||
`INI_VAL_PRS`| chaîne de caractères |Validation initiale de la prescription par un centre de référence ou de compétence||
`MOI`| date |N° période (mois)||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA||
`SEJ_NBR`| nombre entier |Nombre de séjours impliqués||
`TOP_UCD_AUTO`| chaîne de caractères |Top transcodage UCD13 auto||
`UCD_UCD_COD`| chaîne de caractères |Code UCD||
`UCD_COD`| chaîne de caractères |Code UCD||