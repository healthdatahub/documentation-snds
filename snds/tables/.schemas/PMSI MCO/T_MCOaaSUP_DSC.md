### Schéma


- Titre : Tests diagnostiques Covid
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS ePMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`ANN_MOI`| date |Année et Mois de la période de transmission||
`PMP`| nombre entier |Prix moyen pondéré par l'effectif des autotests du personnel non vacciné du 08/08/2021 au 15/10/2021||
`TYP_TEST`| chaîne de caractères |Type de tests||
`ACT_COD`| chaîne de caractères |Code de l'acte||
`PERIOD_ACT`| chaîne de caractères |Période d'execution ||
`NBR_ACT`| nombre entier |Nombre d'actes réalisés ||
`PMP`| nombre entier |Prix moyen pondéré par l'effectif des autotests du personnel non vacciné du 08/08/2021 au 15/10/2021||