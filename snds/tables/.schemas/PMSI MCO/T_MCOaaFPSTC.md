### Schéma


- Titre : ACE DMI en sus
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, SEQ_NUM`=> table [T_MCOaaFASTC](/tables/T_MCOaaFASTC)[ `ETA_NUM`, `SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS géographique||
`FAC_MNT`| number |Montant total facturé||
`LPP_COD`| chaîne de caractères |Code référence LPP||
`LPP_PRI_UNI`| number |Prix d'achat unitaire||
`LPP_PU_DEV`| number |Tarif référence LPP ou Prix Unitaire sur devis||
`LPP_QUA`| nombre entier |Quantité||
`MNT_UNI_ECA`| number |Montant unitaire de l'écart indemnisable||
`SEQ_NUM`| chaîne de caractères |N° séquentiel||
`TIP_PRS_IDE`| nombre entier |Code référence LPP (format numérique)||
`TOT_MNT_ECA`| number |Montant total de l'écart indemnisable||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`EXE_SOI_DTD`| date |Date des soins||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée au format année + mois||
`ENT_ANN`| année |Année début de séjour||
`ENT_MOI`| date |Mois de début de séjour||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1 : ancien, 2 : nouveau)||