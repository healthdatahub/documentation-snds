### Schéma


- Titre : Fich comp prothèse PIP
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN`| chaîne de caractères |Année période||
`ETA_NUM`| chaîne de caractères |N° FINESS e-PMSI||
`MOI`| date |N° période (mois)||
`PIP_COD`| chaîne de caractères |Code PIP||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA||