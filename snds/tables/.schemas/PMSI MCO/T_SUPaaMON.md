### Schéma


- Titre : Molécules onéreuses (extension de fichier .mon)
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN_MOI`| date |Mois et année||
`DER_ACH_PRI`| chaîne de caractères |Dernier prix d'achat||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`MOY_PRI`| chaîne de caractères |Prix moyen||
`NBR_ETH_UCD`| chaîne de caractères |Nombre d'UCD dispensées au titre des essais thérapeutiques||
`NBR_TOT_UCD`| chaîne de caractères |Nombre d'UCD dispensées totales||
`UCD_COD`| chaîne de caractères |Code UCD||