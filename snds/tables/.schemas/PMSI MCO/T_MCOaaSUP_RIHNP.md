### Schéma


- Titre : Recueil du RIHN/LC : établissement producteur
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS ePMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`ANN_MOI`| date |Année+Mois||
`ID_DEMANDR`| chaîne de caractères |Identification du bénéficiaire||
`ACT_COD`| chaîne de caractères |Code de l'acte||
`NBR_ACT_PROD`| nombre entier |Nombre d’actes réalisés||
`NBR_ACT_FACT`| nombre entier |Nombre d’actes facturés||
`MNT_ACT_FACT`| number |Montant facturé||