### Schéma


- Titre : Vaccination Monkeypox
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS ePMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`ANN`| chaîne de caractères |Année de la période de transmission||
`MOI`| date |Mois de la période de transmission||
`PRS`| chaîne de caractères |Prestation||
`PERIOD`| nombre entier |Période||
`NBR_PRS`| nombre entier |Nombre de prestation||