### Schéma


- Titre : Unité médicale
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACT_RUM_NBR`| nombre entier |Nombre de zones d'actes du RUM (Nb_Act_R_1)||
`AGE_GES`| nombre entier |Age gestationnel du RUM||
`AUT_TYP1_UM`| chaîne de caractères |Premier type d'autorisation d'UM||
`AUT_TYP2_UM`| chaîne de caractères |Deuxième type d'autorisation d'UM||
`DGN_ASS_NBR`| nombre entier |Nombre de diagnostics associés du RUM (Nb_DA_R_1)||
`DGN_PAL`| chaîne de caractères |DP||
`DGN_REL`| chaîne de caractères |DR||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Premier numéro FINESS géographique||
`IGS2_COD`| chaîne de caractères |IGS2||
`PAR_DUR_SEJ`| nombre entier |Durée séjour partielle||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA ||
`RSS_NUM`| chaîne de caractères |Numéro de version du format du RSA||
`SUP_TYP1_NAT`| chaîne de caractères |Nature du supplément pour le premier type||
`SUP_TYP1_NBR`| nombre entier |Nombre de supplément pour le premier type||
`SUP_TYP2_NAT`| chaîne de caractères |Nature du supplément pour le deuxième type||
`SUP_TYP2_NBR`| nombre entier |Nombre de supplément pour le deuxième type||
`UM_ORD_NUM`| nombre entier |N° séquentiel de RUM||
`NUM_ANO_UM`| nombre entier |Numéro séquentiel de l'UM||
`RUM_ORD_NUM`| nombre entier |Numero sequentiel du RUM||
`DP_POS`| chaîne de caractères |Position du DP||
`ETA_NUM_GEO1`| chaîne de caractères |Premier numéro FINESS géographique||
`ETA_NUM_GEO2`| chaîne de caractères |Deuxième numéro FINESS géographique||
`IND_SRC`| chaîne de caractères |Indicateur de SRC||
`PAR_NBR_SEJ`| chaîne de caractères |Durée séjour partielle||
`UM_TYP`| chaîne de caractères |Type d'UM||
`VAL_PAR`| chaîne de caractères |Valorisation partielle||
`VAL_REA`| chaîne de caractères |Valorisation réa||