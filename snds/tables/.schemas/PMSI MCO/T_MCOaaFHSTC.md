### Schéma


- Titre : ACE medicament en sus
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, SEQ_NUM`=> table [T_MCOaaFASTC](/tables/T_MCOaaFASTC)[ `ETA_NUM`, `SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACH_PRU_TTC`| number |Prix d'achat unitaire TTC||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`ECRT_IND_MNT`| number |Montant total de l'écart indemnisable||
`ECRT_IND_MTU`| number |Montant unitaire de l'écart indemnisable||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS géographique||
`FAC_MNT`| number |Montant total facturé TTC||
`FRACT_COE`| number |Coefficient de fractionnement||
`QUA`| nombre entier |Quantité||
`SEQ_NUM`| chaîne de caractères |N° séquentiel||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`UCD_COD`| chaîne de caractères |Code UCD||
`UCD_UCD_COD`| chaîne de caractères |Code UCD (0 à gauche)||
`COD_LES`| chaîne de caractères |Code indication des spécialités pharmaceutiques inscrites sur la liste en sus||
`EXE_SOI_DTD`| date |Date des soins||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée du séjour||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1 : ancien, 2 : nouveau)||
`ENT_ANN`| année |Année début de séjour||
`ENT_MOI`| date |Mois de début de séjour||