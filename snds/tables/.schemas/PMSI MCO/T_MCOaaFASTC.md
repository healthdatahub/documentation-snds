### Schéma


- Titre : ACE Entete facture
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`AGE_ANN`| année |Age (en années)||
`BDI_COD`| chaîne de caractères |Code géographique de résidence||
`BDI_DEP`| chaîne de caractères |Code département de résidence||
`COD_CIV`| chaîne de caractères |Code civilité ||
`COD_SEX`| chaîne de caractères |Sexe ||
`CTR_TYP`| chaîne de caractères |Type de contrat souscrit auprès d'un organisme complémentaire||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS géographique||
`EXO_TM`| chaîne de caractères |Justification d'exonération du TM ||
`FIDES_TOP`| chaîne de caractères |Valorisé par FIDES||
`GES_COD`| chaîne de caractères |Code gestion||
`HON_AM_MNR`| number |Total honoraire remboursable AM ||
`HON_MNT`| number |Total honoraire Facturé ||
`HON_OC_MNR`| number |Total remboursable OC pour les honoraires ||
`NAT_ASS`| chaîne de caractères |Nature assurance ||
`NOE_RGM`| chaîne de caractères |Code Gd régime ||
`NON_SEJ_FAC_AM`| chaîne de caractères |Motif de la non facturation à l'assurance maladie||
`NUM_DAT_AT`| chaîne de caractères |Numéro accident du travail ou date d’accident de droit commun||
`OPE_NAT`| chaîne de caractères |Nature opération ||
`ORG_CPL_NUM`| chaîne de caractères |N° d’organisme complémentaire||
`PAS_OC_MNT`| number |Total participation assuré avant OC ||
`PAT_CMU`| chaîne de caractères |Patient bénéficiaire de la CMU||
`PH_AMO_MNR`| number |Total remboursable AMO Prestation hospitalières ||
`PH_BRM`| number |Total Base Remboursement Prestation hospitalière ||
`PH_MNT`| number |Montant total facturé pour PH ||
`PH_OC_MNR`| number |Total remboursable OC pour les PH ||
`PS_IND`| chaîne de caractères |Indicateur du parcours de soins||
`RNG_BEN`| chaîne de caractères |Rang de bénéficiaire||
`RNG_NAI`| chaîne de caractères |Rang de naissance||
`SEJ_FAC_AM`| chaîne de caractères |Séjour facturable à l’assurance maladie||
`SEQ_NUM`| chaîne de caractères |N° séquentiel||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`EXE_SOI_DTD`| date |Date de début de la période de facturation||
`EXE_SOI_DTF`| date |Date de fin de la période de facturation||
`AGE_JOU`| nombre entier |Age en jours||
`COD_POST`| chaîne de caractères |Code postal||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée du séjour||
`EXE_SOI_AMF`| chaîne de caractères |Date de sortie du séjour||
`POST_DEP`| chaîne de caractères |département postal du bénéficiaire ||
`COD_PEC`| chaîne de caractères |Code de prise en charge||
`DAT_RET`| chaîne de caractères |Code retour contrôle « date de référence» (date d'entrée)||
`NIAS_RET`| chaîne de caractères |Code retour contrôle « n° d’identification administratif de séjour »||
`NIR_ANO_17`| chaîne de caractères |N° anonyme||
`NIR_RET`| chaîne de caractères |Code retour contrôle « n° sécurité sociale »||
`SEX_RET`| chaîne de caractères |Code retour contrôle « sexe »||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1 : ancien, 2 : nouveau)||
`SOR_ANN`| année |Année de sortie||
`SOR_MOI`| date |Mois de sortie||