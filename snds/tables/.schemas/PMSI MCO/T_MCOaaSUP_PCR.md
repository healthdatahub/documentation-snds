### Schéma


- Titre : Tests PCR 
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACT_COD`| chaîne de caractères |Code acte||
`ANN_MOI`| date |Année Mois de la période de transmission||
`ETA_NUM`| chaîne de caractères |Numéro FINESS ePMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`NBR_ACT`| nombre entier |Nombre d’actes réalisés||
`PERIOD_ACT`| chaîne de caractères |Periode execution||
`TYP_DEP`| chaîne de caractères |Type de dépistage||
`ACT_COD`| chaîne de caractères |Code acte||
`ANN_MOI`| date |Année Mois de la période de transmission||
`ETA_NUM`| chaîne de caractères |Numéro FINESS ePMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`NBR_ACT`| nombre entier |Nombre d’actes réalisés||
`PERIOD_ACT`| chaîne de caractères |Periode execution||
`TYP_DEP`| chaîne de caractères |Type de dépistage||