### Schéma


- Titre : Diagnostic associé
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ASS_DGN`| chaîne de caractères |Diagnostic associé||
`DGN_ASS_NUM`| nombre entier |Numero de diag par couple eta_num rsa_num||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA ||
`RSS_NUM`| chaîne de caractères |Numéro de version du format du RSA||