### Schéma


- Titre : Fichier d’information des UM
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |N° FINESS e-PMSI||
`FIC_TYP`| chaîne de caractères |N° format||
`ANN_MOI`| date |Année+Mois||
`ETA_NUM_GEO`| chaîne de caractères |N° FINESS géographque||
`AUT_TYP_UM`| chaîne de caractères |Type d'autorisation||
`HOS_TYP`| chaîne de caractères |Mode d'hospitalisation||
`DAT_DEB_AUT`| chaîne de caractères |Date de début d’effet||
`NBR_LIT_UM`| nombre entier |Nombre de lits||
`NUM_ANO_UM`| nombre entier |Numéro séquentiel de l'UM||
`UM_NUM_ETB`| chaîne de caractères |N° UM||