### Schéma


- Titre : Fich comp IVG
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN`| chaîne de caractères |Année période||
`ANN_IVG_PREC`| chaîne de caractères |Année de la dernière IVG||
`ETA_NUM`| chaîne de caractères |N° FINESS e-PMSI||
`ETA_NUM_ENT`| chaîne de caractères |N° FINESS du fichier d'entrée||
`MOI`| date |N° période (mois)||
`NBR_IVG_ANT`| nombre entier |Nombre d'IVG antérieures||
`NBR_NAIS_ANT`| nombre entier |Nombre de naissances vivantes antérieures||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA||
`SOR_ANN`| année |Année du séjour||
`SOR_MOI`| date |Mois du séjour||