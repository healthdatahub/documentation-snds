### Schéma


- Titre : Etablissement
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN_TRT`| chaîne de caractères |N° du trimestre PMSI transmis||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETB_EXE_FIN`| chaîne de caractères |N°FINESS sans clé||
`REG_ETA`| chaîne de caractères |Région||
`SOC_RAI`| chaîne de caractères |Raison sociale||
`STA_ETA`| chaîne de caractères |Statut de l'établissement||
`VAL_ETA`| chaîne de caractères |Validation des données||
`COM_ETA`| chaîne de caractères |Commentaires||