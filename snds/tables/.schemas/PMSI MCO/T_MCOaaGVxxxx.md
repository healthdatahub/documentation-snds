### Schéma


- Titre : Table de passage en version de GHM à partir de mars de l'année suivante
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`FORMAT`| chaîne de caractères |Format||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETE_GHS_NUM`| nombre entier |Regroupage en GHS (format numérique de GHS_NUM)||
`GHM_NUM`| chaîne de caractères |Regroupage en GHM||
`GHS_NUM`| chaîne de caractères |Regroupage en GHS (utilisé pour la valorisation)||
`GHS_THEO`| chaîne de caractères |Regroupage en GHS (GHS correspond au GHM)||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA ||