### Schéma


- Titre : Fich sup SMUR
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN_MOI`| date |Année+Mois||
`ETA_NUM`| chaîne de caractères |Numéro FINESS ePMSI||
`ETA_NUM_SMUR`| chaîne de caractères |N° FINESS d’implantation SMUR||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`NBR_SOR_PRIM`| nombre entier |Nombre de sorties primaires||
`NBR_SOR_SND`| nombre entier |Nombre de sorties secondaires||
`NBR_TIIH`| nombre entier |Nombre TIIH (transfert infirmier inter-hospitalier)||