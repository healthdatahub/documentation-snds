### Schéma


- Titre : Fich sup Médicaments dispensés en USMP (Unité Sanitaire en Milieu Pénitentiaire)
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`UCD_COD_7_13`| chaîne de caractères |Code UCD (7 ou 13)||
`ANN_MOI`| date |Année+Mois||
`ETA_NUM`| chaîne de caractères |N° FINESS d’inscription e-PMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`MNT_TOT_TTC`| number |Montant total TTC||