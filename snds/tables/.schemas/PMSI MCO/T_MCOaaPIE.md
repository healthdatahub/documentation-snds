### Schéma


- Titre : Fich comp prestation inter établissement
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN`| chaîne de caractères |Année période||
`DELAI`| nombre entier |Délai entre la date d’entrée du séjour et la date de début de la prestation||
`ETA_NUM`| chaîne de caractères |N° FINESS e-PMSI||
`ETA_NUM_ENT`| chaîne de caractères |N° FINESS du fichier d'entrée||
`MOI`| date |N° période (mois)||
`POS_ANN`| année |Année de la date de début de la prestation||
`POS_MOI`| date |Mois de la date de début de la prestation||
`PRS_COD`| chaîne de caractères |Code Prestation||
`PRS_JOU_NBR`| nombre entier |Durée de la prestation||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA||
`SUP_PAY_NBR`| nombre entier |Nombre de suppléments payés||