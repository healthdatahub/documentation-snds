### Schéma


- Titre : OQN Honoraire
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACT_COD`| chaîne de caractères |Code acte||
`ACT_COE`| number |Coefficient||
`ACT_DNB`| nombre entier |Dénombrement||
`ACT_NBR`| nombre entier |Quantité||
`AMC_MNR`| number |Montant remboursable par AMC||
`AMO_MNR`| number |Montant Remboursable par AMO||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`EXE_SPE`| chaîne de caractères |Spécialité exécutant||
`EXO_TM`| chaîne de caractères |Justification exo TM||
`HON_MNT`| number |Montant des honoraire (dépassement compris)||
`NOE_MNR`| number |Montant remboursé NOEMIE Retour||
`NOE_OPE`| chaîne de caractères |Nature opération récupération NOEMIE Retour||
`NUM_FAC`| chaîne de caractères |N° séquentiel de facture||
`PRI_UNI`| number |Prix Unitaire||
`PSH_DMT`| chaîne de caractères |Discipline de prestation (ex DMT)||
`PSH_MDT`| chaîne de caractères |Mode de traitement||
`REM_BAS`| number |Montant Base remboursement||
`REM_TAU`| nombre entier |Taux Remboursement||
`RSA_NUM`| chaîne de caractères | N° séquentiel (le même que pour les RSA)||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`EXE_SOI_DTD`| date |Date de l'acte||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée du séjour||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1 : ancien, 2 : nouveau)||
`SOR_ANN`| année |Année de l'acte||
`SOR_MOI`| date |Mois de l'acte||