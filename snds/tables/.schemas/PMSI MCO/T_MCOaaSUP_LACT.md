### Schéma


- Titre : Fich sup lactarium
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN_MOI`| date |Année+Mois||
`ETA_NUM`| chaîne de caractères |Numéro FINESS d’inscription ePMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS géographique||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`LAI_COL_ANO`| number |Quantité de lait collecté dans le cadre d'un don anonyme (en litres)||
`LAI_COL_PERS`| number |Quantité de lait collecté pour don personnalisé (en litres)||
`LAI_CONG_CONS`| number |Quantité de lait congelé consommé (en litres)||
`LAI_LYOP_CONS`| number |Quantité de lait lyophilisé consommé (en litres)||
`LAI_PAST_DIS_ANO`| number |Quantité de lait pasteurisé congelé distribué au total dans le cadre du don anonyme (en litres)||
`LAI_PAST_DIS_PERS`| number |Quantité de lait pasteurisé congelé distribué pour don personnalisé (en litres)||
`LAI_PAST_REQ_ANO`| number |Quantité de lait pasteurisé congelé requalifié en don anonyme (en litres)||
`LAI_PAST_VEND`| number |Quantité de lait pasteurisé congelé vendu (en litres)||