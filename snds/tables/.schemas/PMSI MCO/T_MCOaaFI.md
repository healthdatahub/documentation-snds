### Schéma


- Titre : OQN Interruption séjour
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETB_NUM`| chaîne de caractères |Etablissement de transfert ou de retour ou lieu d'exécution de l'acte||
`NUM_FAC`| chaîne de caractères |N° séquentiel de facture||
`RSA_NUM`| chaîne de caractères | N° séquentiel (le même que pour les RSA)||
`SEJ_FIN`| chaîne de caractères |Nature d'interruption ou de fin de séjour||
`SEJ_FIN`| chaîne de caractères |Nature d'interruption ou de fin de séjour||
`ETB_NUM`| chaîne de caractères |Etablissement de transfert ou de retour ou lieu d'exécution de l'acte||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`EXE_SOI_DTD`| date |Date de début||
`EXE_SOI_DTF`| date |Date de fin||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée du séjour||
`EXE_SOI_AMF`| chaîne de caractères |Date de sortie du séjour||
`SOR_ANN`| année |Année de fin de séjour||
`SOR_MOI`| date |Mois de fin de séjour||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1 : ancien, 2 : nouveau)||