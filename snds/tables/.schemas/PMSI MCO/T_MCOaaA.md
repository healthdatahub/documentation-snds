### Schéma


- Titre : Acte CCAM
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACV_ACT`| chaîne de caractères |Activité||
`ANP_ACT`| chaîne de caractères |Association non prévue||
`CDC_ACT`| chaîne de caractères |Code CCAM (hors extension PMSI)||
`CMP_DAT`| chaîne de caractères |Dates de validité de l'acte compatibles avec les dates du RUM||
`DOC_ACT`| chaîne de caractères |Extension documentaire||
`ENT_DAT_DEL`| nombre entier |Délai depuis la date d'entrée||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`EXT_PMSI`| chaîne de caractères |Extension PMSI||
`MOD_ACT`| chaîne de caractères |Modificateurs||
`NBR_EXE_ACT`| nombre entier |Nombre de réalisations de l'acte n° 1 pendant le séjour||
`PHA_ACT`| chaîne de caractères |Phase||
`REM_EXP_ACT`| chaîne de caractères |Remboursement exceptionnel||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA ||
`RSS_NUM`| chaîne de caractères |Numéro de version du format du RSA||
`ACT_ORD_NUM`| nombre entier |N° sequentiel ACTE||
`RUM_SEQ`| chaîne de caractères |N° séquentiel du RUM ayant fourni le DP||