### Schéma


- Titre : ACE Prestation
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, SEQ_NUM`=> table [T_MCOaaFASTC](/tables/T_MCOaaFASTC)[ `ETA_NUM`, `SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACT_COD`| chaîne de caractères |Code acte||
`ACT_COE`| number |Coefficient||
`ACT_DNB`| nombre entier |Dénombrement||
`ACT_NBR`| nombre entier |Quantité||
`AMC_MNR`| number |Montant remboursable par l'organisme complémentaire (AMC)||
`AMO_MNR`| number |Montant Remboursable par la caisse (AMO)||
`COEF_MCO`| number |Coefficient MCO ||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS géographique||
`EXE_SPE`| chaîne de caractères |Spécialité exécutant||
`EXO_TM`| chaîne de caractères |Justification d'exonération du TM||
`HON_MNT`| number |Montant des honoraires (dépassement compris) ou Montant total de la dépense pour PH||
`PRI_UNI`| number |Prix Unitaire||
`PSH_DMT`| chaîne de caractères |Discipline de prestation (ex DMT)||
`PSH_MDT`| chaîne de caractères |Mode de traitement||
`REM_BAS`| number |Montant Base remboursement||
`REM_TAU`| nombre entier |Taux applicable à la prestation||
`SEQ_NUM`| chaîne de caractères |N° séquentiel||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`TYP_FPI`| chaîne de caractères |Type de prestation intermédiaire||
`EXE_SOI_DTD`| date |Date des soins||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée du séjour||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1 : ancien, 2 : nouveau)||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||
`SOR_ANN`| année |Année des soins||
`SOR_MOI`| date |Mois des soins||