### Schéma


- Titre : Fich comp Transports intra et inter établissements 
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_ENT`| chaîne de caractères |Numéro FINESS du fichier d'entrée||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`ANN`| chaîne de caractères |Année période de transmission||
`MOI`| date |Mois période de transmission||
`RSA_NUM`| chaîne de caractères |Numéro séquentiel de séjour||
`EXE_SOI_DTD`| date |Date d'aller||
`ACT_COD`| chaîne de caractères |Code||
`CL_DISTANCE`| nombre entier |Classe de distance||
`EXE_SOI_RET`| chaîne de caractères |CR Date d'aller||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée du séjour||