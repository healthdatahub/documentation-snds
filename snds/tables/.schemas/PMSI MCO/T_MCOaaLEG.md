### Schéma


- Titre : Fichier donnant toutes les erreurs détectées par la fonction groupage
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN`| chaîne de caractères |Année période||
`COD_ERR`| chaîne de caractères |Code retour||
`ETA_NUM`| chaîne de caractères |N° FINESS e-PMSI||
`MOI`| date |Mois période||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA||