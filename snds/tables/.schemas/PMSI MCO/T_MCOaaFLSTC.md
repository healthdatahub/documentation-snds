### Schéma


- Titre : ACE biologie NABM
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, SEQ_NUM`=> table [T_MCOaaFASTC](/tables/T_MCOaaFASTC)[ `ETA_NUM`, `SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACT_NBR`| nombre entier |Quantité acte||
`DEL_DAT_ENT`| nombre entier |délai par rapport à la date d'entrée||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Numéro FINESS géographique||
`NABM_COD`| chaîne de caractères |Code acte||
`PSH_DMT`| chaîne de caractères |Discipline de prestation (ex DMT)||
`PSH_MDT`| chaîne de caractères |Mode de traitement||
`SEQ_NUM`| chaîne de caractères |N° séquentiel||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`EXE_SOI_DTD`| date |Date de l'acte 1 à 5||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée du séjour||
`ANN_MOI`| date |Mois et année||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1 : ancien, 2 : nouveau)||
`NUM_FAC`| chaîne de caractères |N° Facture séquentiel||