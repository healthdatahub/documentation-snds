### Schéma


- Titre : Recueil du RIHN/LC : établissement demandeur (payeur)
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS ePMSI de l'établissement payeur||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`ANN_MOI`| date |Année+Mois||
`ID_PRODUCT`| chaîne de caractères |Finess établissement producteur||
`ACT_COD`| chaîne de caractères |Code Acte||
`NBR_ACT`| nombre entier |Nombre||
`MNT_ACT`| number |Montant||