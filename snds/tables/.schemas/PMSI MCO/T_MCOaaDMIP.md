### Schéma


- Titre : Fich comp dmi en sus
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN`| chaîne de caractères |Année période||
`DAT_POS_ANN`| année |Année de la date de pose||
`DELAI`| nombre entier |Délai entre la date d’entrée du séjour et la date de pose||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_ENT`| chaîne de caractères |N° FINESS du fichier d'entrée||
`LPP_COD`| chaîne de caractères |Code LPP (attention blanc devant)||
`MOI`| date |N° période (mois)||
`MOI_POS`| date |Mois de la date de pose (si renseginée)||
`NBR_POS`| nombre entier |Nombre posé||
`NBR_POS_PRI`| number |Prix d'achat multiplié par le nombre posé||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA||
`TIP_PRS_IDE`| nombre entier |Code LPP (format numérique)||