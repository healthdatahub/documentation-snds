### Schéma


- Titre : Types d'autorisations d'unités médicales à portée globale valides
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`AUT_TYP_PGV`| chaîne de caractères |Type d'autorisation à portée globale valide ||
`ETA_NUM`| chaîne de caractères |Numéro FINESS||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA ||
`RSS_NUM`| chaîne de caractères |Numéro de version du format du RSA||