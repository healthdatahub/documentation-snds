### Schéma


- Titre : Dispositifs médicaux implantables (extension de fichier .dmi)
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN_MOI`| date |Mois et année||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`LPP_COD`| chaîne de caractères |Code LPP (attention blanc devant)||
`LPP_NBR`| chaîne de caractères |Nombre de LPP posés||
`MOY_PRI`| chaîne de caractères |Prix moyen||