### Schéma


- Titre : OQN medicament en sus
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACH_PRI`| number |Prix d'achat unitaire TTC||
`COD_UCD`| chaîne de caractères |Code UCD||
`COE_TAU`| number |Coefficient de fractionnement||
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`FAC_TOT`| number |Montant total facturé TTC||
`MNT_UNI_ECA`| number |Montant unitaire de l'écart indemnisable||
`NUM_FAC`| chaîne de caractères |N° séquentiel de facture||
`QUA_COD`| nombre entier |Quantité||
`RSA_NUM`| chaîne de caractères | N° séquentiel (le même que pour les RSA)||
`TOT_MNT_ECA`| number |Montant total de l'écart indemnisable||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`UCD_UCD_COD`| chaîne de caractères |Code UCD (0 à gauche)||
`COD_LES`| chaîne de caractères |Code indication des spécialités pharmaceutiques inscrites sur la liste en sus||
`EXE_SOI_DTD`| date |Date de soins||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée du séjour||
`COD_TAU`| chaîne de caractères |Code taux||
`COD_UCP_CIP`| chaîne de caractères |Code UCD ou CIP||
`MNT_IND`| chaîne de caractères |Montant écart indemnisable||
`PRI_UNI_FAC`| chaîne de caractères |Prix unitaire facturé||
`ENT_ANN`| année |Année début de séjour||
`ENT_MOI`| date |Mois de début de séjour||