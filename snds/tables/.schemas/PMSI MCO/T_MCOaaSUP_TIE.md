### Schéma


- Titre : Fichier d’information Transports inter-établissements
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |N° FINESS d’inscription e-PMSI||
`MOIS_TRSP`| date |Mois du transport||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`ANN_MOI`| date |Année||
`ACT_COD`| chaîne de caractères |Code||
`CL_DISTANCE`| nombre entier |Classe de distance||
`NBR_SUP_TRSP`| nombre entier |Nombre de suppléments||
`NBR_SEJ`| nombre entier |Nombre de séjours||