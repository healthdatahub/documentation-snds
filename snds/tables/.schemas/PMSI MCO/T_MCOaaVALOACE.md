### Schéma


- Titre : Valorisation des actes et consultations externes
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, SEQ_NUM`=> table [T_MCOaaFASTC](/tables/T_MCOaaFASTC)[ `ETA_NUM`, `SEQ_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |n°finess de l'établissement||
`fides`| chaîne de caractères |Variable "valorisé par fides" du RAFAEL-A||
`fides_ovalide`| chaîne de caractères |Indicateur (calculé par OVALIDE) servant à la valorisation FIDES||
`mnt_br`| number |Montant Base de remboursement total ||
`mnt_br_atu`| number |Montant Base de remboursement ATU||
`mnt_br_ccam`| number |Montant Base de remboursement CCAM||
`mnt_br_dia`| number |Montant Base de remboursement Alternative à la dialyse en centre||
`mnt_br_dm_ext`| number |Montant Base de remboursement DM externe||
`mnt_br_ffm`| number |Montant Base de remboursement FFM||
`mnt_br_ftn`| number |Montant Base de remboursement FTN||
`MNT_BR_MED_EXT`| number |Montant Base de remboursement Médicaments externe||
`mnt_br_ngap`| number |Montant Base de remboursement NGAP||
`mnt_br_se`| number |Montant Base de remboursement SE||
`MNT_RAC`| number |Montant remboursable par l'organisme complémentaire (AMC)||
`mnt_remb`| number |Montant Remboursé AM  total, y compris factures FIDES||
`mnt_remb_atu`| number |Montant Remboursé AM ATU, y compris factures FIDES||
`mnt_remb_ccam`| number |Montant Remboursé AM CCAM, y compris factures FIDES||
`mnt_br_fu`| number |Montant Base de remboursement Forfait Urgence et suppléments||
`mnt_remb_fu`| number |Montant Remboursé AM Forfait Urgence et suppléments||
`mnt_val_fu`| number |Montant AM effectivement valorisé par OVALIDE Forfait Urgence et suppléments||
`mnt_remb_dia`| number |Montant Remboursé AM Alternative à la dialyse en centre, y compris factures FIDES||
`mnt_remb_dm_ext`| number |Montant Remboursé AM DM externe, y compris factures FIDES||
`mnt_remb_ffm`| number |Montant Remboursé AM FFM, y compris factures FIDES||
`mnt_remb_ftn`| number |Montant Remboursé AM FTN, y compris factures FIDES||
`MNT_REMB_MED_EXT`| number |Montant Remboursé AM  Médicaments externe||
`mnt_remb_ngap`| number |Montant Remboursé AM NGAP, y compris factures FIDES||
`mnt_remb_se`| number |Montant Remboursé AM SE, y compris factures FIDES||
`mnt_val`| number |Montant AM effectivement valorisé par OVALIDE||
`mnt_val_atu`| number |Montant AM effectivement valorisé par OVALIDE ATU||
`mnt_val_ccam`| number |Montant AM effectivement valorisé par OVALIDE CCAM||
`mnt_val_dia`| number |Montant AM effectivement valorisé par OVALIDE Alternative à la dialyse en centre||
`mnt_val_dm_ext`| number |Montant AM effectivement valorisé par OVALIDE DM externe||
`mnt_val_ffm`| number |Montant AM effectivement valorisé par OVALIDE FFM||
`mnt_val_ftn`| number |Montant AM effectivement valorisé par OVALIDE FTN||
`MNT_VAL_MED_EXT`| number |Montant AM effectivement valorisé par OVALIDE Médicaments externe||
`mnt_val_ngap`| number |Montant AM effectivement valorisé par OVALIDE NGAP||
`MNT_VAL_PI`| number |Montant AM effectivement valorisé par OVALIDE PI||
`mnt_val_se`| number |Montant AM effectivement valorisé par OVALIDE SE||
`nbligne`| nombre entier |Nb lignes de la facture||
`NON_SEJ_FAC_AM`| chaîne de caractères |Variable "Motif de non facturation à l'assurance maladie" du RAFAEL-A||
`SEJ_FAC_AM`| chaîne de caractères |Variable "Séjour facturable à l'assurance maladie" du RAFAEL-A||
`SEQ_NUM`| chaîne de caractères |n°  de facture séquentiel||
`valo`| chaîne de caractères |Type de valorisation du séjour||
`TOP_TMF`| chaîne de caractères |tmf||
`pf18`| chaîne de caractères |Participation forfaitaire 18 euros déduite sur la facture||
`MNT_BR_PI`| number |Montant Base de remboursement Prestations Intermédiares||
`MNT_REMB_PI`| number |Montant Remboursé AM Prestations Intermédiares||
`MNT_BR_I04`| number |Montant Base de remboursement Forfait Innovation I04||
`MNT_REMB_I04`| number |Montant Remboursé AM Forfait Innovation I04||
`MNT_VAL_I04`| number |Montant AM effectivement valorisé par OVALIDE Forfait Innovation I04||
`MNT_BR_MOP`| nombre entier | Base de remboursement Majoration Personnes âgées (MOP)||
`MNT_REMB_MOP`| nombre entier |Montant Remboursé AM Majoration Personnes âgées (MOP)||
`MNT_VAL_MOP`| nombre entier |Montant AM effectivement valorisé Majoration Personnes âgées||