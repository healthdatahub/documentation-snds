### Schéma


- Titre : Passage aux urgences (extension de fichier .atu) 
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN_MOI`| date |Mois et année||
`AUT_TYP`| chaîne de caractères |Type d'autorisation||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETA_NUM_GEO`| chaîne de caractères |Premier numéro FINESS géographique||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`NBR_HOS`| chaîne de caractères |Nombres d'hospitalisations||
`NBR_PAS`| chaîne de caractères |Nombre de passages total||