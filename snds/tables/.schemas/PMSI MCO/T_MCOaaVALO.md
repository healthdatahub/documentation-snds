### Schéma


- Titre : Valorisation des séjours
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`COEFGEO`| number |Coefficient géographique||
`COEFPRUD`| number |coefficient prudentiel ||
`coefseg`| number |Coefficient SEGUR||
`ETA_NUM`| chaîne de caractères |Numéro FINESS de l’établissement||
`ETE_GHS_NUM`| nombre entier |GHS au format DCIR||
`FLAG_FIDES`| chaîne de caractères |Indicateur FIDES GENRSA ; vaut 1 si le séjour est pris en charge dans le cadre de FIDES||
`GHS_NUM`| chaîne de caractères |Numéro du GHS||
`GHS50`| nombre entier |Nb séjours avec réhospitalisation dans le même GHM||
`MT_9610_AM`| number |Valorisation AM actes menant dans le GHS 9610||
`MT_9615_AM`| number |Valorisation AM actes menant dans le GHS 9615||
`nb_9639`| number |Nombre d’actes menant dans le GHS 9639||
`mt_9639`| number |Valorisation BR actes menant dans le GHS 9639||
`mt_9639_am`| number |Valorisation AM actes menant dans le GHS 9639||
`mt_trans`| number |Valorisation BR Transport||
`mt_trans_am`| number |Valorisation AM Transport||
`MT_9619_AM`| number |Valorisation AM actes menant dans le GHS 9619||
`MT_9620_AM`| number |Valorisation AM actes menant dans le GHS 9620||
`MT_9621_AM`| number |Valorisation AM actes menant dans le GHS 9621||
`MT_9622_AM`| number |Valorisation AM actes menant dans le GHS 9622||
`MT_9623_AM`| number |Valorisation AM actes menant dans le GHS 9623||
`MT_9625_AM`| number |Valorisation AM actes menant dans le GHS 9625||
`MT_9631_AM`| number |Valorisation AM actes menant dans le GHS 9631||
`MT_9632_AM`| number |Valorisation AM actes menant dans le GHS 9632||
`MT_9633_AM`| number |Valorisation AM actes menant dans le GHS 9633||
`MT_ANT_AM`| number |Valorisation AM suppléments antepartum||
`MT_CAISSON_AM`| number |Valorisation AM suppléments caisson hyperbare||
`MT_DIP_AM`| number |Valorisation AM suppléments de dialyse DIP||
`MT_DMI`| number |Valorisation accordée DMI||
`MT_EXH_AM`| number |Valorisation AM EXH||
`MT_EXINF_AM`| number |Valorisation AM EXB||
`MT_FJ2`| number |Montant du forfait journalier||
`MT_GHS_AM`| number |Valorisation AM GHS||
`MT_GHS50_AM`| number |Valorisation AM des séjours avec réhospitalisation dans le même GHM||
`MT_IVG_AM`| number |Valorisation AM IVG (hors FJS)||
`MT_MAJO`| number |Montant majoration du parcours de soin ||
`MT_MED_ATU`| number |Valorisation accordée médicaments ATU/POST ATU||
`MT_MON`| number |Valorisation accordée molécules onéreuses||
`MT_NN1_AM`| number |Valorisation AM suppléments de néonat sans SI||
`MT_NN2_AM`| number |Valorisation AM suppléments de néonat avec SI||
`MT_NN3_AM`| number |Valorisation AM suppléments de réanimation néonat||
`MT_PO1_AM`| number |Valorisation AM PO1||
`MT_PO2_AM`| number |Valorisation AM PO2||
`MT_PO3_AM`| number |Valorisation AM PO3||
`MT_PO4_AM`| number |Valorisation AM PO4||
`MT_PO5_AM`| number |Valorisation AM PO5||
`MT_PO6_AM`| number |Valorisation AM PO6||
`MT_PO7_AM`| number |Valorisation AM PO7||
`MT_PO8_AM`| number |Valorisation AM PO8||
`MT_PO9_AM`| number |Valorisation AM PO9||
`MT_POA_AM`| number |Valorisation AM POA||
`MT_RAC`| number |Montant remboursable par l'organisme complémentaire (AMC)||
`MT_RAP_AM`| number |Valorisation AM suppléments de radiothérapie pédiatrique||
`MT_REA_AM`| number |Valorisation AM suppléments de réanimation||
`MT_REP_AM`| number |Valorisation AM suppléments de réanimation pédiatrique||
`MT_SC_AM`| number |Valorisation AM suppléments de surveillance continue||
`MT_SI_AM`| number |Valorisation AM suppléments soins intensifs||
`MT_SUPENT1_AM`| number |Valorisation AM suppléments pour entraînements à la dialyse péritonéale automatisée hors séance||
`MT_SUPENT2_AM`| number |Valorisation AM suppléments pour entraînements à la dialyse péritonéale continue ambulatoire hors séance||
`MT_SUPENT3_AM`| number |Valorisation AM suppléments pour entraînements à l’hémodialyse hors séance||
`MT_SUPHD_AM`| number |Valorisation AM suppléments pour hémodialyse hors séance||
`MT_TOT_AM`| number |Valorisation totale AM du séjour.||
`NB_9610`| nombre entier |Nombre d’actes menant dans le GHS 9610||
`NB_9615`| nombre entier |Nombre d’actes menant dans le GHS 9615||
`NB_9619`| nombre entier |Nombre d’actes menant dans le GHS 9619||
`NB_9620`| nombre entier |Nombre d’actes menant dans le GHS 9620||
`NB_9621`| nombre entier |Nombre d’actes menant dans le GHS 9621||
`NB_9622`| nombre entier |Nombre d’actes menant dans le GHS 9622||
`NB_9623`| nombre entier |Nombre d’actes menant dans le GHS 9623||
`NB_9625`| nombre entier |Nombre d’actes menant dans le GHS 9625||
`NB_9631`| nombre entier |Nombre d’actes menant dans le GHS 9631||
`NB_9632`| nombre entier |Nombre d’actes menant dans le GHS 9632||
`NB_9633`| nombre entier |Nombre d’actes menant dans le GHS 9633||
`NB_ANT`| nombre entier |Nb suppléments antepartum||
`NB_DIP`| nombre entier |Nb suppléments dialyse DIP||
`NB_NN1`| nombre entier |Nb suppléments de néonat sans SI||
`NB_NN2`| nombre entier |Nb suppléments de néonat avec SI||
`NB_NN3`| nombre entier |Nb suppléments de réanimation néonat||
`NB_RAP`| nombre entier |Nb suppléments de radiothérapie pédiatrique||
`NB_SRC2`| nombre entier |Nb suppléments de surveillance continue||
`NB_STF2`| nombre entier |Nb suppléments de soins intensifs||
`NBCAISSON`| nombre entier |Nb suppléments caisson hyperbare||
`NBEXB`| nombre entier |Nb journées EXB ||
`NBEXH`| nombre entier |Nombre de journées EXH ||
`NBJREA`| nombre entier |Nb suppléments de réanimation||
`NBREP`| nombre entier |Nb suppléments de réanimation pédiatrique||
`PONDER`| nombre entier |Nombre de séjours ou nombre de séances||
`RSA_NUM`| chaîne de caractères |Numéro de séjour RSS||
`SUPENT1`| nombre entier |Nb suppléments pour entraînements à la dialyse péritonéale automatisée hors séance||
`SUPPNT2`| nombre entier |Nb suppléments pour entraînements à la dialyse péritonéale continue ambulatoire hors séance||
`SUPENT3`| nombre entier |Nb suppléments pour entraînements à l’hémodialyse hors séance||
`SUPHD`| nombre entier |Nb suppléments pour hémodialyse hors séance||
`TAUX2`| number |Taux de remboursement ||
`TYPORG`| chaîne de caractères |Type de prestation de prélèvement d’organe (type 1, 2, 3 ou 4)||
`TYPORG5`| chaîne de caractères |Prélèvement d’organe de type 5||
`TYPORG6`| chaîne de caractères |Prélèvement d’organe de type 6||
`TYPORG7`| chaîne de caractères |Prélèvement d’organe de type 7||
`TYPORG8`| chaîne de caractères |Prélèvement d’organe de type 8||
`TYPORG9`| chaîne de caractères |Prélèvement d’organe de type 9||
`TYPORGA`| chaîne de caractères |Prélèvement d’organe de type A||
`VALO`| chaîne de caractères |Type de valorisation du séjour||
`MT_GHS_BR`| number |Valorisation BR GHS||
`MT_EXINF_BR`| number |Valorisation BR EXB||
`MT_GHS50_BR`| number |Valorisation BR des séjours avec réhospitalisation dans le même GHM||
`MT_EXH_BR`| number |Valorisation BR EXH||
`MT_SUPPHD_BR`| number |Valorisation BR suppléments pour hémodialyse hors séance||
`MT_SUPPENT1_BR`| number |Valorisation BR suppléments pour entraînements à la dialyse péritonéale automatisée hors séance||
`MT_SUPPENT2_BR`| number |Valorisation BR suppléments pour entraînements à la dialyse péritonéale continue ambulatoire hors séance||
`MT_SUPPENT3_BR`| number |Valorisation BR suppléments pour entraînements à l’hémodialyse hors séance||
`MT_DIP_BR`| number |Valorisation BR suppléments de dialyse DIP||
`MT_PO1_BR`| number |Valorisation BR PO1||
`MT_PO2_BR`| number |Valorisation BR PO2||
`MT_PO3_BR`| number |Valorisation BR PO3||
`MT_PO4_BR`| number |Valorisation BR PO4||
`MT_PO5_BR`| number |Valorisation BR PO5||
`MT_PO6_BR`| number |Valorisation BR PO6||
`MT_PO7_BR`| number |Valorisation BR PO7||
`MT_PO8_BR`| number |Valorisation BR PO8||
`MT_PO9_BR`| number |Valorisation BR PO9||
`MT_POA_BR`| number |Valorisation BR POA||
`MT_9610_BR`| number |Valorisation BR actes menant dans le GHS 9610||
`MT_9619_BR`| number |Valorisation BR actes menant dans le GHS 9619||
`MT_9620_BR`| number |Valorisation BR actes menant dans le GHS 9620||
`MT_9621_BR`| number |Valorisation BR actes menant dans le GHS 9621||
`MT_9622_BR`| number |Valorisation BR actes menant dans le GHS 9622||
`MT_9623_BR`| number |Valorisation BR actes menant dans le GHS 9623||
`MT_9625_BR`| number |Valorisation BR actes menant dans le GHS 9625||
`MT_9631_BR`| number |Valorisation BR actes menant dans le GHS 9631||
`MT_9632_BR`| number |Valorisation BR actes menant dans le GHS 9632||
`MT_9633_BR`| number |Valorisation BR actes menant dans le GHS 9633||
`MT_9615_BR`| number |Valorisation BR actes menant dans le GHS 9615||
`MT_REA_BR`| number |Valorisation BR suppléments de réanimation||
`MT_REP_BR`| number |Valorisation BR suppléments de réanimation pédiatrique||
`MT_SI_BR`| number |Valorisation BR suppléments soins intensifs||
`MT_SC_BR`| number |Valorisation BR suppléments de surveillance continue||
`MT_NN1_BR`| number |Valorisation BR suppléments de néonat sans SI||
`MT_NN2_BR`| number |Valorisation BR suppléments de néonat avec SI||
`MT_NN3_BR`| number |Valorisation BR suppléments de réanimation néonat||
`MT_CAISSON_BR`| number |Valorisation BR suppléments caisson hyperbare||
`MT_RAP_BR`| number |Valorisation BR suppléments de radiothérapie pédiatrique||
`MT_ANT_BR`| number |Valorisation BR suppléments antepartum||
`NB_SDC`| nombre entier |Nb suppléments défibrillateur cardiaque||
`MT_SDC_BR`| number |Valorisation BR suppléments défibrillateur cardiaque||
`MT_SDC_AM`| number |Valorisation AM suppléments défibrillateur cardiaque||
`MT_IVG_BR`| number |Valorisation BR IVG||
`MT_TOT_BR`| number |Valorisation totale BR du séjour.
C’est la somme de : 
mnt_mon, mnt_dmi, mnt_med_atu, mnt_ghs, mnt_exinf, mnt_ghs50, mnt_exh, mnt_supphd, mnt_suppent1, mnt_suppent2, mnt_suppent3, mnt_dip, mnt_po1, mnt_po2, mnt_po3, mnt_po4, mnt_po5, mnt_po6, mnt_po7, mnt_po8, mnt_po9, mnt_poa,mnt_9610, mnt_9619, mnt_9620,  mnt_9621, mnt_9622, mnt_9623, mnt_9625, mnt_9631, mnt_9632, mnt_9633, mnt_9615, mnt_rap, mnt_ant, mnt_rea, mnt_rep, mnt_si, mnt_sc, mnt_nn1, mnt_nn2, mnt_nn3, mnt_caisson, mnt_ivg, mnt_sdc
Pour les séjours AME et SU, c’est la somme de: 
mnt_mon, mnt_dmi, mnt_med_atu, mnt_ghs, mnt_exinf, mnt_ghs50, mnt_exh, mnt_supphd, mnt_suppent1, mnt_suppent2, mnt_suppent3, mnt_dip, mnt_9610, mnt_9619, mnt_9620,  mnt_9621, mnt_9622, mnt_9623, mnt_9625, mnt_9631, mnt_9632, mnt_9633, mnt_9615, mnt_rap, mnt_ant, mnt_rea, mnt_rep, mnt_si, mnt_sc, mnt_nn1, mnt_nn2, mnt_nn3, mnt_caisson,mnt_tjp20e, mnt_fjse, mnt_sdc
||
`COEFREP`| number |coefficient de reprise||
`nb_sup_inno`| nombre entier |Nb suppléments Forfait Innovation ||
`mt_supp_inno`| number |Valorisation BR suppléments Forfait Innovation ||
`mt_supp_inno_am`| number |mnt_supp_inno_am||
`MT_TMF`| nombre entier |Ticket modérateur forfaitaire ||
`MNT_CTC`| number |Valorisation BR suppléments Car-t cells||
`MNT_CTC_AM`| number |Valorisation AM suppléments Car-t cells||
`NB_CTC`| nombre entier |Nb suppléments Car-t cells||
`MT_9611_AM`| chaîne de caractères |Valorisation AM actes menant dans le GHS 9611||
`MT_9612_AM`| chaîne de caractères |Valorisation AM actes menant dans le GHS 9612||
`MT_9624_AM`| chaîne de caractères |Valorisation AM actes menant dans le GHS 9624||
`MT_FJS_AME`| année et mois |Valorisation Forfait Journalier de Sortie (uniquement pour les séjours AME)||
`MT_IVGFJS_AM`| chaîne de caractères |Valorisation Forfait Journalier de Sortie (uniquement pour les séjours AME)||
`MT_TJP20_AME`| année et mois |Valorisation 20% TJP (uniquement pour les séjours AME)||
`NB_9611`| chaîne de caractères |Nombre d’actes menant dans le GHS 9611||
`NB_9612`| chaîne de caractères |Nombre d’actes menant dans le GHS 9612||
`NB_9624`| chaîne de caractères |Nombre d’actes menant dans le GHS 9624||
`GHSMIN`| nombre entier |Nb séjours avec minoration forfaitaire liste en sus||
`MT_GHSMIN_AM`| number |Valorisation AM des séjours avec minoration forfaitaire liste en sus||
`MT_18`| number |Montant participation forfaitaire du 18 euros||
`SEJXINF`| chaîne de caractères |Type de séjour inférieur à la borne extrême basse ||