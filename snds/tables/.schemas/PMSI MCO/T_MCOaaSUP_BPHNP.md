### Schéma


- Titre : Fich sup BP HN etb producteur
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`MNT_BHN_LAB_PRI`| number |Montant total de BHN facturés à l'encontre d'un laboratoire privé ou d'un cabinet (en centimes d'euro)||
`MNT_BHN_ETB_EXDG`| number |Montant total de BHN facturés à l'encontre d'un établissement de santé ex-DG (en centimes d'euro)||
`MNT_BHN_ETB_EXOQN`| number |Montant total de BHN facturés à l'encontre d'un établissement de santé ex-OQN (en centimes d'euro)||
`MNT_PHN_LAB_PRI`| number |Montant total de PHN facturés à l'encontre d'un laboratoire privé ou d'un cabinet (en centimes d'euro)||
`MNT_PHN_ETB_EXDG`| number |Montant total de PHN facturés à l'encontre d'un établissement de santé ex-DG (en centimes d'euro)||
`MNT_PHN_ETB_EXOQN`| number |Montant total de PHN facturés à l'encontre d'un établissement de santé ex-OQN (en centimes d'euro)||
`ANN_MOI`| date |Année+Mois||
`ETA_NUM`| chaîne de caractères |N° FINESS||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`MNT_LC_ETB_EXDG`| number |Montant total d’actes de la LC dont la facture a été émise  à l'encontre d'un établissement de santé ex-DG (en centimes d'euro)||
`MNT_LC_ETB_EXOQN`| number |Montant total d’actes de la LC dont la facture a été émise  à l'encontre d'un établissement de santé ex-OQN (en centimes d'euro)||
`MNT_LC_LAB_PRI`| number |Montant total d’actes de la LC dont la facture a été émise  à l'encontre d'un laboratoire privé ou d'un cabinet (en centimes d'euro)||
`MNT_RIHN_ETB_EXDG`| number |Montant total d’actes du RIHN dont la facture a été émise à l'encontre d'un établissement de santé ex-DG (en centimes d'euro)||
`MNT_RIHN_ETB_EXOQN`| number |Montant total d’actes du RIHN dont la facture a été émise à l'encontre d'un établissement de santé ex-OQN (en centimes d'euro)||
`MNT_RIHN_LAB_PRI`| number |Montant total d’actes du RIHN dont la facture a été émise à l'encontre d'un laboratoire privé ou d'un cabinet (en centimes d'euro)||