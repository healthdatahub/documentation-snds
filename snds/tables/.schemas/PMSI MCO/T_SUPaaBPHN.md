### Schéma


- Titre : B et P hors nomenclature (extension de fichier .bphn)
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACT_COD`| chaîne de caractères |Code acte||
`ANN_MOI`| date |Mois et année||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`NBR_AUT_ETP`| chaîne de caractères |Nombre réalisé pour un autre établissement public||
`NBR_CON_EXT`| chaîne de caractères |Nombre réalisé pour consultants externes||
`NBR_FAC_AUT_ETP`| chaîne de caractères |Nombre facturé pour un autre établissement public||
`NBR_FAC_CON_EXT`| chaîne de caractères |Nombre facturé pour consultants externes||
`NBR_FAC_INC_DEM`| chaîne de caractères |Nombre facturé autre ou demandeur inconnu||
`NBR_FAC_PHE`| chaîne de caractères |Nombre facturé pour patients hospitalisés au sein de l’établissement||
`NBR_FAC_SEC_LIB`| chaîne de caractères |Nombre facturé  émanant du secteur libéral||
`NBR_FAC_TOT`| chaîne de caractères |Nombre total facturé||
`NBR_INC_DEM`| chaîne de caractères |Nombre réalisé autre ou demandeur inconnu||
`NBR_PHE`| chaîne de caractères |Nombre réalisé pour  patients hospitalisés au sein de l’établissement||
`NBR_REA_TOT`| chaîne de caractères |Nombre total réalisé||
`NBR_SEC_LIB`| chaîne de caractères |Nombre réalisé émanant du secteur libéral||