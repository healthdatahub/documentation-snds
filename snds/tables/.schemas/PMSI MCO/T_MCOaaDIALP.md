### Schéma


- Titre : Fich comp dialyse péritonéale en sus
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN`| chaîne de caractères |Année période||
`ETA_NUM`| chaîne de caractères |N° FINESS||
`ETA_NUM_ENT`| chaîne de caractères |N° FINESS du fichier d'entrée||
`MOI`| date |N° période (mois)||
`NBR_PRS`| nombre entier |Nombre de suppléments DIP||
`PRS_COD`| chaîne de caractères |Code Prestation||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA||
`FAC_SEM_NBR`| chaîne de caractères |Nombre de semaine facturées||