### Schéma


- Titre : Fich comp radiothérapie
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`ETE_GHS_NUM`| nombre entier |Code du supplément (n° GHS)||
`GHS_SUP_COD`| chaîne de caractères |Code du supplément (n° GHS)||
`NBR_SUP`| nombre entier |Nombre de suppléments (Nb actes menant dans le GHS)||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA ||
`RSS_NUM`| chaîne de caractères |Numéro de version du format du RSA||