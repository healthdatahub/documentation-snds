### Schéma


- Titre : Fich sup FFM (forfait petit matériel)
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ETA_NUM`| chaîne de caractères |Numéro FINESS d’inscription ePMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`ANN_MOI`| date |Année+Mois||
`NBR_FFM`| nombre entier |Nombre total réalisé||