### Schéma


- Titre : OQN Entete facture
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`AGE_ANN`| année |Age (en années)||
`COD_CIV`| chaîne de caractères |Code civilité ||
`COD_PEC`| chaîne de caractères |Code de prise en charge||
`COD_SEX`| chaîne de caractères |Sexe||
`CON_TYP`| chaîne de caractères |Type de contrat souscrit auprès d'un organisme complémentaire||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`EXO_TM`| chaîne de caractères |Justification d'exonération du TM||
`FAC_ETL`| chaîne de caractères |Etat de liquidation de la facture||
`GES_COD`| chaîne de caractères |Code gestion||
`HON_AM_MNR`| number |Total honoraire remboursable AM||
`HON_MNT`| number |Total honoraire Facturé||
`HON_OC_MNR`| number |Total remboursable OC pour les honoraires||
`NAT_ASS`| chaîne de caractères |Nature assurance||
`NOE_RGM`| chaîne de caractères |Code Gd régime||
`NUM_DAT_AT`| chaîne de caractères |Numéro accident du travail ou date d’accident de droit commun||
`NUM_FAC`| chaîne de caractères |N° séquentiel de facture||
`OPE_NAT`| chaîne de caractères |Nature opération||
`ORG_CPL_NUM`| chaîne de caractères |N° d’organisme complémentaire||
`PAS_OC_MNT`| number |Total participation assuré avant OC||
`PAT_CMU`| chaîne de caractères |Patient bénéficiaire de la CMU||
`PH_AMO_MNR`| number |Total remboursable AMO Prestation hospitalières||
`PH_BRM`| number |Total Base Remboursement Prestation hospitalière||
`PH_MNT`| number |Montant total facturé pour  PH||
`PH_OC_MNR`| number |Total remboursable OC pour les PH||
`RNG_BEN`| chaîne de caractères |Rang de bénéficiaire||
`RNG_NAI`| chaîne de caractères |Rang de naissance||
`RSA_NUM`| chaîne de caractères | N° séquentiel (le même que pour les RSA)||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`EXE_SOI_DTD`| date |Date d'entrée du séjour||
`EXE_SOI_DTF`| date |Date de sortie du séjour||
`AGE_JOU`| nombre entier |Age en jours||
`BDI_COD`| chaîne de caractères |Code géographique ||
`COD_POST`| chaîne de caractères |Code postal||
`BDI_DEP`| chaîne de caractères |Commune de résidence||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée du séjour||
`EXE_SOI_AMF`| chaîne de caractères |Date de sortie du séjour||
`POST_DEP`| chaîne de caractères |Code postal||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1 : ancien, 2 : nouveau)||
`SOR_ANN`| année |Année de sortie||
`SOR_MOI`| date |Mois de sortie||
`NIR_ANO_MAM`| chaîne de caractères |N° anonyme mère-enfant||