### Schéma


- Titre : Fich comp prélévement d'organe
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN`| chaîne de caractères |Année période||
`ANN_PO`| chaîne de caractères |Année de la date de réalisation du prélèvement d'organe||
`DELAI`| nombre entier |Délai entre la date d’entrée du séjour et la date de réalisation du prélèvement d'organe||
`ETA_NUM`| chaîne de caractères |N° FINESS e-PMSI||
`ETA_NUM_ENT`| chaîne de caractères |N° FINESS du fichier d'entrée||
`MOI`| date |N° période (mois)||
`MOI_PO`| date |Mois de la date de réalisation du prélèvement d'organe||
`PO_COD`| chaîne de caractères |Code PO||
`PRS_TYP`| chaîne de caractères |Type de prestation||
`RSA_NUM`| chaîne de caractères |N° d'index du RSA||