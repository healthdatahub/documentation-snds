### Schéma


- Titre : Fich sup BP HN nb
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ACT_COD`| chaîne de caractères |Code acte||
`ANN_MOI`| date |Année+Mois||
`NBR_ACT_CON_EXT`| nombre entier |Dont nombre d'actes réalisés pour consultants externe||
`NBR_ACT_PAT_AUT_ETB`| nombre entier |Nombre d'actes réalisés pour les patients d'autres établissements ou de laboratoires extérieurs||
`NBR_ACT_PAT_ETB`| nombre entier |Nombre d'actes réalisés pour patients de l'etab||
`NBR_ACT_PAT_HOS`| nombre entier |Dont nombre d'actes réalisés pour patients hospitalisés||
`ETA_NUM`| chaîne de caractères |N° FINESS||
`FIC_TYP`| chaîne de caractères |Type de fichier||