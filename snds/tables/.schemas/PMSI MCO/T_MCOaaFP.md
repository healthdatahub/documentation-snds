### Schéma


- Titre : OQN DMI en sus
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM, RSA_NUM`=> table [T_MCOaaB](/tables/T_MCOaaB)[ `ETA_NUM`, `RSA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`DEL_DAT_ENT`| nombre entier |Délai par rapport à la date d'entrée||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`FAC_MNT`| number |Montant total facturé||
`LPP_COD`| chaîne de caractères |Code référence LPP||
`LPP_PRI_UNI`| number |Prix d'achat unitaire||
`LPP_PU_DEV`| number |Tarif référence LPP/ Prix Unitaire sur devis||
`LPP_QUA`| nombre entier |Quantité||
`MNT_UNI_ECA`| number |Montant unitaire de l'écart indemnisable||
`NUM_FAC`| chaîne de caractères |N° séquentiel de facture||
`RSA_NUM`| chaîne de caractères | N° séquentiel (le même que pour les RSA)||
`TIP_PRS_IDE`| nombre entier |Code référence LPP (format numérique)||
`TOT_MNT_ECA`| number |Montant total de l'écart indemnisable||
`TYP_ART`| chaîne de caractères |Type d'enregistrement||
`EXE_SOI_DTD`| date |Date de soins||
`EXE_SOI_AMD`| chaîne de caractères |Date d'entrée du séjour||
`FAC_MNT1`| chaîne de caractères |Montant facturé 1||
`FAC_MNT2`| chaîne de caractères |Montant facturé 2||
`LPP_COD1`| chaîne de caractères |Code LPP 1||
`LPP_COD2`| chaîne de caractères |Code LPP 2||
`LPP_PRI_UNI1`| chaîne de caractères |Tarif référence LPP/Prix unitaire 1||
`LPP_PRI_UNI2`| chaîne de caractères |Tarif référence LPP/Prix unitaire 2||
`LPP_QUA1`| chaîne de caractères |Quantité 1||
`LPP_QUA2`| chaîne de caractères |Quantité 2||
`ENT_ANN`| année |Année début de séjour||
`ENT_MOI`| date |Mois de début de séjour||
`RSF_TYP`| chaîne de caractères |Type de format RSF (1 : ancien, 2 : nouveau)||