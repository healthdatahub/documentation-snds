### Schéma


- Titre : Alternatives à la dialyse (extension de fichier .ald)
<br />



- Clé(s) étrangère(s) : <br />
`ETA_NUM`=> table [T_MCOaaE](/tables/T_MCOaaE)[ `ETA_NUM` ]<br />

 
### Liste des variables

Nom | Type | Description | Règle de gestion
-|-|-|-
`ANN_MOI`| date |Mois et année||
`ETA_NUM`| chaîne de caractères |Numéro FINESS e-PMSI||
`FIC_TYP`| chaîne de caractères |Type de fichier||
`NBR_REA_EAL`| chaîne de caractères |Nombre réalisé en activité libérale||
`NBR_REA_HAL`| chaîne de caractères |Nombre réalisé hors activité libérale||
`PRS_COD`| chaîne de caractères |Code Prestation||