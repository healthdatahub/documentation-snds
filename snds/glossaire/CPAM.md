# CPAM - Caisse primaire d'assurance maladie
<!-- SPDX-License-Identifier: MPL-2.0 -->

Une Caisse primaire d'assurance maladie est un organisme qui assure les relations de proximité avec les ayants droit de la <link-previewer href="Cnam.html" text="CNAM" preview-title="CNAM - Caisse nationale de l’assurance maladie" preview-text="La Caisse nationale de l’assurance maladie est la « tête de réseau » opérationnelle du régime d’assurance maladie obligatoire en France. " />. 

# Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/Caisse_primaire_d%27assurance_maladie)
