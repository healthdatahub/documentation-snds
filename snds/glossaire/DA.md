# DAS - Diagnostic associé significatif
<!-- SPDX-License-Identifier: MPL-2.0 -->

Un diagnostic associé significatif (ou diagnostic associé (DA)) est un diagnostic ou une pathologie majorant l’effort de soins et les moyens utilisés, par rapport à la morbidité principale. En effet, un DAS est un problème de santé pris en charge en plus du DP, tel une complication de celui-ci, une complication de son traitement ou une affection distincte supplémentaire.  
Les DAS influencent le groupage des séjours dans un <link-previewer href="GHM.html" text="GHM" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO."/> pour le PMSI MCO et dans un <link-previewer href="RHS.html" text="RHS" preview-title="RHS - Résumé Hebdomadaire Standardisé"/> (Résumé Hebdomadaire Standardisé) pour le PMSI SSR (Soins de Suite ou de Réadaptation).

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/diagnostic-associe-da) sur le site internet du ministère
- Plus d'informations dans le [glossaire](https://www.atih.sante.fr/glossaire) et sur le [site](https://www.atih.sante.fr/) de l'ATIH
