# NGAP - Nomenclature générale des actes professionnels
<!-- SPDX-License-Identifier: MPL-2.0 -->

Nomenclature des actes cliniques réalisés par les médecins (consultations des généralistes ou des spécialistes par exemple) et des actes médico-techniques des paramédicaux (kinésithérapeutes, infirmiers, …). 

Remplacée dans une très large mesure par la classification commune des actes médicaux (<link-previewer href="CCAM.html" text="CCAM" preview-title="CCAM - Classification commune des actes médicaux" preview-text="La classification commune des actes médicaux est une nomenclature française destinée à coder les gestes pratiqués par les médecins, gestes techniques dans un premier temps puis, par la suite, les actes intellectuels cliniques."/>) pour les actes techniques médicaux, elle est encore majoritairement utilisée par les auxiliaires médicaux. 

Elle est disponible en téléchargement sur le site de l'Assurance Maladie : [Nomenclature NGAP](https://www.ameli.fr/gironde/medecin/exercice-liberal/facturation-remuneration/consultations-actes/nomenclatures-codage/ngap).

# Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/Nomenclature_g%C3%A9n%C3%A9rale_des_actes_professionnels)
- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/ngap-nomenclature-generale-des-actes-professionnels) sur le site internet du ministère
