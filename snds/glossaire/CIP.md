# CIP - Code Identifiant de Présentation
<!-- SPDX-License-Identifier: MPL-2.0 -->


## Ailleurs dans la documentation
- Fiche thématique sur les [médicaments](../fiches/medicament.md)

## Références
- [Répertoire des spécialités pharmaceutiques](https://agence-prd.ansm.sante.fr/php/ecodex/index.php#result) de l'[ANSM](ANSM.md)
- [Club InterPharmaceutique](https://www.cipmedicament.org/)
- [Correspondance CIP/UCD](https://smt.esante.gouv.fr/terminologie-cip_ucd/)