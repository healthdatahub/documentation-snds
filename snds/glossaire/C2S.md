# C2S - Complémentaire Santé Solidaire (CSS)
<!-- SPDX-License-Identifier: MPL-2.0 -->

<TagLinks />

A partir du 1er novembre 2019, l'[ACS](../fiches/acs.md) et la [CMU-C](../fiches/cmu_c.md) ont fusionné pour former la [Complémentaire Santé Solidaire](https://www.complementaire-sante-solidaire.gouv.fr/).  
On distingue la C2S et la C2SP selon que les bénéficiaires participent ou non au paiement de sa complémentaire.
