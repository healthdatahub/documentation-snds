# CCAM - Classification commune des actes médicaux
<!-- SPDX-License-Identifier: MPL-2.0 -->

La classification commune des actes médicaux est une nomenclature française destinée à coder les gestes pratiqués par les médecins, gestes techniques dans un premier temps puis, par la suite, les actes intellectuels cliniques. 

Elle a succédé au catalogue des actes médicaux (CdAM) en milieu hospitalier et, pour les actes techniques, à la nomenclature générale des actes professionnels (<link-previewer href="NGAP.html" text="NGAP" preview-title="NGAP - Nomenclature générale des actes professionnels" preview-text="Nomenclature des actes cliniques réalisés par les médecins (consultations des généralistes ou des spécialistes par exemple) et des actes médico-techniques des paramédicaux (kinésithérapeutes, infirmiers, …). " />) en secteur libéral et hospitalier.

# Références
- Site [Aide au codage](https://www.aideaucodage.fr/ccam) de la Cnam
- La [CCAM en ligne](https://www.ameli.fr/accueil-de-la-ccam/index.php) de la Cnam
- [Site de la CCAM](https://www.ameli.fr/accueil-de-la-ccam/index.php) par l'assurance maladie, présenté sur [cette page](https://www.ameli.fr/medecin/exercice-liberal/remuneration/nomenclatures-codage/codage-actes-medicaux-ccam).
- [Page wikipedia](https://fr.wikipedia.org/wiki/Classification_commune_des_actes_m%C3%A9dicaux)
- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/classification-commune-des-actes-medicaux-ccam) sur le site internet du ministère
