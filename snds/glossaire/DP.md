# DP - Diagnostic principal
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le diagnostic principal est le motif des soins qui justifient l’hospitalisation dans le PMSI MCO (Médecine-Chirurgie-Obstétrie).  
C'est à dire qu'il commande l’orientation dans une catégorie majeure de diagnostic au moment du groupage, et peut influer sur le classement dans un groupe homogène de malades (<link-previewer href="GHM.html" text="GHM" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO."/>).  
Le [Manuel des GHM](https://www.atih.sante.fr/manuel-des-ghm-version-11-version-complete) est disponible en téléchargement sur la site de l'<link-previewer href="ATIH.html" text="ATIH" preview-title="ATIH - Agence technique de l'information sur l'hospitalisation" preview-text="L’ATIH est un établissement public de l’État chargé de différentes missions dans les secteurs sanitaire et médico-social. Dans le cadre du SNDS, l’ATIH est en charge du PMSI, la composante relative à l’activité des établissements de santé."/>.

Avant la version 11 de la classification des GHM en <link-previewer href="MCO.html" text="MCO" preview-title="MCO - Médecine, chirurgie, obstétrique" preview-text="Terme utilisé pour désigner les activités aigus de courte durée réalisées dans les établissements de santé, en hospitalisation (avec ou sans hébergement) ou en consultations externes."/>, il correspondait aux soins ayant mobilisé l’essentiel de l’effort médical et soignant. 


# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/diagnostic-principal-dp) sur le site internet du ministère  
- Plus d'informations sur le [site](https://www.atih.sante.fr/manuel-des-ghm-version-11-version-complete) de l'ATIH 
- [Statistiques](https://www.scansante.fr/applications/statistiques-activite-MCO-par-diagnostique-et-actes) de l'activité par DP en MCO sur le site ScanSante
