# EHPA - Etablissement d’Hébergement pour Personnes Agées
<!-- SPDX-License-Identifier: MPL-2.0 -->

Les EHPA regroupent les résidences d’hébergement temporaire, les résidences-autonomie, les maisons de retraite, les unités de soins de longue durée, etc.
