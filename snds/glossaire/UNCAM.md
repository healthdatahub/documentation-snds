# UNCAM - Union nationale des caisses d’assurance maladie
<!-- SPDX-License-Identifier: MPL-2.0 -->

L'Union nationale des caisses d'assurance maladie (UNCAM) est une instance créée par la loi du 13 août 2004 relative à l’assurance maladie, pour regrouper les trois principaux régimes d'assurance maladie :
- le <link-previewer href="RG.html" text="régime général" preview-title="RG - Régime Général" preview-text="Régime Général" /> d'assurance maladie, 
- le régime agricole (<link-previewer href="MSA.html" text="Mutualité Sociale Agricole" preview-title="MSA - Mutualité sociale agricole" preview-text="La Mutualité sociale agricole (MSA) est le régime de protection sociale obligatoire des personnes salariées et non salariées des professions agricoles." />), 
- le Régime social des indépendants (<link-previewer href="RSI.html" text="RSI" preview-title="RSI - Régime social des Indépendants" preview-text="Le régime social des indépendants (RSI), créé en 2006 et dissous début 2018, est un « organisme de prévoyance sociale à régime spécial de la Sécurité Sociale ». " />). 

# Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/Union_nationale_des_caisses_d%27assurance_maladie)
- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/regles-de-facturation/les-regles-de-facturation-glossaire/article/uncam) sur le site internet du ministère
