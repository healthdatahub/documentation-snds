# Carte Vitale
<!-- SPDX-License-Identifier: MPL-2.0 -->

La carte Vitale est la carte de l'assurance maladie.

Il s'agit d'une carte à puce permettant de justifier les droits du titulaire, ou de ses ayants droit, mineurs ou conjoint, à la couverture par un organisme de sécurité sociale des dépenses de santé en France. 

Les <link-previewer href="PS.html" text="professionnel de santé" preview-title="PS - Professionnel de santé " preview-text="Les Professionnel de santé (PS) sont des personnes physiques pouvant prescrire ou exécuter des prestations de santé." /> (médecin, dentiste, etc.) utilisent la carte Vitale pour établir les <link-previewer href="feuille_soin.html" text="feuilles de soins" preview-title="Feuille de soin" preview-text="La feuille de soin regroupe l'ensemble des prestations réalisées par un soignant et permet le remboursement de ces prestations auprès de l'assurance maladie." /> électronique, qui remplacent les feuilles de soins papier.
Cette feuille de soin est directement transmises à l'organisme d'assurance maladie. 

La carte Vitale est identique pour tous les régimes obligatoires d'assurance maladie, et utilisable seulement en France. 
Elle est complémentaire de la carte européenne d'assurance maladie qui ne peut pas servir sur le territoire national. 

# Références

- [Présentation](https://www.ameli.fr/assure/remboursements/etre-bien-rembourse/carte-vitale) sur le site de l'Assurance Maladie
- [Page wikipedia](https://fr.wikipedia.org/wiki/Carte_Vitale)
