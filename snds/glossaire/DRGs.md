# DRGs - Diagnosis Related Groups
<!-- SPDX-License-Identifier: MPL-2.0 -->

La classification des Diagnosis Related Groups, ou groupes apparentés par diagnostic, a été élaborée dans les années 1970 aux Etats-Unis. Elle repose sur le classement des séjours hospitaliers en un nombre volontairement limité de groupes, caractérisés par une double homogénéité médicale et économique.  

La classification française des groupes homogènes de malades (<link-previewer href="GHM.html" text="GHM" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO." />), ainsi que la plupart des autres classifications médico-économiques utilisées dans le reste du monde, sont dérivées du système des DRGs.

Pour la France, le manuel des GHM est mis à jour annuellement et est téléchargeable sur le site de l'ATIH en suivant ce [lien](https://www.atih.sante.fr/manuel-des-ghm-version-11-version-complete).


# Références

- [Page wikipedia](https://en.wikipedia.org/wiki/Diagnosis-related_group)
- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/diagnosis-related-groups-drgs) sur le site internet du ministère  
- Plus d'informations sur le [site](https://www.atih.sante.fr/manuel-des-ghm-version-11-version-complete) de l'ATIH 
- [Statistiques](https://www.scansante.fr/applications/statistiques-activite-MCO-par-GHM) de l'activité par GHM sur le site ScanSante