# GHPC - Groupe homogène de prise en charge
<!-- SPDX-License-Identifier: MPL-2.0 -->

Combinaison de 3 variables composant un séjour en hospitalisation à domicile : 
- le mode de prise en charge principal, 
- le mode de prise en charge associé, 
- un indice de mesure de la dépendance (indice de Karnofsky). 

Le GHPC est obtenu à partir des informations recueillies dans les résumés par sous-séquence de soins (<link-previewer href="RPSS.html" text="RPSS" preview-title="RPSS - Résumé par sous-séquence de soins" preview-text="Aucune définition détaillée n'existe pour l'instant dans le glossaire. Pour contribuer, référrez-vous au guide de contribution." />).

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/groupe-homogene-de-prise-en-charge-ghpc) sur le site internet du ministère
