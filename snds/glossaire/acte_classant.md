# Acte classant
<!-- SPDX-License-Identifier: MPL-2.0 -->

Un acte classant est un acte médico-technique codé dans le résumé d’unité médicale (<link-previewer href="RUM.html" text="RUM" preview-title="RUM - Résumé d’unité médicale" preview-text="Un résumé d’unité médicale (RUM) est produit à la fin de chaque séjour de malade dans une unité médicale assurant des soins de médecine, chirurgie, obstétrique et odontologie. Il contient des informations d’ordre administratif et médical, codées selon des nomenclatures et des classifications standardisées, afin de bénéficier d’un traitement automatisé." />) de chaque patient hospitalisé, d’après la classification commune des actes médicaux <link-previewer href="CCAM.html" text="CCAM" preview-title="CCAM - Classification commune des actes médicaux" preview-text="La classification commune des actes médicaux est une nomenclature française destinée à coder les gestes pratiqués par les médecins, gestes techniques dans un premier temps puis, par la suite, les actes intellectuels cliniques. " />. 

Sa mention dans le RUM est indispensable car il est pris en compte par l’algorithme qui classe chaque séjour dans le groupement homogène de malade <link-previewer href="GHM.html" text="GHM" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO. " /> adéquat (cf. <link-previewer href="groupage.html" text="groupage" preview-title="Groupage" preview-text="Le groupage est un processus de classement des informations médico-administratives recueillies par les établissements, dans des groupes homogènes :" />).  

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/acte-classant) sur le site internet du ministère 

