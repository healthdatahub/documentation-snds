# Groupage
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le groupage est un processus de classement des informations médico-administratives recueillies par les établissements, dans des groupes homogènes :
- de malades <link-previewer href="GHM.html" text="GHM" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO. " />, 
- de journées<link-previewer href="GHJ.html" text="GHJ" preview-title="GHJ - Groupe homogène de journées" preview-text="Le groupe homogène de journées est la catégorie élémentaire de la classification médico-économique propre au PMSI en SSR. Il se fonde sur les informations médico-administratives figurant dans le résumé hebdomadaire standardisé (RHS réalisé par les établissements pour chaque patient." />, 
- de tarifs <link-previewer href="GHT.html" text="GHT" preview-title="GHT - Groupe homogène de tarifs" preview-text="Le groupe homogène de tarifs correspond aux tarifs journaliers applicables en hospitalisation à domicile (HAD. " />).

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/groupage) sur le site internet du ministère
