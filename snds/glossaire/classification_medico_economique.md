# Classification médico-économique
<!-- SPDX-License-Identifier: MPL-2.0 -->

La classification médico-économique consiste à classer des pathologies en groupes cohérents d’un point de vue médical et en termes de coûts. 

Il s’agit de : 
- la classification des groupes homogènes de malades (<link-previewer href="GHM.html" text="GHM" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO. " />) en médecine, chirurgie, obstétrique et odontologie (<link-previewer href="MCO.html" text="MCO" preview-title="MCO - Médecine, chirurgie, obstétrique" preview-text="Terme utilisé pour désigner les activités aigus de courte durée réalisées dans les établissements de santé, en hospitalisation (avec ou sans hébergement) ou en consultations externes." />) ; 
- de la classification des groupes homogènes de journées (<link-previewer href="GHJ.html" text="GHJ" preview-title="GHJ - Groupe homogène de journées" preview-text="Le groupe homogène de journées est la catégorie élémentaire de la classification médico-économique propre au PMSI en SSR. Il se fonde sur les informations médico-administratives figurant dans le résumé hebdomadaire standardisé (RHS réalisé par les établissements pour chaque patient." />), en soins de suite et de réadaptation (<link-previewer href="SSR.html" text="SSR" preview-title="SSR - Soins de suite et de réadaptation" preview-text="Les soins de suite et de réadaptation (SSR) désignent un dispositif qui a pour objet de prévenir ou de réduire les conséquences fonctionnelles, physiques, cognitives, psychologiques ou sociales des déficiences et des limitations de capacité des patients et de promouvoir leur réadaptation et leur réinsertion. " />) ; 
- de la classification des groupes homogènes de tarifs (<link-previewer href="GHT.html" text="GHT" preview-title="GHT - Groupe homogène de tarifs" preview-text="Le groupe homogène de tarifs correspond aux tarifs journaliers applicables en hospitalisation à domicile (HAD. " />), en hospitalisation à domicile (<link-previewer href="HAD.html" text="HAD" preview-title="HAD - Hospitalisation à domicile" preview-text="L’hospitalisation à domicile (HAD) est une hospitalisation à temps complet au cours de laquelle les soins sont effectués au domicile de la personne. " />).

# Références

- [Page wikipedia]()

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/classification-medico-economique) sur le site internet du ministère
