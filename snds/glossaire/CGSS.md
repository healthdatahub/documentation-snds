# CGSS - Caisse générale de sécurité sociale
<!-- SPDX-License-Identifier: MPL-2.0 -->

Les Caisses générales de sécurité sociale (CGSS) assurent, dans les <link-previewer href="Dom.html" text="Dom" preview-title="Dom - Départements d'outre-mer" preview-text="Les départements d'outre-mer (Dom) sont des collectivités territoriales, intégrées à la République française au même titre que les départements ou régions de la France métropolitaine. " />, l'ensemble des rôles de Sécurité sociale (notamment les branches « Maladie », « Vieillesse et veuvage » et « Recouvrement »), dévolus en métropole aux [URSSAF](https://fr.wikipedia.org/wiki/URSSAF), [CARSAT](https://fr.wikipedia.org/wiki/CARSAT) et <link-previewer href="CPAM.html" text="CPAM" preview-title="CPAM - Caisse primaire d'assurance maladie" preview-text="Une Caisse primaire d'assurance maladie est un organisme qui assure les relations de proximité avec les ayants droit de la CNAM. " />. 

## Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/Caisse_g%C3%A9n%C3%A9rale_de_s%C3%A9curit%C3%A9_sociale)