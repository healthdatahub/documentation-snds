# ATC - Anatomique, Thérapeutique et Chimique
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le système de classification ATC (Anatomique, Thérapeutique et Chimique) est utilisé pour classer les médicaments. Les médicaments sont divisés en groupes selon l'organe ou le système sur lequel ils agissent ou leurs caractéristiques thérapeutiques et chimiques.  
C'est le *Collaborating Centre for Drug Statistics Methodology* de l'[OMS](OMS.md) qui le contrôle. Chaque code ATC se divise en 5 niveaux qui sont définis sur le site de l'OMS : [ATC classification](https://www.who.int/tools/atc-ddd-toolkit/atc-classification)

En France, deux autres classifications du médicament ont cours : la classification [UCD](UCD.md) et la codification [CIP](CIP.md).

# Références
- [Page Wikipédia](https://fr.wikipedia.org/wiki/Classification_ATC)
- [Site](https://www.who.int/standards/classifications/other-classifications/the-anatomical-therapeutic-chemical-classification-system-with-defined-daily-doses) de l'OMS 
- [Séries Medic'AM](https://www.assurance-maladie.ameli.fr/etudes-et-donnees/medicaments-classe-atc-medicam) de la Cnam par classe ATC