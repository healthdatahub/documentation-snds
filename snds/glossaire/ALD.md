# ALD - Affection de Longue Durée
<!-- SPDX-License-Identifier: MPL-2.0 -->

L'Affection longue durée (ALD) concerne une maladie dont la gravité et/ou le caractère chronique nécessite un traitement prolongé.

## Ailleurs dans la documentation
- [Publication](https://www.ameli.fr/paris/assure/droits-demarches/maladie-accident-hospitalisation/affection-longue-duree-ald/affection-longue-duree-maladie-chronique) sur le site Ameli
- Fiche thématique [bénéficiaires ALD](../fiches/beneficiaires_ald.md) où se trouvent notamment la liste des codes de l'ensemble des ALD ;
- Fiche thématique [requête type ALD](../fiches/requete_type_ald.md).
