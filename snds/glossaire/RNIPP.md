# RNIPP - Répertoire national d'identification des personnes physiques
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le répertoire national d'identification des personnes physiques (RNIPP) est un répertoire français, tenu par l'Insee depuis 1946. 

Le RNIPP recense l'ensemble des personnes vivantes et décédées, nées en France ou nées à l'étranger et venues travailler en France. 

## Contenu

Le RNIPP contient uniquement des informations sur l'état-civil : 
- le numéro d'inscription au répertoire (<link-previewer href="NIR.html" text="NIR" preview-title="NIR - Numéro de sécurité sociale" preview-text="Le numéro d'inscription au répertoire (NIR) est le numéro d'inscription au répertoire national d'identification des personnes physiquesRNIPP." />) ;
- le nom de famille et parfois le nom d'usage, ou nom marital ;
- les prénoms ;
- le sexe ;
- la date et le lieu de naissance ;
- le numéro de l'acte de naissance ;
- la date et le lieu de décès pour les personnes décédées ;
- le numéro de l'acte de décès pour les personnes décédées.

## Alimentation

Le RNIPP est mis à jour quotidiennement grâce aux bulletins statistiques de l'état civil établis et adressés à l'Insee par les communes à la suite de naissances, décès, reconnaissances, et mentions portées en marge des actes de naissance pour les personnes nées en France métropolitaine et dans les départements d'outre-mer (<link-previewer href="Dom.html" text="Dom" preview-title="Dom - Départements d'outre-mer" preview-text="Les départements d'outre-mer (Dom) sont des collectivités territoriales, intégrées à la République française au même titre que les départements ou régions de la France métropolitaine. " />).

Le RNIPP alimente le Système national de gestion des identifiants (<link-previewer href="SNGI.html" text="SNGI" preview-title="SNGI - Système national de gestion des identifiants" preview-text="Le Système national de gestion des identifiants (SNGI) répertorie l'état civil et le NIR des personnes relevant d’un régime de sécurité sociale. " />) géré par la <link-previewer href="Cnav.html" text="CNAV" preview-title="Cnav - Caisse nationale d'assurance vieillesse" preview-text="La Caisse nationale d'assurance vieillesse (Cnav) gère la retraite des salariés, hors secteur agricole et hors fonction publique." />.

Réciproquement, le RNIPP est alimenté par le SNGI pour les personnes nées à l'étranger et venues travailler en France.  

## Utilisations

Le RNIPP a plusieurs finalités.

C'est un instrument fondamental de l'état civil en France. 
Il permet de lever le doute sur les homonymies, ou de préciser si une personne est en vie ou décédée.
Il est ainsi utilisé par l'administration fiscale et les caisses de retraite pour certifier des états civils.

Il participe à la veille sanitaire. 
Depuis la canicule de l'été 2003, l'Insee transmet quotidiennement à <link-previewer href="SpF.html" text="Santé publique France" preview-title="SpF - Santé publique France" preview-text="Santé publique France (SpF), de son nom officiel Agence nationale de santé publique (ANSP) est un établissement public administratif sous tutelle du ministère chargé de la Santé." /> (anciennement InVs) des informations sur les décès.

Il permet d'élaborer des statistiques démographiques.

# Références

- [Fiche](https://www.insee.fr/fr/metadonnees/definition/c1602) sur le site de l'Insee
- [Page wikipedia](https://fr.wikipedia.org/wiki/R%C3%A9pertoire_national_d%27identification_des_personnes_physiques)
