# DR - Diagnostic relié
<!-- SPDX-License-Identifier: MPL-2.0 -->

le DR est une information, une précision qui fait partie du <link-previewer href="DP.html" text="DP" preview-title="DP - Diagnostic Principal" preview-text="Un diagnostic principal est le motif des soins qui justifient l’hospitalisation dans le PMSI MCO."/>. Il répond à la question : pour quel problème de santé la prise en charge enregistrée comme DP a-t-elle été faite ? Les notions de DP et de couple DP-DR sont de ce fait équivalentes, rassemblées sous l’appellation de « morbidité principale » du résumé d’unité médicale (<link-previewer href="RUM.html" text="RUM" preview-title="RUM - Résumé d'Unité Médicale" preview-text="Un RUM est produit à la fin de chaque séjour de malade dans une unité médicale assurant des soins de MCO."/>).  

Le diagnostic relié ne peut exister que si le diagnostic principal est un diagnostic du chapitre 21 de la CIM (codes en Z qui regroupent les motifs de prise en charge du type « séance de chimiothérapie ») et s'il s'agit d'une maladie chronique ou de longue durée. Son rôle est d'améliorer la précision documentaire du codage en indiquant la pathologie à l'origine du motif de prise en charge que représente le code « Z ».  

# Références
- [Page](https://fr.wikipedia.org/wiki/Programme_de_m%C3%A9dicalisation_des_syst%C3%A8mes_d%27information) wikipédia  
- Plus d'informations dans le [glossaire](https://www.atih.sante.fr/glossaire) et sur le [site](https://www.atih.sante.fr/) de l'ATIH
