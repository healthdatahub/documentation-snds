# UCD - Unité Commune de Dispensation
<!-- SPDX-License-Identifier: MPL-2.0 -->

La liste en sus définit un ensemble d’unités communes de dispensation (UCD), i.e. le plus petit conditionnement pharmaceutique, qui sont les médicaments financés « en sus » des tarifs des séjours hospitaliers. Ce dispositif vise à favoriser l’accès aux traitements innovants et coûteux à travers un financement dérogatoire, en garantissant à l’établissement de santé de pouvoir administrer ces traitements sans que cela ne lui impute trop son budget global.

La liste UCD contient des codes de 7 caractères numériques et des libellés.

**Le contenu de la liste en sus est dynamique.**   
La liste initiale a été publiée au journal officiel de la république française le 10 mai 2005 : *« Arrêté du 4 avril 2005 pris en application de l'article L. 162-22-7 du code de la sécurité sociale et fixant la spécialités pharmaceutiques prises en charge par l'assurance maladie en sus des prestations d'hospitalisation »*. Cette liste est ensuite mise à jour par des arrêtés modificatifs.

# Références
- [Fiche](../fiches/medicaments_de_la_liste_en_sus.md) sur les médicaments en sus