# ATIH - Agence technique de l'information sur l'hospitalisation
<!-- SPDX-License-Identifier: MPL-2.0 -->

L’ATIH est un établissement public de l’État à caractère administratif placé sous la tutelle des ministres chargés de la santé, des affaires sociales et de la sécurité sociale. Sur les secteurs sanitaire et médico-social, l'agence est chargée de :

- la collecte, l’hébergement et l’analyse des données des établissements de santé ;
- la gestion technique des dispositifs de financement des établissements ;
- la réalisation d’études sur les coûts des établissements sanitaires et médico-sociaux ;
- l’élaboration et la maintenance des nomenclatures de santé.

Dans le cadre du SNDS, l’ATIH est en charge du <link-previewer href="PMSI.html" text="PMSI" preview-title="PMSI - Programme de médicalisation des systèmes d’information" preview-text="Le PMSI permet de décrire de façon synthétique et standardisée l’activité médicale des établissements de santé."/>, la composante relative à l’activité des établissements de santé.

# Références

- Site internet [atih.sante.fr](https://www.atih.sante.fr)
- [Page wikipedia](https://fr.wikipedia.org/wiki/Agence_technique_de_l%27information_sur_l%27hospitalisation)
