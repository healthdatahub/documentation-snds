# DAF - Dotation annuelle de financement
<!-- SPDX-License-Identifier: MPL-2.0 -->

La Dotation annuelle de financement est une dotation forfaitaire rémunérant la part des dépenses prises en charge par l’assurance maladie au titre de l’activité de soins de suite et de réadaptation et/ou de psychiatrie des établissements de santé publics ou des établissements de santé antérieurement financés par dotation globale, dispensés au profit des patients assurés sociaux. 

La somme des DAF est retracée par l’objectif de dépenses d’assurance-maladie (<link-previewer href="ONDAM.html" text="ODAM" preview-title="ONDAM - Objectif national des dépenses d’assurance maladie" preview-text="L’ONDAM est fixé chaque année par le Parlement, conformément aux dispositions de la loi de financement de la sécurité sociale (LFSS. " />).

Mise en place en même temps que la <link-previewer href="T2A.html" text="T2A" preview-title="T2A - Tarification à l'activité" preview-text="La tarification à l'activité (T2A) est un mode de financement des établissements de santé français issu de la réforme hospitalière du plan Hôpital 2007, qui vise à médicaliser le financement tout en équilibrant l'allocation des ressources financières et en responsabilisant les acteurs de santé. " /> pour les établissements restés hors du champ, elle est calculée a priori et non pas à partir des données de coûts des établissements comme l’était la <link-previewer href="DGF.html" text="DGF" preview-title="DGF - Dotation globale de financement" preview-text="La DGF, dite « budget global », a constitué le mode de financement des hôpitaux publics et des établissements d’hospitalisation privés non lucratifs, dont ceux participant au service public hospitalier [^1] de 1984 jusqu’à la mise en place de la T2A. " />.

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/dotation-annuelle-de-financement-daf) sur le site internet du ministère
