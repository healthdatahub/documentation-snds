# RTU - Recommandation temporaire d'utilisation
<!-- SPDX-License-Identifier: MPL-2.0 -->

Une recommandation temporaire d’utilisation (RTU) est établie par l’<link-previewer href="ANSM.html" text="ANSM" preview-title="ANSM - Agence nationale de sécurité du médicament" preview-text="L'ANSM est l'Agence nationale de sécurité du médicament et des produits de santé." /> et à son initiative, en vue d’encadrer et de sécuriser une pratique de prescription hors <link-previewer href="AMM.html" text="AMM" preview-title="AMM - Autorisation de mise sur le marché" preview-text="Pour être commercialisée, une spécialité pharmaceutique doit obtenir préalablement une autorisation de mise sur le marché (AMM). " /> qui a cours sur le territoire national et qui est susceptible d’exposer les patients à des risques. 

Une RTU est établie par l’ANSM en l’absence de spécialité de même principe actif, de même dosage et de même forme pharmaceutique bénéficiant d’une AMM ou d’une <link-previewer href="ATU.html" text="ATU" preview-title="ATU - Autorisation temporaire d'utilisation" preview-text="Des spécialités pharmaceutiques qui ne bénéficient pas d’une autorisation de mise sur le marché délivrées par l’ANSM si elles sont destinées à traiter des maladies graves ou rares, en l’absence de traitement approprié, lorsque la mise en œuvre du traitement ne peut être différée." /> dans l’indication considérée. 
La RTU repose sur l’évaluation par l’ANSM d’une présomption de rapport bénéfice/risque favorable. 
La RTU est accordée pour une durée de 3 ans renouvelable.

## Références

- [Page sur le site du ministère de la santé](https://solidarites-sante.gouv.fr/soins-et-maladies/medicaments/professionnels-de-sante/autorisation-de-mise-sur-le-marche/article/recommandations-temporaires-d-utilisation-rtu)
- [Page sur le site de l'ANSM](https://www.ansm.sante.fr/Activites/Recommandations-Temporaires-d-Utilisation-RTU/Les-Recommandations-Temporaires-d-Utilisation-Principes-generaux/(offset)/0)
- [Page wikipedia](https://fr.wikipedia.org/wiki/Recommandation_temporaire_d%E2%80%99utilisation)
