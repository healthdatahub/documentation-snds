# CIM - Classification internationale des maladies
<!-- SPDX-License-Identifier: MPL-2.0 -->

La Classification internationale des maladies (CIM) (en anglais, *International Classification of Diseases* (*ICD*))1 est une classification médicale codifiée classifiant les maladies et une très vaste variété de signes, symptômes, lésions traumatiques, empoisonnements, circonstances sociales et causes externes de blessures ou de maladies. L'ensemble des diagnostics est classé en chapitres (21 chapitres dans la version CIM-10).

Elle est publiée par l'Organisation mondiale de la santé ([OMS](OMS.md)) et est mondialement utilisée pour l'enregistrement des causes de morbidité et de mortalité touchant le domaine de la médecine.

> La version actuellement utilisée en France depuis 1996 est la dixième révision : [CIM-10](https://icd.who.int/browse10/2019/en). Et la version la plus récente, publiée en janvier 2022, est la [CIM-11](https://icd.who.int/browse/2024-01/mms/en).

# Références

- [Section ICD10](https://icd.who.int/browse10/2019/en) du site de l'OMS (en anglais)
- [Définition](https://www.cepidc.inserm.fr/causes-medicales-de-deces/classification-internationale-des-maladies-cim) sur le site du CépiDC-Inserm
- [Page wikipedia](https://fr.wikipedia.org/wiki/Classification_internationale_des_maladies)
- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/classification-internationale-des-maladies-cim) sur le site internet du ministère de la santé
- [Le codage CIM10 dans le PMSI](https://www.atih.sante.fr/cim-10-fr-2022-usage-pmsi) sur le site de l'ATIH et leur version [pdf](https://www.atih.sante.fr/sites/default/files/public/content/4216/cim-10-fr_2022_a_usage_pmsi.pdf)
- [Site](https://www.aideaucodage.fr/cim) d'aide au codage