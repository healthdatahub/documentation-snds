# Documentation de la standardisation de la base principale du SNDS au format OMOP
<!-- SPDX-License-Identifier: MPL-2.0 -->

Bienvenue dans la documentation de la standardisation de la **base principale du SNDS** au format **OMOP**.

Fruit d'un premier travail mené par le Health Data Hub, lors d'appels à projet EHDEN, la méthode de standardisation de la base principale du SNDS au format OMOP présentée ici, se veut **<u>collaborative</u>** ! 

Toute suggestion d'amélioration est la bienvenue, via le système d'issues de [ce dépôt GitLab](https://gitlab.com/healthdatahub/applications-du-hdh/documentation-snds). 

Cette documentation illustre les scripts correspondants, disponibles sur [cet autre dépôt GitLab](https://gitlab.com/healthdatahub/snds_omop/-/blob/master/README.md?ref_type=heads&plain=0)

## Organisation
Cette documentation est organisé en 2 sections : 
- [Introduction](./introduction/) contient des détails sur le contexte du projet et le modèle OMOP.
- [Documentation de l'ETL](./documentation_etl/) contient des explications sur les correspondances entre les tables et variables du SNDS et le modèle OMOP.







