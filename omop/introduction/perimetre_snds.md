# Périmètre des données SNDS standardisées
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette documentation concerne la standardisation d'un **échantillon** du SNDS au format OMOP. Plus précisément, cet échantillon contient des données de 3 millions de patients sur les années 2015-2021.

Plus précisément, l'extraction contient : 

- Les tables du **DCIR** et du **PMSI** pour les années **2015-2021**.
- Les tables du **CépiDC** pour les années **2015-2017**.

Dans ce cadre, la liste complète des tables et variables du SNDS qui ont été utilisées lors de la standardisation est disponible dans [ce fichier](/files/tables_variables_snds.xlsx).


