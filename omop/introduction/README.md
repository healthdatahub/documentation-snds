# Introduction
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette section contient des éléments introductifs à la transformation du SNDS au [format OMOP, v5.3](https://ohdsi.github.io/CommonDataModel/cdm53.html), déclinés en trois parties :
- [Transformer le SNDS au format OMOP : Contexte](snds_omop.md). Cette section décrit le contexte du projet de standardisation du SNDS au format OMOP mené par le Health Data Hub. 
- [Périmètre des données standardisées](perimetre_snds.md). Cette section décrit le périmètre des données concernées par les règles de standardisation. Il est possible que celles-ci doivent être légèrement adaptées pour un périmètre plus large.
- [Le modèle OMOP-CDM](omop.md). Cette section procure des informations succintes sur le modèle OMOP 





