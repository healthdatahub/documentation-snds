# Fiches descriptives des standards et terminologies nationaux et internationaux des données de santé 
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le Health Data Hub est heureux de mettre à disposition des fiches descriptives sur les standards et terminologies nationaux et internationaux de données de santé.  
Ce travail a pour objectif de favoriser l’interopérabilité au sein de notre écosystème en facilitant la compréhension, et ainsi, l’adoption de ces standards.   

Ces fiches ont fait l’objet d’une collaboration avec les équipes Veltys, et avec des experts pour la relecture et la correction. Nous tenons particulièrement à remercier Vianney Jouhet (CHU de Bordeaux), Elise Caudin (CépiDc), Sylvie Cormont (AP-HP), Sophie Guéant (ATIH), Inès Bozinovic (ATIH), Joëlle Dubois (ATIH), Diane Paillet (ATIH), Alain Perie (ANS), Elisabeth Serrot (ANS), Maël Le Gall (ANS) et Anne-Françoise Adam-Blondon (INRAE).  

Avis à nos lecteurs et contributeurs : vos retours et remarques sont les bienvenus, notamment concernant les mises à jour de ces standards et terminologies, afin de pouvoir les maintenir pertinents au fil du temps.  

Bonne lecture !  

L’équipe du Health Data Hub  